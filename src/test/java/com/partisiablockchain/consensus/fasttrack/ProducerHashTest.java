package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.crypto.Hash;
import com.secata.tools.coverage.FunctionUtility;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test of {@link ProducerHash}. */
public final class ProducerHashTest {

  @Test
  public void values() {
    Hash empty = Hash.create(FunctionUtility.noOpConsumer());
    Block block = new Block(1000L, 10, 10, empty, empty, List.of(), List.of(), (short) 0);

    Hash hash = ProducerHash.create(block);
    assertThat(hash.toString())
        .isEqualTo("7c8180e14266e25e15b083339e3db738e64d4408e0c6d86b2a3da0c31961e670");
  }
}
