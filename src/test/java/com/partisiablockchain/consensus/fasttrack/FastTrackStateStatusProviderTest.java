package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
final class FastTrackStateStatusProviderTest {

  @Test
  public void getName() {
    String subChainId = "Shard1";
    FastTrackStateStatusProvider fastTrackStateStatus =
        new FastTrackStateStatusProvider(subChainId, null);
    Assertions.assertThat(fastTrackStateStatus.getName())
        .isEqualTo(subChainId + "-FastTrackStateStatusProvider");

    fastTrackStateStatus = new FastTrackStateStatusProvider(null, null);
    Assertions.assertThat(fastTrackStateStatus.getName())
        .isEqualTo("Gov-FastTrackStateStatusProvider");
  }

  @Test
  void getStatus() {
    Hash hashA = Hash.create(h -> h.writeInt(111));
    BlockStateStatus blockStateStatusA =
        new BlockStateStatus(hashA, 222, 21, List.of(createAddress(1)), List.of(createAddress(2)));
    Hash hashB = Hash.create(h -> h.writeInt(123));
    BlockStateStatus blockStateStatusB =
        new BlockStateStatus(
            hashB,
            321,
            42,
            List.of(createAddress(3), createAddress(4)),
            List.of(createAddress(5), createAddress(6)));
    List<BlockStateStatus> blockStates = List.of(blockStateStatusA, blockStateStatusB);
    int latestSignedBlockTime = 123;
    FastTrackStateStatusProvider fastTrackStateStatus =
        new FastTrackStateStatusProvider(
            "subChainId", new DummyFastTrackStatusProvider(blockStates, latestSignedBlockTime));

    Map<String, String> status = fastTrackStateStatus.getStatus();
    Assertions.assertThat(status).hasSize(11);
    Assertions.assertThat(status.get("latestSignedBlockTime"))
        .isEqualTo(Long.toString(latestSignedBlockTime));
    for (int i = 0; i < blockStates.size(); i++) {
      String prefix = "blockState" + i;
      BlockStateStatus blockState = blockStates.get(i);
      Assertions.assertThat(status.get(prefix + "BlockIdentifier"))
          .isEqualTo(blockState.blockIdentifier().toString());
      Assertions.assertThat(status.get(prefix + "BlockTime"))
          .isEqualTo(Long.toString(blockState.blockTime()));
      Assertions.assertThat(status.get(prefix + "BlockProducerIndex"))
          .isEqualTo(Long.toString(blockState.blockProducerIndex()));
      Assertions.assertThat(status.get(prefix + "GoodSigners"))
          .isEqualTo(
              blockState.goodSigners().stream()
                  .map(BlockchainAddress::writeAsString)
                  .toList()
                  .toString());
      Assertions.assertThat(status.get(prefix + "BadSigners"))
          .isEqualTo(
              blockState.badSigners().stream()
                  .map(BlockchainAddress::writeAsString)
                  .toList()
                  .toString());
    }
  }

  private record DummyFastTrackStatusProvider(
      List<BlockStateStatus> blockStates, long latestSignedBlockTime)
      implements AdditionalFastTrackStateStatus {}

  private BlockchainAddress createAddress(int i) {
    return BlockchainAddress.fromHash(
        BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeInt(i)));
  }
}
