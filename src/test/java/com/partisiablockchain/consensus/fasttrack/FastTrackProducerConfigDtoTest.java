package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FastTrackProducerConfigDtoTest {

  @Test
  public void values() {
    FastTrackProducerConfigDto fastTrackProducerConfigDto = createDto();
    Assertions.assertThat(fastTrackProducerConfigDto.producerKey).isEqualTo("1");
    Assertions.assertThat(fastTrackProducerConfigDto.timeout).isEqualTo(1500);
  }

  /** Create default dto. */
  static FastTrackProducerConfigDto createDto() {
    FastTrackProducerConfigDto fastTrackProducerConfigDto = new FastTrackProducerConfigDto();
    fastTrackProducerConfigDto.producerKey = "1";
    fastTrackProducerConfigDto.producerBlsKey = "2";
    fastTrackProducerConfigDto.timeout = 1500;
    fastTrackProducerConfigDto.registerProvider = new DummyAdditionalStatusProviders();
    return fastTrackProducerConfigDto;
  }
}
