package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.secata.tools.coverage.FunctionUtility;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockStateTest {

  private final Block block =
      new Block(
          System.currentTimeMillis(),
          1,
          0,
          Hash.create(FunctionUtility.noOpConsumer()),
          Hash.create(FunctionUtility.noOpConsumer()),
          List.of(),
          List.of());

  private final BlockState blockState =
      new BlockState(
          FunctionUtility.noOpConsumer(),
          block,
          BlockchainTestHelper.firstProducer.key.sign(block.identifier()),
          List.of(
              BlockchainTestHelper.firstProducer.publicKey,
              BlockchainTestHelper.secondProducer.publicKey,
              BlockchainTestHelper.thirdProducer.publicKey,
              BlockchainTestHelper.fourthProducer.publicKey),
          List.of(
              BlockchainTestHelper.firstProducer.blsKey.getPublicKey(),
              BlockchainTestHelper.secondProducer.blsKey.getPublicKey(),
              BlockchainTestHelper.thirdProducer.blsKey.getPublicKey(),
              BlockchainTestHelper.fourthProducer.blsKey.getPublicKey()),
          List.of(),
          state -> Hash.create(state::write).getBytes());

  @Test
  public void testToString() {
    Assertions.assertThat(blockState.toString())
        .contains(block.toString())
        .contains("producerIndex=-1");
  }

  @Test
  public void testProofIsValid() {
    Hash state = block.getState();
    BlockchainPublicKey publicKey = BlockchainTestHelper.firstProducer.publicKey;
    Hash proof =
        Hash.create(
            s -> {
              publicKey.write(s);
              s.write(Hash.create(state::write).getBytes());
            });
    Assertions.assertThat(blockState.proofValid(proof, publicKey)).isTrue();

    Hash badProof = Hash.create(s -> s.writeInt(123));
    Assertions.assertThat(blockState.proofValid(badProof, publicKey)).isFalse();
  }
}
