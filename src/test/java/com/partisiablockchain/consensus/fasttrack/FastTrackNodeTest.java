package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.server.ServerModule.ServerConfig;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;

/** Test. */
public final class FastTrackNodeTest {

  @TempDir public Path temporaryFolder;

  @Test
  public void register() throws IOException {
    BlockchainTestHelper blockchainHelper = BlockchainTestHelper.createEmpty(temporaryFolder);
    FastTrackProducerConfigDto config = FastTrackProducerConfigDtoTest.createDto();
    ServerConfig resourceConfig = Mockito.mock(ServerConfig.class);
    FastTrackNode node = new FastTrackNode();
    node.register(
        MemoryStorage.createRootDirectory(Files.createTempDirectory("data").toFile()),
        blockchainHelper.blockchain,
        resourceConfig,
        config);

    Mockito.verify(resourceConfig).registerCloseable(Mockito.any(FastTrackProducer.class));
    blockchainHelper.blockchain.close();
  }

  @Test
  public void getConfigType() {
    Assertions.assertThat(new FastTrackNode().configType())
        .isEqualTo(FastTrackProducerConfigDto.class);
  }
}
