package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.consensus.fasttrack.FastTrackProducer.FastTrackBlock;
import com.partisiablockchain.consensus.fasttrack.FastTrackProducer.FinalizationMessage;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.util.NumericConversion;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Test. */
public final class FastTrackStateTest {

  @TempDir public Path temporaryFolder;
  private File latestSignedBlock;
  private RootDirectory dataDirectory;

  private final List<FinalizationMessagePair> flooded = new ArrayList<>();
  private final Map<Hash, SignedTransaction> transactions = new HashMap<>();
  private final Map<Hash, ExecutableEvent> events = new HashMap<>();
  private final List<Hash> eventTransactions = new ArrayList<>();

  private FastTrackState state;
  private BlockchainLedger blockchain;
  private BlockchainTestHelper testHelper;

  /** Close any resources. */
  @AfterEach
  public void tearDown() {
    if (state != null) {
      state.close();
    }
    if (blockchain != null) {
      blockchain.close();
    }
  }

  /** Setup the state. */
  @BeforeEach
  public void setUp() {
    this.testHelper = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    this.blockchain = testHelper.blockchain;
    dataDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    latestSignedBlock = new File(temporaryFolder.toFile(), UUID.randomUUID().toString());
  }

  private void createStateForSigner() {
    createStateForSigner(false);
  }

  private void createStateForSigner(boolean shutdownInProgress) {
    ExceptionConverter.call(
        () ->
            this.state =
                new FastTrackState(
                    BlockchainTestHelper.secondProducer.key,
                    BlockchainTestHelper.secondProducer.blsKey,
                    possibleParents(),
                    dataDirectory,
                    latestSignedBlock,
                    this::addFlood,
                    transactions::get,
                    events::get,
                    () -> shutdownInProgress,
                    (Hash hash) -> blockchain.getStateStorage().read(hash)),
        "");
  }

  private void addFlood(
      FinalizationMessage messageType, Consumer<SafeDataOutputStream> contentBuilder) {
    this.flooded.add(new FinalizationMessagePair(messageType, contentBuilder));
  }

  private void createStateForNonSigner() {
    createStateForNonSigner(possibleParents());
  }

  private void createStateForNonSigner(List<BlockAndState> possibleParents) {
    ExceptionConverter.call(
        () ->
            this.state =
                new FastTrackState(
                    BlockchainTestHelper.notPayable.key,
                    BlockchainTestHelper.notPayable.blsKey,
                    possibleParents,
                    dataDirectory,
                    latestSignedBlock,
                    this::addFlood,
                    transactions::get,
                    events::get,
                    () -> false,
                    (Hash hash) -> blockchain.getStateStorage().read(hash)),
        "");
  }

  private List<BlockAndState> possibleParents() {
    ArrayList<BlockAndState> parents = new ArrayList<>(blockchain.getProposals());
    parents.add(blockchain.latest());
    return parents;
  }

  @Test
  public void isValid() {
    createStateForNonSigner();

    BlockAndState latest = blockchain.latest();

    Assertions.assertThat(createBlock(latest, 1).isValid()).isFalse();
    Assertions.assertThat(createBlock(latest, 0).isValid()).isTrue();

    FastTrackBlock invalidBlock =
        createBlock(latest, Hash.create(FunctionUtility.noOpConsumer()), 0);
    Assertions.assertThat(invalidBlock.isValid()).isFalse();

    Assertions.assertThat(
            createFastTrack(
                    latest,
                    new Block(
                        System.currentTimeMillis(),
                        latest.getBlock().getBlockTime() + 1,
                        latest.getBlock().getCommitteeId() + 1,
                        latest.getBlock().identifier(),
                        latest.getState().getHash(),
                        List.of(),
                        List.of(),
                        (short) 0))
                .isValid())
        .isFalse();
  }

  @Test
  public void addBlock() {
    createStateForNonSigner();
    Assertions.assertThat(state.getBlockStates()).hasSize(0);

    BlockAndState latest = blockchain.latest();
    FastTrackBlock block = createBlock(latest);
    state.addBlock(block);

    Assertions.assertThat(state.getBlockStates()).hasSize(1);
    Assertions.assertThat(flooded).hasSize(1);
    FinalizationMessagePair actual = flooded.get(0);
    Assertions.assertThat(actual.messageType).isEqualTo(FinalizationMessage.BLOCK);
    Assertions.assertThat(actual.content)
        .isEqualTo(
            SafeDataOutputStream.serialize(
                stream -> {
                  block.block.write(stream);
                  block.producerSignature.write(stream);
                }));
  }

  @Test
  public void addBlockAgainShouldNotFlood() {
    createStateForNonSigner();

    BlockAndState latest = blockchain.latest();
    FastTrackBlock block = createBlock(latest);
    state.addBlock(block);
    Assertions.assertThat(flooded).hasSize(1);
    state.addBlock(block);
    Assertions.assertThat(flooded).hasSize(1);
  }

  @Test
  public void shouldNotSignWhenShutdownInProgress() {
    createStateForSigner(true);

    BlockAndState latest = blockchain.latest();
    FastTrackBlock block = createBlock(latest);
    state.addBlock(block);
    Assertions.assertThat(flooded).hasSize(1);
  }

  @Test
  public void differentBlocksWithSameBlockTime() throws InterruptedException {
    createStateForNonSigner();

    BlockAndState latest = blockchain.latest();
    FastTrackBlock firstBlock = createBlock(latest);
    state.addBlock(firstBlock);

    FastTrackBlock secondBlock = createDifferentBlock(latest, firstBlock);
    Assertions.assertThatThrownBy(() -> state.addBlock(secondBlock))
        .hasMessageContaining("Unable to add block since the block time has already been seen");
  }

  private FastTrackBlock createDifferentBlock(BlockAndState latest, FastTrackBlock notEqualTo)
      throws InterruptedException {
    FastTrackBlock secondBlock = createBlock(latest);
    if (notEqualTo.block.identifier().equals(secondBlock.block.identifier())) {
      // Ensure that we get another production time
      Thread.sleep(1);
      secondBlock = createBlock(latest);
    }
    return secondBlock;
  }

  @Test
  public void addBlockForSigner() throws IOException {
    createStateForSigner();

    BlockAndState latest = blockchain.latest();
    FastTrackBlock block = createBlock(latest);
    state.addBlock(block);

    Assertions.assertThat(flooded).hasSize(2);

    long persistedSignatureTime =
        NumericConversion.longFromBytes(Files.readAllBytes(latestSignedBlock.toPath()), 0);
    Assertions.assertThat(persistedSignatureTime).isEqualTo(block.block.getBlockTime());
  }

  @Test
  public void shouldNotSignWhenPreviouslySigned() throws IOException {
    FastTrackBlock block = createBlock(blockchain.latest());
    Files.write(
        latestSignedBlock.toPath(), NumericConversion.longToBytes(block.block.getBlockTime()));
    createStateForSigner();

    state.addBlock(block);

    Assertions.assertThat(flooded).hasSize(1);
  }

  @Test
  public void unableToWriteAfterClose() {
    createStateForNonSigner();
    FastTrackBlock block = createBlock(blockchain.latest());
    state.addBlock(block);

    state.close();

    BlsSignature blsSignature =
        BlockchainTestHelper.secondProducer.blsKey.sign(block.block.identifier());
    Signature signature =
        BlockchainTestHelper.secondProducer.key.sign(
            FinalizationMessageHelper.createMessage(block.block.identifier(), blsSignature));
    Hash proof =
        createProof(
            BlockchainTestHelper.secondProducer.key, blockchain.latest().getBlock().getState());
    Assertions.assertThatThrownBy(
            () -> state.addEcho(createSignatureAndVerification(signature, blsSignature, proof)))
        .hasStackTraceContaining("Stream Closed");
  }

  private Hash createProof(KeyPair prover, Hash stateHash) {
    byte[] data = blockchain.getStateStorage().read(stateHash);
    return Hash.create(
        stream -> {
          prover.getPublic().write(stream);
          stream.write(data);
        });
  }

  @Test
  public void addSignatureMultipleTimes() {
    createStateForNonSigner();
    FastTrackBlock block = createBlock(blockchain.latest());
    state.addBlock(block);
    BlsSignature blsSignature =
        BlockchainTestHelper.secondProducer.blsKey.sign(block.block.identifier());
    Signature signature =
        BlockchainTestHelper.secondProducer.key.sign(
            FinalizationMessageHelper.createMessage(block.block.identifier(), blsSignature));
    Hash proof = createProof(BlockchainTestHelper.secondProducer.key, block.block.getState());
    state.addEcho(createSignatureAndVerification(signature, blsSignature, proof));
    Assertions.assertThat(flooded).hasSize(2);
    state.addEcho(createSignatureAndVerification(signature, blsSignature, proof));
    Assertions.assertThat(flooded).hasSize(2);
    Assertions.assertThat(state.getBlockStates().get(0).getGoodSigners()).hasSize(1);
  }

  @Test
  public void blockStateWithBadSigner() {
    createStateForNonSigner();
    FastTrackBlock block = createBlock(blockchain.latest());
    state.addBlock(block);

    // Bad signature
    BlsSignature blsSignature =
        BlockchainTestHelper.firstProducer.blsKey.sign(Hash.create(h -> h.writeInt(1234)));
    Signature signature =
        BlockchainTestHelper.firstProducer.key.sign(
            FinalizationMessageHelper.createMessage(block.block.identifier(), blsSignature));
    Hash proof = createProof(BlockchainTestHelper.firstProducer.key, block.block.getState());
    state.addEcho(createSignatureAndVerification(signature, blsSignature, proof));

    // Good signature
    blsSignature = BlockchainTestHelper.secondProducer.blsKey.sign(block.block.identifier());
    signature =
        BlockchainTestHelper.secondProducer.key.sign(
            FinalizationMessageHelper.createMessage(block.block.identifier(), blsSignature));
    proof = createProof(BlockchainTestHelper.secondProducer.key, block.block.getState());
    state.addEcho(createSignatureAndVerification(signature, blsSignature, proof));

    // Good signature
    blsSignature = BlockchainTestHelper.thirdProducer.blsKey.sign(block.block.identifier());
    signature =
        BlockchainTestHelper.thirdProducer.key.sign(
            FinalizationMessageHelper.createMessage(block.block.identifier(), blsSignature));
    proof = createProof(BlockchainTestHelper.thirdProducer.key, block.block.getState());
    state.addEcho(createSignatureAndVerification(signature, blsSignature, proof));

    BlockState blockState = state.getBlockStates().get(0);
    Assertions.assertThat(blockState.getBadSigners()).hasSize(1);
    Assertions.assertThat(blockState.getGoodSigners()).hasSize(2);
  }

  @Test
  public void ignoreBlockWithInvalidProof() {
    createStateForNonSigner();
    FastTrackBlock block = createBlock(blockchain.latest());
    state.addBlock(block);
    BlsSignature blsSignature =
        BlockchainTestHelper.secondProducer.blsKey.sign(block.block.identifier());
    Signature signature =
        BlockchainTestHelper.secondProducer.key.sign(Hash.create(blsSignature::write));
    Hash invalidProof = Hash.create(s -> s.writeString("invalid"));
    state.addEcho(createSignatureAndVerification(signature, blsSignature, invalidProof));
    Assertions.assertThat(flooded).hasSize(1);
  }

  @Test
  public void ignoreInvalidFile() throws IOException {
    Files.write(dataDirectory.createFile(StorageFiles.FILE_PREFIX + 0).toPath(), new byte[10]);
    createStateForNonSigner();
    assertEmptyState();
  }

  private SignatureAndVerification createForProducer(
      BlockchainTestHelper.TestProducer producer, FastTrackBlock block) {
    BlsSignature blsSignature = producer.blsKey.sign(block.block.identifier());
    Signature signature =
        producer.key.sign(
            FinalizationMessageHelper.createMessage(block.block.identifier(), blsSignature));
    Hash proof = createProof(producer.key, block.parent.getBlock().getState());
    return createSignatureAndVerification(signature, blsSignature, proof);
  }

  @Test
  public void cleanupBlockOnceAppendedToBlockchain() {
    createStateForSigner();

    FinalBlock finalBlock = finalBlock(blockchain.latest());
    blockchain.appendBlock(finalBlock);
    BlockAndState blockAndState = blockchain.getProposals().get(0);

    FastTrackBlock firstBlock = createBlock(blockAndState);
    state.addBlock(firstBlock);

    blockchain.appendBlock(finalBlock(firstBlock.block));
    BlockAndState blockAndState1 = blockchain.getProposals().get(0);

    state.clean(List.of(blockAndState1, blockAndState));
    verifyStateBySerialization(Map.of());
  }

  @Test
  public void shouldPersistToStorage() {
    createStateForNonSigner();
    FastTrackBlock block = createBlock(blockchain.latest());
    state.addBlock(block);
    SignatureAndVerification data = createForProducer(BlockchainTestHelper.secondProducer, block);
    state.addEcho(data);

    state.close();

    createStateForSigner();

    verifyStateBySerialization(
        Map.of(block.block, List.of(createProducerSignature(block.producerSignature), data)));
  }

  @Test
  public void shouldPersistMultipleBlocksToStorage() {
    blockchain.appendBlock(finalBlock(blockchain.latest()));

    // block 1
    createStateForNonSigner();
    FastTrackBlock firstBlock = createBlock(blockchain.latest());
    state.addBlock(firstBlock);
    SignatureAndVerification firstBlockData =
        createForProducer(BlockchainTestHelper.secondProducer, firstBlock);
    state.addEcho(firstBlockData);

    // block 2
    FastTrackBlock secondBlock = createBlock(blockchain.getProposals().get(0));
    state.addBlock(secondBlock);
    SignatureAndVerification secondBlockData0 =
        createForProducer(BlockchainTestHelper.thirdProducer, secondBlock);
    state.addEcho(secondBlockData0);
    SignatureAndVerification secondBlockData1 =
        createForProducer(BlockchainTestHelper.fourthProducer, secondBlock);
    state.addEcho(secondBlockData1);

    state.close();
    Assertions.assertThat(dataDirectory.createFile(StorageFiles.FILE_PREFIX + 0)).exists();
    Assertions.assertThat(dataDirectory.createFile(StorageFiles.FILE_PREFIX + 1)).exists();

    createStateForSigner();

    verifyStateBySerialization(
        Map.of(
            firstBlock.block,
            List.of(createProducerSignature(firstBlock.producerSignature), firstBlockData),
            secondBlock.block,
            List.of(
                createProducerSignature(secondBlock.producerSignature),
                secondBlockData0,
                secondBlockData1)));
  }

  private SignatureAndVerification createProducerSignature(Signature signature) {
    // We cheat a bit here and use a SignatureAndVerification object to pass a producer signature.
    return createSignatureAndVerification(signature, null, null);
  }

  private SignatureAndVerification createSignatureAndVerification(
      Signature signature, BlsSignature blsSignature, Hash proof) {
    return new SignatureAndVerification(signature, blsSignature, proof);
  }

  @Test
  public void shouldReuseStorageFile() {
    blockchain.appendBlock(finalBlock(blockchain.latest()));
    BlockAndState proposal = blockchain.getProposals().get(0);

    createStateForNonSigner();
    FastTrackBlock block = createBlock(blockchain.latest());
    state.addBlock(block);

    // Should remove the pending block and allow reuse of file
    state.clean(List.of(proposal));

    FastTrackBlock proposedBlock = createBlock(proposal);
    state.addBlock(proposedBlock);
    state.close();
    Assertions.assertThat(dataDirectory.createFile(StorageFiles.FILE_PREFIX + 0)).exists();
    Assertions.assertThat(dataDirectory.createFile(StorageFiles.FILE_PREFIX + 1)).doesNotExist();

    state.close();

    createStateForSigner();

    verifyStateBySerialization(
        Map.of(
            proposedBlock.block,
            List.of(new SignatureAndVerification(proposedBlock.producerSignature, null, null))));
  }

  private void verifyStateBySerialization(Map<Block, List<SignatureAndVerification>> expected) {
    // Make mutable
    expected = new HashMap<>(expected);
    SafeDataInputStream fromBytes =
        SafeDataInputStream.createFromBytes(SafeDataOutputStream.serialize(state::writeForSync));
    int count = expected.size();
    Assertions.assertThat(fromBytes.readInt()).isEqualTo(count);
    for (int i = 0; i < count; i++) {
      // each chunk is stored as [block | producerSignature | signatures ... ]
      Block read = Block.read(fromBytes);
      Signature producerSignature = Signature.read(fromBytes);
      List<SignatureAndVerification> expectedSignatures = expected.remove(read);
      Assertions.assertThat(producerSignature).isEqualTo(expectedSignatures.get(0).getSignature());
      Assertions.assertThat(expectedSignatures).isNotNull();
      Assertions.assertThat(eventTransactions).hasSize(read.getEventTransactions().size());
      List<SignatureAndVerification> signatures =
          FastTrackProducer.SIGNATURE_AND_VERIFICATION_LIST.readDynamic(fromBytes);
      Assertions.assertThat(signatures).hasSize(expectedSignatures.size() - 1);
      List<SignatureAndVerification> expectedVerifications =
          expectedSignatures.subList(1, expectedSignatures.size());
      Assertions.assertThat(signatures).containsExactlyInAnyOrderElementsOf(expectedVerifications);
    }
  }

  @Test
  public void shouldNotReadOutdatedBlocks() {
    blockchain.appendBlock(finalBlock(blockchain.latest()));

    createStateForNonSigner();
    FastTrackBlock block = createBlock(blockchain.latest());
    state.addBlock(block);
    state.close();

    createStateForNonSigner(List.of(blockchain.getProposals().get(0)));

    assertEmptyState();
  }

  private void assertEmptyState() {
    verifyStateBySerialization(Map.of());
  }

  @Test
  public void invalidBlock() {
    createStateForSigner();
    BlockAndState latest = blockchain.latest();
    Assertions.assertThatThrownBy(
            () -> {
              Block block =
                  new Block(
                      System.currentTimeMillis(),
                      latest.getBlock().getBlockTime() + 1,
                      latest.getBlock().getCommitteeId(),
                      latest.getBlock().getState(),
                      latest.getState().getHash(),
                      List.of(),
                      List.of());
              state.addBlock(createFastTrack(latest, block));
            })
        .hasMessageContaining("Invalid block");
  }

  @Test
  public void invalidProducer() {
    createStateForSigner();
    BlockAndState latest = blockchain.latest();
    Assertions.assertThatThrownBy(
            () -> {
              Block block =
                  new Block(
                      System.currentTimeMillis(),
                      latest.getBlock().getBlockTime() + 1,
                      latest.getBlock().getCommitteeId(),
                      latest.getBlock().identifier(),
                      latest.getState().getHash(),
                      List.of(),
                      List.of(),
                      0);
              state.addBlock(
                  createFastTrack(latest, block, BlockchainTestHelper.secondProducer.key));
            })
        .hasMessageContaining("Invalid block");
  }

  private FastTrackBlock createFastTrack(BlockAndState latest, Block block) {
    return createFastTrack(latest, block, BlockchainTestHelper.firstProducer.key);
  }

  private FastTrackBlock createFastTrack(BlockAndState latest, Block block, KeyPair producer) {
    Hash messageHash = ProducerHash.create(block);
    return new FastTrackBlock(block, producer.sign(messageHash), latest, 0);
  }

  @Test
  public void missingTransaction() {
    createStateForSigner();
    BlockAndState latest = blockchain.latest();
    Assertions.assertThatThrownBy(
            () ->
                state.addBlock(
                    createFastTrack(
                        latest,
                        new Block(
                            System.currentTimeMillis(),
                            latest.getBlock().getBlockTime() + 1,
                            latest.getBlock().getBlockTime(),
                            latest.getBlock().identifier(),
                            latest.getState().getHash(),
                            List.of(),
                            List.of(Hash.create(FunctionUtility.noOpConsumer())),
                            (short) 0))))
        .hasMessageContaining("Block with invalid transactions");
  }

  @Test
  public void missingEvent() {
    createStateForSigner();
    BlockAndState latest = blockchain.latest();
    Assertions.assertThatThrownBy(
            () ->
                state.addBlock(
                    createFastTrack(
                        latest,
                        new Block(
                            System.currentTimeMillis(),
                            latest.getBlock().getBlockTime() + 1,
                            latest.getBlock().getBlockTime(),
                            latest.getBlock().identifier(),
                            latest.getState().getHash(),
                            List.of(Hash.create(FunctionUtility.noOpConsumer())),
                            List.of(),
                            (short) 0))))
        .hasMessageContaining("Block with invalid transactions");
  }

  @Test
  public void invalidTransaction() {
    createStateForSigner();
    BlockAndState latest = blockchain.latest();
    SignedTransaction transaction =
        testHelper.createTransaction(
            BlockchainTestHelper.firstProducer.key, System.currentTimeMillis() - 1);
    transactions.put(transaction.identifier(), transaction);
    Assertions.assertThatThrownBy(
            () ->
                state.addBlock(
                    createFastTrack(
                        latest,
                        new Block(
                            System.currentTimeMillis(),
                            latest.getBlock().getBlockTime() + 1,
                            latest.getBlock().getBlockTime(),
                            latest.getBlock().identifier(),
                            latest.getState().getHash(),
                            List.of(),
                            List.of(transaction.identifier()),
                            (short) 0))))
        .hasMessageContaining("Block with invalid transactions");
  }

  @Test
  public void invalidEvent() {
    createStateForSigner();
    BlockAndState latest = blockchain.latest();
    ExecutableEvent event =
        new ExecutableEvent(
            "Shard",
            new EventTransaction(
                Hash.create(FunctionUtility.noOpConsumer()),
                new ShardRoute(null, 7),
                7,
                0,
                0,
                null,
                new InnerSystemEvent.CreateAccountEvent(
                    BlockchainTestHelper.firstProducer.address)));
    events.put(event.identifier(), event);
    Assertions.assertThatThrownBy(
            () ->
                state.addBlock(
                    createFastTrack(
                        latest,
                        new Block(
                            System.currentTimeMillis(),
                            latest.getBlock().getBlockTime() + 1,
                            latest.getBlock().getBlockTime(),
                            latest.getBlock().identifier(),
                            latest.getState().getHash(),
                            List.of(event.identifier()),
                            List.of(),
                            (short) 0))))
        .hasMessageContaining("Block with invalid transactions");
  }

  static BlsSignature aggregateSign(Hash message, BlockchainTestHelper.TestProducer... producers) {
    BlsSignature sig = null;
    for (BlockchainTestHelper.TestProducer producer : producers) {
      BlsSignature sign = producer.blsKey.sign(message);
      if (sig == null) {
        sig = sign;
      } else {
        sig = sig.addSignature(sign);
      }
    }
    return sig;
  }

  private FinalBlock finalBlock(BlockAndState latest) {
    FastTrackBlock block = createBlock(latest);
    Hash echoHash = block.block.identifier();
    BlsSignature blsSignature =
        aggregateSign(
            echoHash,
            BlockchainTestHelper.firstProducer,
            BlockchainTestHelper.thirdProducer,
            BlockchainTestHelper.fourthProducer);
    return new FinalBlock(
        block.block,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeDynamicBytes(new byte[] {0b1101}); // first, fourth, third
              blsSignature.write(s);
            }));
  }

  private FinalBlock finalBlock(Block block) {
    BlsSignature blsSignature =
        aggregateSign(
            block.identifier(),
            BlockchainTestHelper.firstProducer,
            BlockchainTestHelper.thirdProducer,
            BlockchainTestHelper.fourthProducer);
    return new FinalBlock(
        block,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeDynamicBytes(new byte[] {0b1101}); // first, fourth, third
              blsSignature.write(s);
            }));
  }

  private FastTrackBlock createBlock(BlockAndState latest) {
    return createBlock(latest, 0);
  }

  private FastTrackBlock createBlock(BlockAndState latest, int producerIndex) {
    return createBlock(latest, latest.getBlock().identifier(), producerIndex);
  }

  private FastTrackBlock createBlock(BlockAndState latest, Hash parent, int producerIndex) {
    return createFastTrack(
        latest,
        new Block(
            System.currentTimeMillis(),
            latest.getBlock().getBlockTime() + 1,
            latest.getBlock().getCommitteeId(),
            parent,
            latest.getState().getHash(),
            List.of(),
            List.of(),
            (short) producerIndex));
  }

  private static final class FinalizationMessagePair {

    private final FinalizationMessage messageType;
    private final byte[] content;

    private FinalizationMessagePair(
        FinalizationMessage messageType, Consumer<SafeDataOutputStream> content) {
      this.messageType = messageType;
      this.content = SafeDataOutputStream.serialize(content);
    }
  }
}
