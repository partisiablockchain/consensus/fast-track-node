package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateString;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/** Test of {@link FastTrackProducer#determineNextProducer}. */
public final class DetermineCurrentProducerIndexTest {

  private final Map<Hash, Integer> cachedExpectedProducer = new HashMap<>();

  @Test
  public void genesis() {
    assertBasic(0, 0, 0);
  }

  @Test
  public void nullBlock() {
    ConsensusState state = createState(1, createProducers(4));
    assertThat(
            FastTrackProducer.determineNextProducer(
                null, state, null, null, cachedExpectedProducer))
        .isEqualTo(0);
  }

  @Test
  public void parentHasNullState() {
    Map<Long, Block> blocks = createBlockMap(createBlock(1, -1), createBlock(2, -1));

    List<PairOfKeys> producers = createProducers(4);
    int actual =
        FastTrackProducer.determineNextProducer(
            blocks.get(2L),
            createState(1, producers),
            blocks::get,
            block -> {
              if (block.getBlockTime() == 1) {
                return new ConsensusState(null, null);
              } else {
                return createState(1, producers);
              }
            },
            cachedExpectedProducer);

    assertThat(actual).isEqualTo(1);
  }

  @Test
  public void nonResetBlock() {
    for (int i = 0; i < 10; i++) {
      assertBasic(1, i, i);
    }
  }

  @Test
  public void nonResetBlockModulo100() {
    for (int i = 0; i < 10; i++) {
      assertBasic((i + 1) * 100, 0, 1);
    }
  }

  @Test
  public void resetBlock() {
    List<Block> blocks = List.of(createBlock(0, 0), createBlock(1, 0), createBlock(2, -1));

    int actual =
        FastTrackProducer.determineNextProducer(
            blocks.get(2),
            createState(1, createProducers(4)),
            num -> blocks.get((int) num),
            block -> createState(0, createProducers(4)),
            cachedExpectedProducer);

    assertThat(actual).isEqualTo(1);
  }

  @Test
  public void resetBlocksAllTheWayBackToGenesis() {
    List<Block> blocks = List.of(createBlock(0, -1), createBlock(1, -1), createBlock(2, -1));

    int actual =
        FastTrackProducer.determineNextProducer(
            blocks.get(2),
            createState(0, createProducers(4)),
            num -> blocks.get((int) num),
            block -> createState(0, createProducers(4)),
            cachedExpectedProducer);

    assertThat(actual).isEqualTo(2);
  }

  @Test
  public void resetBlockModulo100() {
    Map<Long, Block> blocks = createBlockMap(createBlock(99, 0), createBlock(100, -1));

    int actual =
        FastTrackProducer.determineNextProducer(
            blocks.get(100L),
            createState(1, createProducers(4)),
            blocks::get,
            block -> createState(0, createProducers(4)),
            cachedExpectedProducer);

    assertThat(actual).isEqualTo(1);
  }

  Map<Long, Block> chainReset(int lastProducer, long lastNonResetBlock, int numberOfResetBlocks) {
    Map<Long, Block> map = new HashMap<>();
    for (int i = 0; i < numberOfResetBlocks; i++) {
      long blockTime = i + 1 + lastNonResetBlock;
      map.put(blockTime, createBlock(blockTime, -1));
    }
    map.put(lastNonResetBlock, createBlock(lastNonResetBlock, lastProducer));
    return map;
  }

  @Test
  public void chainOfResetBlocks() {
    List<PairOfKeys> producers = createProducers(4);
    int startingBlock = 95;
    ConsensusState state = createState(0, producers);
    for (int producer = 0; producer < producers.size(); producer++) {
      for (int numberOfResetBlocks = 0; numberOfResetBlocks <= 10; numberOfResetBlocks++) {
        Map<Long, Block> blocks = chainReset(producer, startingBlock, numberOfResetBlocks);

        Block currentBlock = blocks.get((long) startingBlock + numberOfResetBlocks);
        HashMap<Hash, Integer> cachedExpectedProducer = new HashMap<>();
        int actual =
            FastTrackProducer.determineNextProducer(
                currentBlock, state, blocks::get, block -> state, cachedExpectedProducer);

        assertThat(actual).isEqualTo((producer + numberOfResetBlocks) % 4);
        assertThat(cachedExpectedProducer).containsEntry(currentBlock.identifier(), actual);
      }
    }
  }

  @Test
  public void chainOfResetBlocksWithCache() {
    List<PairOfKeys> producers = createProducers(4);
    int startingBlock = 95;
    ConsensusState state = createState(0, producers);
    for (int producer = 0; producer < producers.size(); producer++) {
      cachedExpectedProducer.clear();
      for (int numberOfResetBlocks = 0; numberOfResetBlocks <= 10; numberOfResetBlocks++) {
        Map<Long, Block> blocks = chainReset(producer, startingBlock, numberOfResetBlocks);

        Block currentBlock = blocks.get((long) startingBlock + numberOfResetBlocks);
        int actual =
            FastTrackProducer.determineNextProducer(
                currentBlock, state, blocks::get, block -> state, cachedExpectedProducer);

        assertThat(actual).isEqualTo((producer + numberOfResetBlocks) % 4);
        assertThat(cachedExpectedProducer).containsEntry(currentBlock.identifier(), actual);
      }
    }
  }

  @Test
  public void chainOfResetBlocksWithCommitteeChange() {
    Map<Long, Block> blocks =
        createBlockMap(createBlock(30, 0), createBlock(31, -1), createBlock(32, -1));

    ConsensusState state1 = createState(0, createProducers(4));
    ConsensusState state2 = createState(1, createProducers(4));

    Map<Long, ConsensusState> stateMap =
        Map.of(
            30L, state1,
            31L, state1,
            32L, state2);

    int actual =
        FastTrackProducer.determineNextProducer(
            blocks.get(32L),
            createState(1, createProducers(4)),
            blocks::get,
            block -> stateMap.get(block.getBlockTime()),
            cachedExpectedProducer);

    assertThat(actual).isEqualTo(1);
  }

  @Test
  public void nonFastTrackParent() {
    Map<Long, Block> blocks = createBlockMap(createBlock(31, -1), createBlock(32, -1));

    ConsensusState state1 = new ConsensusState(new StateString("OtherConsensusState"), null);
    ConsensusState state2 = createState(1, createProducers(4));

    Map<Long, ConsensusState> stateMap =
        Map.of(
            31L, state1,
            32L, state2);

    int actual =
        FastTrackProducer.determineNextProducer(
            blocks.get(32L),
            createState(1, createProducers(4)),
            blocks::get,
            block -> stateMap.get(block.getBlockTime()),
            cachedExpectedProducer);

    assertThat(actual).isEqualTo(1);
  }

  @Test
  public void chainOfResetBlocksWithCommitteeChange2() {
    Map<Long, Block> blocks =
        createBlockMap(createBlock(30, -1), createBlock(31, -1), createBlock(32, -1));

    ConsensusState state1 = createState(0, createProducers(4));
    ConsensusState state2 = createState(1, createProducers(4));

    Map<Long, ConsensusState> stateMap =
        Map.of(
            30L, state1,
            31L, state2,
            32L, state2);

    int actual =
        FastTrackProducer.determineNextProducer(
            blocks.get(32L),
            createState(1, createProducers(4)),
            blocks::get,
            block -> stateMap.get(block.getBlockTime()),
            cachedExpectedProducer);

    assertThat(actual).isEqualTo(2);
  }

  @Test
  public void chainOfResetBlocksWithDifferentSizedCommitteeChange() {
    Map<Long, Block> blocks =
        createBlockMap(
            createBlock(29, 4),
            createBlock(30, -1),
            createBlock(31, -1),
            createBlock(32, -1),
            createBlock(33, -1),
            createBlock(34, -1),
            createBlock(35, -1));

    ConsensusState state1 = createState(0, createProducers(7));
    ConsensusState state2 = createState(1, createProducers(4));

    Map<Long, ConsensusState> stateMap =
        Map.of(
            29L, state1,
            30L, state2, // Committee change
            31L, state2, // 0
            32L, state2, // 1
            33L, state2, // 2
            34L, state2, // 3
            35L, state2 // 1, next should be 2
            );

    int actual =
        FastTrackProducer.determineNextProducer(
            blocks.get(35L),
            state2,
            blocks::get,
            block -> stateMap.get(block.getBlockTime()),
            cachedExpectedProducer);

    assertThat(actual).isEqualTo(2);
  }

  Map<Long, Block> createBlockMap(Block... blocks) {
    return Arrays.stream(blocks)
        .collect(Collectors.toMap(Block::getBlockTime, Function.identity()));
  }

  Block createBlock(long blockTime, int producerIndex) {
    return new Block(
        0,
        blockTime,
        0,
        Hash.create(hash -> hash.writeLong(blockTime - 1)),
        blockTime == 0 ? Block.GENESIS_PARENT : Hash.create(hash -> hash.writeLong(blockTime)),
        List.of(),
        List.of(),
        producerIndex);
  }

  static ConsensusState createState(int committee, List<PairOfKeys> producers) {
    return new ConsensusState(
        FastTrackGlobal.initialize(
            committeeId(committee),
            producers.stream()
                .map(
                    pok ->
                        new CommitteeMember(
                            pok.producerKey.createAddress(), pok.producerKey, pok.producerBlsKey))
                .collect(Collectors.toList())),
        new FastTrackLocal());
  }

  private static Hash committeeId(int number) {
    return Hash.create(hash -> hash.writeInt(number));
  }

  private void assertBasic(int latestBlock, int latestProducer, int expectedProducerIndex) {
    Block block = createBlock(latestBlock, latestProducer);
    assertThat(
            FastTrackProducer.determineNextProducer(
                block,
                createState(1, createProducers(100)),
                null,
                i -> createState(1, createProducers(100)),
                cachedExpectedProducer))
        .isEqualTo(expectedProducerIndex);
  }

  private BlsPublicKey randomBlsPublicKey() {
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(256, new SecureRandom()));
    return blsKeyPair.getPublicKey();
  }

  private List<PairOfKeys> createProducers(int count) {
    List<PairOfKeys> result = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      result.add(new PairOfKeys(new KeyPair().getPublic(), randomBlsPublicKey()));
    }
    return result;
  }

  static final class PairOfKeys {

    public final BlockchainPublicKey producerKey;
    public final BlsPublicKey producerBlsKey;

    PairOfKeys(BlockchainPublicKey producerKey, BlsPublicKey producerBlsKey) {
      this.producerKey = producerKey;
      this.producerBlsKey = producerBlsKey;
    }
  }
}
