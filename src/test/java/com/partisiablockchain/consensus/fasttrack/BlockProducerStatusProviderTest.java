package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
final class BlockProducerStatusProviderTest {

  @Test
  void getName() {
    String subChainId = "Shard1";
    BlockProducerStatusProvider blockProducerStatusProvider =
        new BlockProducerStatusProvider(subChainId, null);
    Assertions.assertThat(blockProducerStatusProvider.getName())
        .isEqualTo(subChainId + "-BlockProducerStatusProvider");

    blockProducerStatusProvider = new BlockProducerStatusProvider(null, null);
    Assertions.assertThat(blockProducerStatusProvider.getName())
        .isEqualTo("Gov-BlockProducerStatusProvider");
  }

  @Test
  void getStatus() {
    int latestProducedBlockTime = 1;
    AdditionalBlockProducerStatus additionalBlockProducerStatus = () -> latestProducedBlockTime;
    BlockProducerStatusProvider blockProducerStatusProvider =
        new BlockProducerStatusProvider(null, additionalBlockProducerStatus);
    Assertions.assertThat(blockProducerStatusProvider.getStatus().get("latestProducedBlockTime"))
        .isEqualTo(Long.toString(additionalBlockProducerStatus.getLatestProducedBlockTime()));
  }
}
