package com.partisiablockchain.consensus.fasttrack.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.stream.SafeDataOutputStream;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Test of {@link EpochEndPayload}. */
public final class EpochEndPayloadTest {

  @Test
  public void serializationWithNonNullShard() {
    EpochEndPayload end = new EpochEndPayload("shard0", Long.MAX_VALUE, new int[] {2});
    byte[] serialize = SafeDataOutputStream.serialize(end);
    assertThat(Hex.toHexString(serialize))
        .isEqualTo("0401000000067368617264307fffffffffffffff000000010002");
  }

  @Test
  public void serializationWithNullShard() {
    EpochEndPayload end = new EpochEndPayload(null, Long.MAX_VALUE, new int[] {2});
    byte[] serialize = SafeDataOutputStream.serialize(end);
    assertThat(Hex.toHexString(serialize)).isEqualTo("04007fffffffffffffff000000010002");
  }

  @Test
  public void values() {
    EpochEndPayload payload = new EpochEndPayload("shard0", 42, new int[] {2});
    assertThat(payload.shard()).isEqualTo("shard0");
    assertThat(payload.epoch()).isEqualTo(42);
    assertThat(payload.metrics()).isEqualTo(new int[] {2});
  }
}
