package com.partisiablockchain.consensus.fasttrack.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.consensus.fasttrack.CommitteeInfoGetter;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test of {@link EpochChangeListener}. */
public final class EpochChangeListenerTest {

  private final ImmutableChainState state = Mockito.mock(ImmutableChainState.class);
  private BlockchainPublicKey key1;
  private BlockchainPublicKey key2;
  private BlockchainPublicKey key3;
  private CommitteeInfoGetterStub committeeInfoGetter;

  /** Set up the test. */
  @BeforeEach
  public void setUp() {
    key1 = new KeyPair().getPublic();
    key2 = new KeyPair().getPublic();
    key3 = new KeyPair().getPublic();
    committeeInfoGetter = new CommitteeInfoGetterStub(List.of(key1, key2, key3));
  }

  @Test
  public void noEpochChange() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            transaction -> {
              throw new RuntimeException();
            },
            () -> 10_000L + epochInMillis());

    listener.newFinalBlock(createBlock(3, 11_000L), state);
  }

  @Test
  public void epochChange() {
    committeeInfoGetter = new CommitteeInfoGetterStub(List.of(key3, key2, key1));
    AtomicInteger transactionArgument = new AtomicInteger();
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            transaction -> transactionArgument.incrementAndGet(),
            () -> 5_000 + epochInMillis());

    listener.newFinalBlock(createBlock(2, epochInMillis()), state);
    listener.newFinalBlock(createBlock(2, epochInMillis() * 2), state);

    Assertions.assertThat(transactionArgument).hasValue(1);
  }

  @Test
  public void epochChangeWrongCommittee() {
    committeeInfoGetter = new CommitteeInfoGetterStub(List.of(key2, key3));
    AtomicInteger called = new AtomicInteger();
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            transaction -> called.incrementAndGet(),
            () -> 5_000 + epochInMillis());

    listener.newFinalBlock(createBlock(2, epochInMillis()), state);

    Assertions.assertThat(called).hasValue(0);
  }

  @Test
  void epochChangeWithMultipleFinalizedBlocks() {
    EpochChangeListener listener =
        new EpochChangeListener(
            l -> {
              if (l != 2) {
                return new byte[] {0, 0, 0, 1, 7};
              } else {
                return new byte[] {0, 0, 0, 1, 3};
              }
            },
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {
              SafeDataInputStream stream =
                  SafeDataInputStream.createFromBytes(SafeDataOutputStream.serialize(tx));
              assertHeaderIsCorrect(stream, "shard0");
              // epoch
              Assertions.assertThat(stream.readLong()).isEqualTo(0);
              // metrics
              Assertions.assertThat(stream.readInt()).isEqualTo(3);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(2);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(2);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(1);
            },
            () -> 2_000 + epochInMillis());

    committeeInfoGetter.activeCommittee = () -> 1;

    // First block indicates new committee, so it is signed by previous committee and not accounted
    // for here. It just changes the committee to contain key1.
    listener.newFinalBlock(createBlock(2, 2_000), state);
    // Updates the metrics
    listener.newFinalBlock(createBlock(2, 2_000), state);
    // Updates the metrics
    listener.newFinalBlock(createBlock(3, 2_000), state);
    // Epoch changed so send metrics of previous epoch and include this block in new epoch.
    listener.newFinalBlock(createBlock(3, epochInMillis()), state);
  }

  @Test
  void nextEpochHasNewMetrics() {
    AtomicInteger epoch = new AtomicInteger(2);
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {
              SafeDataInputStream stream =
                  SafeDataInputStream.createFromBytes(SafeDataOutputStream.serialize(tx));
              assertHeaderIsCorrect(stream, "shard0");
              Assertions.assertThat(stream.readLong()).isEqualTo(epoch.getAndIncrement());
              // metrics
              Assertions.assertThat(stream.readInt()).isEqualTo(3);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(1);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(1);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(1);
            },
            () -> {
              if (epoch.get() == 0) {
                return 1_000L + epochInMillis();
              } else {
                return epochInMillis() * 2;
              }
            });
    committeeInfoGetter.activeCommittee = () -> 3;
    listener.newFinalBlock(createBlock(4, epochInMillis() * 2), state);
    listener.newFinalBlock(createBlock(5, epochInMillis() * 2), state);
    listener.newFinalBlock(createBlock(6, epochInMillis() * 3), state);
    Assertions.assertThat(epoch.get()).isEqualTo(3);
  }

  @Test
  void epochWontChangeIfBlockTimeIs0() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            cid -> committeeInfoGetter,
            key1,
            null,
            tx -> {
              throw new RuntimeException();
            },
            this::epochInMillis);
    listener.newFinalBlock(createBlock(1, epochInMillis()), state);
  }

  @Test
  void metricsCollectionResetsOnNewCommittee() {
    EpochChangeListener listener =
        new EpochChangeListener(
            l -> {
              if (l == 2) {
                return new byte[] {0, 0, 0, 1, 0b1111};
              } else {
                return new byte[] {0, 0, 0, 1, 0b1110};
              }
            },
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            null,
            () -> 3_000 + epochInMillis());

    Assertions.assertThat(listener.getMetrics()).isNull();
    listener.newFinalBlock(createBlock(2, 2_000), state);
    listener.newFinalBlock(createBlock(2, 2_000), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {1, 1, 1});
    listener.newFinalBlock(
        new Block(4_000, 4, 1, blockTimeHash(3), blockTimeHash(4), List.of(), List.of()), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {0, 0, 0});
  }

  @Test
  void noIncrementBeyondMaxValue() {
    EpochChangeListener.CommitteeSignatureFrequencies metrics =
        new EpochChangeListener.CommitteeSignatureFrequencies(
            new int[] {2 * Short.MAX_VALUE}, createBlock(1, 1), () -> 1);
    Assertions.assertThat(metrics.getFrequencies()[0]).isEqualTo(2 * Short.MAX_VALUE);
    metrics.increment(0);
    Assertions.assertThat(metrics.getFrequencies()[0]).isEqualTo(2 * Short.MAX_VALUE + 1);
    metrics.increment(0);
    Assertions.assertThat(metrics.getFrequencies()[0]).isEqualTo(2 * Short.MAX_VALUE + 1);
  }

  @Test
  void onlyFloodMetricsOnCurrentEpoch() {
    AtomicBoolean transactionSenderCalled = new AtomicBoolean(false);
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> transactionSenderCalled.set(true),
            () -> epochInMillis() * 2);

    listener.newFinalBlock(createBlock(2, epochInMillis()), state);
    Assertions.assertThat(transactionSenderCalled.get()).isFalse();
    listener.newFinalBlock(createBlock(4, epochInMillis() * 2), state);
    Assertions.assertThat(transactionSenderCalled.get()).isTrue();
  }

  /** Metrics are only collected on the current epoch. */
  @Test
  void onlyCollectMetricsOnCurrentEpoch() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {},
            () -> epochInMillis() * 3);

    committeeInfoGetter.activeCommittee = () -> 1;
    // First block indicates new committee and is therefore signed by previous committee. Therefore,
    // no tracking of this block needed.
    listener.newFinalBlock(createBlock(2, epochInMillis()), state);
    listener.newFinalBlock(createBlock(2, epochInMillis()), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {0, 0, 0});
    listener.newFinalBlock(createBlock(4, epochInMillis() * 2), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {1, 1, 1});
  }

  /** Metrics are only collected when producer is part of the committee. */
  @Test
  void onlyCollectMetricsWhenInCommittee() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {},
            () -> epochInMillis() * 2);
    committeeInfoGetter.committee = List.of(key2, key3);
    listener.newFinalBlock(createBlock(4, epochInMillis() * 2), state);
    listener.newFinalBlock(createBlock(5, epochInMillis() * 2), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {0, 0});

    committeeInfoGetter.activeCommittee = () -> 3;
    committeeInfoGetter.committee = List.of(key1, key3);
    listener.newFinalBlock(createBlock(6, epochInMillis() * 2), state);
    listener.newFinalBlock(createBlock(7, epochInMillis() * 3), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {1, 1});
  }

  /** Metrics are not collected for block indicating new committee. */
  @Test
  void metricsNotCollectedForBlockIndicatingNewCommittee() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {},
            this::epochInMillis);
    listener.newFinalBlock(createBlock(1, epochInMillis()), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {0, 0, 0});
  }

  /** Metrics are only collected when producer is part of committee. */
  @Test
  void metricsOnlyCollectedWhenProducerInCommittee() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {},
            this::epochInMillis);
    committeeInfoGetter.committee = List.of(key2, key3);
    // First block indicates new committee and is therefore not included in the metrics.
    listener.newFinalBlock(createBlock(1, epochInMillis()), state);
    listener.newFinalBlock(createBlock(2, epochInMillis()), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {0, 0});
  }

  /** Metrics are only collected on relevant epochs. */
  @Test
  void metricsOnlyCollectedWhenOnRelevantEpochs() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {},
            () -> epochInMillis() * 2);
    // First block indicates new committee and is therefore not included in the metrics.
    listener.newFinalBlock(createBlock(1, 0), state);
    listener.newFinalBlock(createBlock(2, 0), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {0, 0, 0});
  }

  /** Metrics are send when the epoch changes and the epoch before the change is relevant. */
  @Test
  void metricsAreSendWhenEpochIsRelevant() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {
              SafeDataInputStream stream =
                  SafeDataInputStream.createFromBytes(SafeDataOutputStream.serialize(tx));
              assertHeaderIsCorrect(stream, "shard0");
              Assertions.assertThat(stream.readLong()).isEqualTo(0);
              // metrics
              Assertions.assertThat(stream.readInt()).isEqualTo(3);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(1);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(1);
              Assertions.assertThat(stream.readUnsignedShort()).isEqualTo(1);
            },
            () -> 0);
    committeeInfoGetter.activeCommittee = () -> 1;
    // First block indicates new committee and is therefore not included in the metrics.
    listener.newFinalBlock(createBlock(1, 0), state);
    // Updates the metrics.
    listener.newFinalBlock(createBlock(1, 0), state);
    // Epoch changed which sends the collected metrics.
    listener.newFinalBlock(createBlock(2, epochInMillis()), state);
  }

  /** Metrics are reset on new epoch and the current block is included in the metrics. */
  @Test
  void metricsResetOnNewEpoch() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {},
            () -> 0);
    committeeInfoGetter.activeCommittee = () -> 1;
    // First block indicates new committee and is therefore not included in the metrics.
    listener.newFinalBlock(createBlock(1, 0), state);
    // Updates the metrics.
    listener.newFinalBlock(createBlock(1, 0), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {1, 1, 1});
    // Epoch changed which resets metrics and includes the given block.
    listener.newFinalBlock(createBlock(2, epochInMillis()), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {1, 1, 1});
  }

  /** Metrics are reset on new committee and the block is not included in the metrics. */
  @Test
  void metricsResetOnNewCommittee() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> {},
            () -> 0);
    committeeInfoGetter.activeCommittee = () -> 1;
    // First block indicates new committee and is therefore not included in the metrics.
    listener.newFinalBlock(createBlock(1, 0), state);
    // Updates the metrics.
    listener.newFinalBlock(createBlock(1, 0), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {1, 1, 1});
    // Epoch changed which resets metrics and includes the given block.
    committeeInfoGetter.activeCommittee = () -> 2;
    listener.newFinalBlock(createBlock(2, epochInMillis()), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {0, 0, 0});
  }

  /** Epoch does not count as a new epoch if parent block time is 0. */
  @Test
  void epochIsNotNewWhenParentBlockTimeIs0() {
    EpochChangeListener listener =
        new EpochChangeListener(
            this::defaultFinalizationDataGetter,
            committeeId -> committeeInfoGetter,
            key1,
            "shard0",
            tx -> Assertions.fail(),
            this::epochInMillis);
    committeeInfoGetter.activeCommittee = () -> 1;
    // First block indicates new committee and is therefore not included in the metrics.
    listener.newFinalBlock(createBlock(1, 0), state);
    listener.newFinalBlock(createBlock(1, 0), state);
    // Does not send or reset the metrics as the parent block time is 0.
    listener.newFinalBlock(createBlock(1, epochInMillis()), state);
    Assertions.assertThat(listener.getMetrics().getFrequencies()).isEqualTo(new int[] {2, 2, 2});
  }

  private void assertHeaderIsCorrect(SafeDataInputStream stream, String shardName) {
    // invocation
    Assertions.assertThat(stream.readUnsignedByte()).isEqualTo(4);
    // optional shard name
    Assertions.assertThat(stream.readBoolean()).isTrue();
    Assertions.assertThat(stream.readString()).isEqualTo(shardName);
  }

  @SuppressWarnings("unused")
  private byte[] defaultFinalizationDataGetter(Long blockTime) {
    return new byte[] {0, 0, 0, 1, 7};
  }

  private Block createBlock(int blockTime, long productionTime) {
    Hash parentBlock;
    if (blockTime == 0) {
      parentBlock = null;
    } else {
      parentBlock = blockTimeHash(blockTime - 1);
    }

    Hash hash = blockTimeHash(blockTime);
    return new Block(productionTime, blockTime, 0, parentBlock, hash, List.of(), List.of());
  }

  private Hash blockTimeHash(long i) {
    return Hash.create(s -> s.writeLong(i));
  }

  private long epochInMillis() {
    return 1 << 21;
  }

  static final class CommitteeInfoGetterStub implements CommitteeInfoGetter {
    private int activeCommitteeTimesCalled = 0;
    Supplier<Integer> activeCommittee;
    List<BlockchainPublicKey> committee;

    public CommitteeInfoGetterStub(List<BlockchainPublicKey> committee) {
      this.activeCommittee =
          () -> {
            activeCommitteeTimesCalled++;
            if (activeCommitteeTimesCalled > 2) {
              return 1;
            } else {
              return 0;
            }
          };
      this.committee = committee;
    }

    @Override
    public int getActiveCommittee() {
      return activeCommittee.get();
    }

    @Override
    public List<BlockchainPublicKey> getCommittee() {
      return committee;
    }
  }
}
