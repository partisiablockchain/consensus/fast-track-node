package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.File;
import java.nio.file.Path;
import java.util.UUID;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Test. */
public final class ShutdownStateStorageTest {

  @TempDir public Path temporaryFolder;

  @Test
  public void illegalRandomSeed() {
    ShutdownStateStorage shutdownStateStorage =
        new ShutdownStateStorage(new File(temporaryFolder.toFile(), UUID.randomUUID().toString()));
    Assertions.assertThatThrownBy(() -> shutdownStateStorage.writeRandomSeed(new byte[31]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Random seed must be of length 32");
    Assertions.assertThatThrownBy(() -> shutdownStateStorage.writeRandomSeed(new byte[33]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Random seed must be of length 32");
    shutdownStateStorage.close();
  }

  @Test
  public void resources() {
    ShutdownStateStorage shutdownStateStorage =
        new ShutdownStateStorage(new File(temporaryFolder.toFile(), UUID.randomUUID().toString()));
    Stream<AutoCloseable> resources = shutdownStateStorage.resources();
    Assertions.assertThat(resources).hasSize(1);
    shutdownStateStorage.close();
  }
}
