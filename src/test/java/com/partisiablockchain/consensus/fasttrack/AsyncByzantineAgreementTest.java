package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.consensus.fasttrack.AsyncByzantineAgreement.MessageType;
import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.VoteResult;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AsyncByzantineAgreementTest {

  private final byte[] protocolId = Hash.create(FunctionUtility.noOpConsumer()).getBytes();
  private final List<KeyPair> keyPairs = VoteUpdatingProtocolTest.createKeyPairs();
  private final List<BlsKeyPair> blsKeyPairs = VoteUpdatingProtocolTest.createBlsKeyPairs();
  private final List<BlsPublicKey> blsPublicKeys =
      blsKeyPairs.stream().map(BlsKeyPair::getPublicKey).collect(Collectors.toList());
  private final List<BlockchainAddress> parties =
      keyPairs.stream()
          .map(KeyPair::getPublic)
          .map(BlockchainPublicKey::createAddress)
          .collect(Collectors.toList());
  private final Flooding flooding = new Flooding();
  private final DeterministicRandomBitGenerator defaultRandom =
      new DeterministicRandomBitGenerator(new byte[32]);

  @Test
  public void fullStackWithPreAgreement() {
    runFullTest(
        configuration ->
            AsyncByzantineAgreement.create(
                DeterministicRandomBitGenerator.createForTest(new byte[32]),
                configuration,
                flooding::flood,
                protocolId,
                true),
        true);
  }

  @Test
  public void fullStackWithoutPreAgreement() {
    Random random = new Random(66662);
    runFullTest(
        configuration -> {
          byte[] entropy = new byte[32];
          random.nextBytes(entropy);
          DeterministicRandomBitGenerator generator = new DeterministicRandomBitGenerator(entropy);
          return AsyncByzantineAgreement.create(
              generator,
              configuration,
              flooding::flood,
              protocolId,
              parties.indexOf(configuration.getMyAddress()) % 2 == 0);
        },
        true);
  }

  @Test
  public void fullStackWithoutPreAgreementStabilizeFalse() {
    runFullTest(
        configuration -> {
          byte[] entropy = new byte[32];
          entropy[0] = 2;
          DeterministicRandomBitGenerator generator = new DeterministicRandomBitGenerator(entropy);
          return AsyncByzantineAgreement.create(
              generator,
              configuration,
              flooding::flood,
              protocolId,
              parties.indexOf(configuration.getMyAddress()) % 2 == 0);
        },
        false);
  }

  @Test
  public void timeCapsules() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    TimeCapsules timeCapsules = new TimeCapsules(partyConfiguration);
    int partyId = 0;
    for (; partyId < partyConfiguration.maliciousCount() + 1; partyId++) {
      BlockchainAddress party = keyPairs.get(partyId).getPublic().createAddress();
      Assertions.assertThat(timeCapsules.isDetermined()).isFalse();
      Assertions.assertThat(timeCapsules.shouldTerminate()).isFalse();
      Assertions.assertThat(timeCapsules.addCapsule(true, party)).isTrue();
      Assertions.assertThat(timeCapsules.addCapsule(true, party)).isFalse();
    }

    BlockchainAddress party0 = keyPairs.get(0).getPublic().createAddress();
    Assertions.assertThat(timeCapsules.addCapsule(false, party0)).isTrue();
    Assertions.assertThat(timeCapsules.addCapsule(false, party0)).isFalse();

    // Ignore for wrong value and wrong sender
    BlockchainAddress anotherParty = keyPairs.get(partyId).getPublic().createAddress();
    Assertions.assertThat(timeCapsules.addCapsule(false, anotherParty)).isTrue();
    Assertions.assertThat(timeCapsules.addCapsule(true, new KeyPair().getPublic().createAddress()))
        .isFalse();

    Assertions.assertThat(timeCapsules.isDetermined()).isTrue();
    Assertions.assertThat(timeCapsules.getDeterminedResult()).isTrue();
    for (; partyId < partyConfiguration.honestCount(); partyId++) {
      BlockchainAddress party = keyPairs.get(partyId).getPublic().createAddress();
      Assertions.assertThat(timeCapsules.isDetermined()).isTrue();
      Assertions.assertThat(timeCapsules.shouldTerminate()).isFalse();
      Assertions.assertThat(timeCapsules.addCapsule(true, party)).isTrue();
      Assertions.assertThat(timeCapsules.addCapsule(true, party)).isFalse();
    }
    Assertions.assertThat(timeCapsules.isDetermined()).isTrue();
    Assertions.assertThat(timeCapsules.shouldTerminate()).isTrue();
  }

  @Test
  public void timeCapsulesDeterminedFalse() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    TimeCapsules timeCapsules = new TimeCapsules(partyConfiguration);
    int partyId = 0;
    for (; partyId < partyConfiguration.maliciousCount(); partyId++) {
      BlockchainAddress party = keyPairs.get(partyId).getPublic().createAddress();
      Assertions.assertThat(timeCapsules.addCapsule(true, party)).isTrue();
    }
    for (; partyId < 2 * partyConfiguration.maliciousCount() + 1; partyId++) {
      Assertions.assertThat(timeCapsules.isDetermined()).isFalse();
      Assertions.assertThat(timeCapsules.shouldTerminate()).isFalse();
      BlockchainAddress party = keyPairs.get(partyId).getPublic().createAddress();
      Assertions.assertThat(timeCapsules.addCapsule(false, party)).isTrue();
    }
    Assertions.assertThat(timeCapsules.isDetermined()).isTrue();
    Assertions.assertThat(timeCapsules.getDeterminedResult()).isFalse();
  }

  @Test
  public void shouldFloodTimeCapMessages() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    AsyncByzantineAgreement agreement =
        AsyncByzantineAgreement.create(
            defaultRandom, partyConfiguration, flooding::flood, protocolId, true);
    Assertions.assertThat(flooding.hasNext()).isTrue();
    flooding.takeNext();
    Signature signature = keyPairs.get(1).sign(agreement.createTimeCapsuleHash(true));
    byte[] timeCapMessage = createTimeCapMessage(signature);
    agreement.incoming(SafeDataInputStream.createFromBytes(timeCapMessage));
    Assertions.assertThat(flooding.hasNext()).isTrue();
    byte[] flooded = flooding.takeNext();
    Assertions.assertThat(flooded).isEqualTo(timeCapMessage);
  }

  @Test
  public void shouldSetResultWhenTimeCapsAreDetermined() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    AsyncByzantineAgreement agreement =
        AsyncByzantineAgreement.create(
            defaultRandom, partyConfiguration, flooding::flood, protocolId, true);
    flooding.takeNext();
    Hash timeCapsuleHash = agreement.createTimeCapsuleHash(true);
    for (int i = 0; i < partyConfiguration.maliciousCount() + 1; i++) {
      Signature signature = keyPairs.get(i + 1).sign(timeCapsuleHash);
      byte[] timeCapMessage = createTimeCapMessage(signature);
      agreement.incoming(SafeDataInputStream.createFromBytes(timeCapMessage));
      flooding.takeNext();
    }
    Assertions.assertThat(flooding.hasNext()).isTrue();
    byte[] flooded = flooding.takeNext();
    Assertions.assertThat(flooded)
        .isEqualTo(createTimeCapMessage(keyPairs.get(0).sign(timeCapsuleHash)));
    Assertions.assertThat(agreement.isDone()).isTrue();
  }

  @Test
  public void timeCapsuleHash() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    AsyncByzantineAgreement agreement =
        AsyncByzantineAgreement.create(
            defaultRandom, partyConfiguration, flooding::flood, protocolId, true);
    Assertions.assertThat(agreement.createTimeCapsuleHash(true))
        .isEqualTo(
            Hash.create(
                stream -> {
                  stream.writeString("TIME_CAPSULE");
                  stream.write(protocolId);
                  stream.writeBoolean(true);
                }));
  }

  @Test
  public void voteUpdateHash() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    AsyncByzantineAgreement agreement =
        AsyncByzantineAgreement.create(
            defaultRandom, partyConfiguration, flooding::flood, protocolId, true);
    Assertions.assertThat(agreement.createDetectingHash())
        .isEqualTo(
            Hash.create(
                stream -> {
                  stream.writeString("DETECTING");
                  stream.write(protocolId);
                  stream.writeInt(0);
                }));
    Assertions.assertThat(agreement.createStabilizingHash())
        .isEqualTo(
            Hash.create(
                stream -> {
                  stream.writeString("STABILIZING");
                  stream.write(protocolId);
                  stream.writeInt(0);
                }));
  }

  @Test
  public void hardVoteInDetectShouldSetResult() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    InnerVoteUpdateCreator voteUpdates = new InnerVoteUpdateCreator();
    AsyncByzantineAgreement agreement =
        AsyncByzantineAgreement.createInternal(
            partyConfiguration, flooding::flood, voteUpdates, protocolId, true);
    Assertions.assertThat(agreement.getRound()).isEqualTo(0);

    voteUpdates.finishNext(true, false);
    flooding.runToEmpty(List.of(agreement));
    Assertions.assertThat(agreement.getRound()).isEqualTo(0);

    voteUpdates.finishNext(true, true);
    flooding.runToEmpty(List.of(agreement));
    Assertions.assertThat(agreement.getRound()).isEqualTo(1);

    Assertions.assertThat(agreement.isDone()).isTrue();
    Assertions.assertThat(agreement.getResult()).isTrue();
  }

  private byte[] createTimeCapMessage(Signature signature) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeEnum(MessageType.TIMECAP);
          new TimeCapMessage(true, signature).write(stream);
        });
  }

  private void runFullTest(
      Function<PartyConfiguration, AsyncByzantineAgreement> creator, boolean expectedResult) {
    List<AsyncByzantineAgreement> protocols = new ArrayList<>();

    for (int i = 0; i < parties.size(); i++) {
      PartyConfiguration configuration =
          new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(i), blsKeyPairs.get(0));
      AsyncByzantineAgreement protocol = creator.apply(configuration);
      protocols.add(protocol);
      Assertions.assertThat(protocol.isDone()).isFalse();
    }

    flooding.runToEmpty(protocols);

    for (AsyncByzantineAgreement protocol : protocols) {
      Assertions.assertThat(protocol.isDone()).isTrue();
      Boolean result = protocol.getResult();
      Assertions.assertThat(result).isEqualTo(expectedResult);
      Assertions.assertThat(protocol.isTerminated()).isTrue();
    }
  }

  private static final class InnerVoteUpdateCreator implements VoteUpdateCreator {

    private final List<Consumer<DataStreamSerializable>> floods = new ArrayList<>();

    @Override
    public Protocol<VoteResult> createDetecting(
        PartyConfiguration partyConfiguration,
        byte[] protocolId,
        Consumer<DataStreamSerializable> flood,
        boolean vote) {
      floods.add(flood);
      return new TestVoteUpdate();
    }

    @Override
    public Protocol<VoteResult> createStabilizing(
        PartyConfiguration partyConfiguration,
        byte[] protocolId,
        Consumer<DataStreamSerializable> flood,
        boolean vote) {
      floods.add(flood);
      return new TestVoteUpdate();
    }

    void finishNext(boolean hardVote, boolean vote) {
      Assertions.assertThat(floods).isNotEmpty();
      floods
          .remove(0)
          .accept(
              stream -> {
                stream.writeBoolean(hardVote);
                stream.writeBoolean(vote);
              });
    }

    private static final class TestVoteUpdate implements Protocol<VoteResult> {

      VoteResult result = null;

      @Override
      public VoteResult getResult() {
        return result;
      }

      @Override
      public boolean isDone() {
        return result != null;
      }

      @Override
      public void incoming(SafeDataInputStream message) {
        result = new VoteResult(message.readBoolean(), message.readBoolean());
      }
    }
  }
}
