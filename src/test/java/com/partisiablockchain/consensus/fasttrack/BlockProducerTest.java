package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.consensus.fasttrack.BlockProducer.ProductionData;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.storage.BlockTimeStorage;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.util.NumericConversion;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BooleanSupplier;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Test. */
public final class BlockProducerTest {

  @TempDir public Path temporaryFolder;

  private final List<Block> produced = new ArrayList<>();
  private BlockProducer producer;
  private final File file = tempFile();
  private BlockchainTestHelper helper;

  /**
   * Initialize fields.
   *
   * @throws IOException if unable to create temp file
   */
  public BlockProducerTest() throws IOException {}

  /** Close the producer. */
  @AfterEach
  public void tearDown() {
    if (producer != null) {
      producer.close();
    }
    if (helper != null) {
      helper.blockchain.close();
    }
  }

  private File tempFile() throws IOException {
    return File.createTempFile("producer", ".bin");
  }

  private void createProducer(int timeout, boolean shouldProduce) {
    createProducer(timeout, () -> shouldProduce);
  }

  private void createProducer(int timeout, BooleanSupplier shouldProduce) {
    this.producer =
        new BlockProducer(
            helper.blockchain,
            BlockchainTestHelper.firstProducer.publicKey,
            file,
            (block, consensusState) -> shouldProduce.getAsBoolean(),
            parent -> timeout,
            produced::add,
            30_000L);
  }

  @Test
  public void newBlockNotProducer() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    createProducer(0, false);
    notifyAndNotProduction();
  }

  @Test
  public void nullSafeShard() {
    assertThat(BlockProducer.getNullSafeShard("Shard0")).isEqualTo("Shard0");
    assertThat(BlockProducer.getNullSafeShard(null)).isEqualTo("Gov");
  }

  @Test
  public void newBlockShouldTriggerProduction() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    createProducer(0, true);
    assertProduced(1);
    helper.appendEmptyBlock(BlockchainTestHelper.firstProducer.key);
    assertProduced(2);
  }

  @Test
  public void newEventShouldTriggerProduction() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    AtomicBoolean produce = new AtomicBoolean(false);
    createProducer(0, produce::get);
    notifyAndNotProduction();
    produce.set(true);
    producer.newPendingEvent(null);
    assertProduced(1);
  }

  private void notifyAndNotProduction() {
    producer.newPendingTransaction(null);
    assertProduced(0);
  }

  private void assertProduced(int expectedProductionCount) {
    sleep(200);
    assertThat(produced).hasSize(expectedProductionCount);
  }

  private void sleep(int sleepTime) {
    ExceptionConverter.run(() -> Thread.sleep(sleepTime), "");
  }

  @Test
  public void close() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    createProducer(0, false);
    assertThat(producer.isRunning()).isTrue();
    producer.close();

    BlockTimeStorage storage = producer.getLatestProductionTime();
    assertThatThrownBy(() -> storage.storeLatestBlockTime(1L))
        .hasStackTraceContaining("IOException: Stream Closed");
    assertThat(producer.isRunning()).isFalse();
  }

  @Test
  public void determineCommitteeId() {
    helper = BlockchainTestHelper.createWithFastTrackThreeCommittees(temporaryFolder);
    createProducer(0, false);
    BlockchainLedger.BlockAndState latest = helper.blockchain.latest();
    Block latestBlock = latest.getBlock();
    Block block =
        new Block(
            System.currentTimeMillis(),
            latestBlock.getBlockTime() + 1,
            latestBlock.getCommitteeId(),
            latestBlock.identifier(),
            latestBlock.getState(),
            List.of(),
            List.of(),
            -1);
    helper.blockchain.appendBlock(FastTrackProducerTest.createFinalBlock(block));
    latest = helper.blockchain.latest();
    assertThat(producer.determineCommitteeId(new ConsensusState(latest.getState()))).isEqualTo(1);
  }

  @Test
  public void shouldWaitAfterProduction() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    createProducer(0, false);

    AtomicBoolean done = new AtomicBoolean();
    Thread thread =
        new Thread(
            () -> {
              try {
                producer.produceBlock();
                done.set(true);
              } catch (InterruptedException e) {
                throw new RuntimeException(e);
              }
            });
    thread.start();

    sleep(100);
    assertThat(done).isFalse();

    thread.interrupt();
  }

  @Test
  public void shouldWaitForTransactions() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    createProducer(1000, true);
    assertProduced(0);
    notifyAndNotProduction();
    addPending(helper.createTransaction(BlockchainTestHelper.firstProducer.key));
    assertProduced(1);
    assertThat(produced.get(0).getTransactions()).hasSize(1);
  }

  @Test
  public void shouldNotProduceWhenProductionTimeIsLarger() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    BlockTimeStorage blockTimeStorage = new BlockTimeStorage(file, 0);
    blockTimeStorage.storeLatestBlockTime(helper.blockchain.getBlockTime() + 1);
    blockTimeStorage.close();

    createProducer(0, true);
    assertProduced(0);
  }

  @Test
  public void shouldStoreLatestProduced() throws IOException {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    long blockTimeBefore = helper.blockchain.getBlockTime();
    createProducer(0, true);
    assertProduced(1);
    assertThat(Files.readAllBytes(file.toPath()))
        .isEqualTo(NumericConversion.longToBytes(blockTimeBefore + 1));
  }

  @Test
  public void shouldFilterOutdated() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    addPending(
        SignedTransaction.create(
                CoreTransactionPart.create(1, System.currentTimeMillis() - 1, 10000),
                helper.createDeploy())
            .sign(BlockchainTestHelper.firstProducer.key, helper.chainId()));
    createProducer(0, true);
    assertProduced(1);
    assertThat(produced.get(0).getTransactions()).hasSize(0);
  }

  @Test
  public void producerIndexIsMinusOneWhenNoCommittee() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    addPending(
        SignedTransaction.create(
                CoreTransactionPart.create(1, System.currentTimeMillis() - 1, 10000),
                helper.createDeploy())
            .sign(BlockchainTestHelper.firstProducer.key, helper.chainId()));
    createProducer(0, true);
    assertProduced(1);
    Block block = produced.get(0);
    assertThat(block.getProducerIndex()).isEqualTo((short) -1);
    assertThat(block.getCommitteeId()).isEqualTo(-1);
  }

  @Test
  public void waitForTransactions() throws InterruptedException {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    createProducer(0, false);
    AtomicInteger count = new AtomicInteger();
    long before = System.currentTimeMillis();
    ProductionData productionData =
        producer.waitForTransactions(
            before + 200,
            () -> {
              count.getAndIncrement();
              return new ProductionData(0, List.of(), List.of());
            });
    long after = System.currentTimeMillis();
    assertThat(after - before).isGreaterThanOrEqualTo(200);
    assertThat(productionData).isNotNull();
    assertThat(count).hasValueLessThan(5);
  }

  @Test
  public void waitForTransactionsImmediateReturn() throws InterruptedException {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    createProducer(0, false);
    AtomicInteger count = new AtomicInteger();
    ProductionData productionData =
        producer.waitForTransactions(
            200,
            () -> {
              count.getAndIncrement();
              return new ProductionData(
                  0,
                  List.of(
                      SignedTransaction.create(
                              CoreTransactionPart.create(1, Long.MAX_VALUE, 10000),
                              helper.createDeploy())
                          .sign(BlockchainTestHelper.notPayable.key, helper.chainId())),
                  List.of());
            });
    assertThat(productionData).isNotNull();
    assertThat(count).hasValue(1);
  }

  private void addPending(SignedTransaction transaction) {
    boolean added = helper.blockchain.addPendingTransaction(transaction);
    assertThat(added).isTrue();
  }

  @Test
  public void produceFullBlock() {
    helper = BlockchainTestHelper.withManyAccountsAndFee(temporaryFolder);
    for (int i = 0; i < BlockProducer.MAX_TRANSACTIONS + 1; i++) {
      addPending(
          SignedTransaction.create(
                  CoreTransactionPart.create(1, Long.MAX_VALUE, 10000), helper.createDeploy())
              .sign(BlockchainTestHelper.keyPairs.get(i), helper.chainId()));
    }
    createProducer(0, true);
    waitForProducedBlock();
    assertThat(produced.get(0).getTransactions()).hasSize(BlockProducer.MAX_TRANSACTIONS);
  }

  private void waitForProducedBlock() {
    for (int i = 0; i < 40 && produced.size() == 0; i++) {
      sleep(200);
    }
    assertThat(produced).hasSize(1);
  }

  @Test
  public void twoTransactionsForAccount() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    addPending(helper.createTransaction(BlockchainTestHelper.firstProducer.key));
    addPending(
        SignedTransaction.create(
                CoreTransactionPart.create(1, Long.MAX_VALUE - 1, 10000), helper.createDeploy())
            .sign(BlockchainTestHelper.firstProducer.key, helper.chainId()));
    createProducer(0, true);
    waitForProducedBlock();
    assertThat(produced.get(0).getTransactions()).hasSize(1);
  }

  @Test
  public void newProposalShouldTriggerProduction() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    AtomicBoolean produce = new AtomicBoolean(false);
    createProducer(0, produce::get);
    notifyAndNotProduction();
    produce.set(true);
    producer.newBlockProposal(helper.blockchain.getLatestBlock());
    assertProduced(1);
  }

  @Test
  public void newFinalBlockShouldTriggerProduction() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    AtomicBoolean produce = new AtomicBoolean(false);
    createProducer(0, produce::get);
    notifyAndNotProduction();
    produce.set(true);
    producer.newFinalBlock(helper.blockchain.getLatestBlock(), helper.blockchain.getChainState());
    assertProduced(1);
  }

  @Test
  public void allowedToProduce() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    createProducer(0, false);
    Hash hash = Hash.create(FunctionUtility.noOpConsumer());
    long committeeId = 10;
    Block block =
        new Block(System.currentTimeMillis(), 10, committeeId, hash, hash, List.of(), List.of());
    assertThat(producer.allowedToProduce(block)).isTrue();
  }

  @Test
  public void productionData() {
    ProductionData productionData =
        new ProductionData(
            System.currentTimeMillis(),
            List.of(),
            List.of(
                new ExecutableEvent(
                    "org",
                    new EventTransaction(
                        Hash.create(s -> s.writeInt(12)),
                        new ShardRoute("dest", 12),
                        12,
                        0,
                        2,
                        null,
                        new InnerSystemEvent.SetFeatureEvent("", "")))));
    assertThat(productionData.hasTransaction()).isTrue();
  }

  @Test
  public void getValidTransactions() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    List<SignedTransaction> transactions =
        BlockProducer.getValidTransactions(null, 100L, BlockchainTestHelper.syncingState());
    assertThat(transactions).isEmpty();
  }
}
