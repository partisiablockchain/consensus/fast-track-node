package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.consensus.fasttrack.CutOffVote.AgreementCreator;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CutOffVoteTest {

  private final byte[] protocolId =
      Hash.create(stream -> stream.writeString("CutOffVote")).getBytes();
  private final List<KeyPair> keyPairs =
      IntStream.range(1, 5).mapToObj(BigInteger::valueOf).map(KeyPair::new).toList();
  private final List<BlsKeyPair> blsKeyPairs =
      IntStream.range(11, 15)
          .mapToObj(BigInteger::valueOf)
          .map(BlsKeyPair::new)
          .collect(Collectors.toList());
  private final List<BlsPublicKey> blsPublicKeys =
      blsKeyPairs.stream().map(BlsKeyPair::getPublicKey).collect(Collectors.toList());
  private final List<BlockchainAddress> parties =
      keyPairs.stream().map(KeyPair::getPublic).map(BlockchainPublicKey::createAddress).toList();
  private final Flooding flooding = new Flooding();

  @Test
  public void fullStackTest() {
    runFullTest(
        configuration ->
            CutOffVote.create(
                configuration,
                flooding::flood,
                protocolId,
                78,
                new DeterministicRandomBitGenerator(new byte[32])),
        78);
  }

  @Test
  public void fullStackWithRollback() {
    runFullTest(
        configuration ->
            CutOffVote.create(
                configuration,
                flooding::flood,
                protocolId,
                78 - parties.indexOf(configuration.getMyAddress()) % 2,
                new DeterministicRandomBitGenerator(new byte[32])),
        77);
  }

  @Test
  public void fullStackWithSomeBehind() {
    runFullTest(
        configuration ->
            CutOffVote.create(
                configuration,
                flooding::flood,
                protocolId,
                78 + parties.indexOf(configuration.getMyAddress()) % 2,
                new DeterministicRandomBitGenerator(new byte[32])),
        79);
  }

  @Test
  public void protocolId() {
    CutOffVote protocol =
        CutOffVote.createInternal(
            new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0)),
            FunctionUtility.noOpConsumer(),
            new InnerAgreementCreator(),
            protocolId,
            100,
            new DeterministicRandomBitGenerator(new byte[32]));
    Assertions.assertThat(protocol.createProtocolId(0))
        .isEqualTo(
            Hash.create(
                stream -> {
                  stream.writeString("ABA");
                  stream.write(protocolId);
                  stream.writeInt(0);
                }));
  }

  @Test
  public void shouldVoteTrueInTwoAgreements() {
    List<Boolean> votes = new ArrayList<>();
    CutOffVote.createInternal(
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0)),
        FunctionUtility.noOpConsumer(),
        (randomGenerator, partyConfiguration, flood, protocolId, currentVote) -> {
          votes.add(currentVote);
          return AsyncByzantineAgreement.create(
              randomGenerator, partyConfiguration, flood, protocolId, currentVote);
        },
        protocolId,
        100,
        new DeterministicRandomBitGenerator(new byte[32]));
    Assertions.assertThat(votes).containsExactlyInAnyOrder(false, false, true, true);
  }

  @Test
  public void noAgreements() {
    InnerAgreementCreator agreementCreator = new InnerAgreementCreator();
    final CutOffVote protocol =
        CutOffVote.createInternal(
            new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0)),
            flooding::flood,
            agreementCreator,
            protocolId,
            100,
            new DeterministicRandomBitGenerator(new byte[32]));
    for (int i = 0; i < 3; i++) {
      agreementCreator.finishNext(false);
    }
    flooding.runToEmpty(List.of(protocol));
    Assertions.assertThat(protocol.isDone()).isFalse();
    agreementCreator.finishNext(false);
    Assertions.assertThatThrownBy(() -> flooding.floodNext(List.of(protocol)))
        .hasMessageContaining("Malicious behaviour")
        .hasMessageContaining("No agreement about block times");
  }

  @Test
  public void twoRollbacks() {
    InnerAgreementCreator agreementCreator = new InnerAgreementCreator();
    final CutOffVote protocol =
        CutOffVote.createInternal(
            new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0)),
            flooding::flood,
            agreementCreator,
            protocolId,
            100,
            new DeterministicRandomBitGenerator(new byte[32]));
    agreementCreator.finishNext(false);
    agreementCreator.finishNext(false);
    agreementCreator.finishNext(true);
    flooding.runToEmpty(List.of(protocol));
    Assertions.assertThat(protocol.isDone()).isFalse();
    Assertions.assertThat(protocol.allAreTerminated()).isFalse();
    agreementCreator.finishNext(false);
    Assertions.assertThatThrownBy(() -> flooding.floodNext(List.of(protocol)))
        .hasMessageContaining("Malicious behaviour")
        .hasMessageContaining("We are only able to do a single rollback");
    Assertions.assertThat(protocol.allAreTerminated()).isTrue();
  }

  @Test
  public void unknownIncoming() {
    InnerAgreementCreator agreementCreator = new InnerAgreementCreator();
    CutOffVote protocol =
        CutOffVote.createInternal(
            new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0)),
            flooding::flood,
            agreementCreator,
            protocolId,
            100,
            new DeterministicRandomBitGenerator(new byte[32]));
    protocol.incoming(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                Hash.create(stream -> stream.writeString("I am not a valid protocol")))));
    Assertions.assertThat(flooding.hasNext()).isFalse();
  }

  private void runFullTest(Function<PartyConfiguration, CutOffVote> creator, long expectedResult) {
    List<CutOffVote> protocols = new ArrayList<>();

    for (int i = 0; i < parties.size(); i++) {
      KeyPair myKey = keyPairs.get(i);
      BlsKeyPair myBlsKey = blsKeyPairs.get(i);
      PartyConfiguration configuration =
          new PartyConfiguration(parties, blsPublicKeys, myKey, myBlsKey);
      CutOffVote protocol = creator.apply(configuration);
      protocols.add(protocol);
      Assertions.assertThat(protocol.isDone()).isFalse();
    }

    flooding.runToEmpty(protocols);

    for (CutOffVote protocol : protocols) {
      Assertions.assertThat(protocol.isDone()).isTrue();
      long result = protocol.getResult();
      Assertions.assertThat(result).isEqualTo(expectedResult);
    }
  }

  private static final class InnerAgreementCreator implements AgreementCreator {

    private final List<Consumer<DataStreamSerializable>> floods = new ArrayList<>();

    void finishNext(boolean vote) {
      Assertions.assertThat(floods).isNotEmpty();
      floods.remove(0).accept(stream -> stream.writeBoolean(vote));
    }

    @Override
    public Protocol<Boolean> create(
        DeterministicRandomBitGenerator randomGenerator,
        PartyConfiguration partyConfiguration,
        Consumer<DataStreamSerializable> flood,
        byte[] protocolId,
        boolean currentVote) {
      floods.add(flood);
      return new TestVoteUpdate();
    }

    private static final class TestVoteUpdate implements Protocol<Boolean> {

      Boolean result = null;

      @Override
      public Boolean getResult() {
        return result;
      }

      @Override
      public boolean isDone() {
        return result != null;
      }

      @Override
      public void incoming(SafeDataInputStream message) {
        result = message.readBoolean();
      }
    }
  }
}
