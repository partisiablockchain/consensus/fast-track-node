package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.consensus.fasttrack.AsyncBroadcastFromSignatures.MessageType;
import com.partisiablockchain.consensus.fasttrack.AsyncBroadcastFromSignatures.SignatureRequest;
import com.partisiablockchain.consensus.fasttrack.AsyncBroadcastFromSignatures.SignatureSet;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AsyncBroadcastFromSignaturesTest {

  private final Flooding flooding = new Flooding();
  private final byte[] broadcastId = new byte[1];
  private final List<KeyPair> keyPairs = VoteUpdatingProtocolTest.createKeyPairs();
  private final List<BlsKeyPair> blsKeyPairs = VoteUpdatingProtocolTest.createBlsKeyPairs();
  private final List<BlsPublicKey> blsPublicKeys =
      blsKeyPairs.stream().map(BlsKeyPair::getPublicKey).collect(Collectors.toList());
  private final List<BlockchainAddress> parties =
      keyPairs.stream()
          .map(KeyPair::getPublic)
          .map(BlockchainPublicKey::createAddress)
          .collect(Collectors.toList());

  private final KeyPair broadcastSigner = keyPairs.get(1);
  private final byte[] trueBroadcast = {1};
  private final byte[] falseBroadcast = {0};

  @Test
  public void fullTest() {
    BlockchainAddress broadcaster = keyPairs.get(2).getPublic().createAddress();
    List<AsyncBroadcastFromSignatures> protocols = new ArrayList<>();

    byte[] expectedBroadcast = Hash.create(stream -> stream.writeInt(77)).getBytes();
    for (int i = 0; i < parties.size(); i++) {
      PartyConfiguration configuration =
          new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(i), blsKeyPairs.get(i));
      BlockchainAddress party = keyPairs.get(i).getPublic().createAddress();
      if (broadcaster.equals(party)) {
        protocols.add(
            AsyncBroadcastFromSignatures.sendBroadcast(
                broadcastId, configuration, flooding::flood, expectedBroadcast));
      } else {
        protocols.add(
            AsyncBroadcastFromSignatures.receiveBroadcast(
                broadcastId, broadcaster, configuration, flooding::flood));
      }
    }

    while (flooding.hasNext()) {
      byte[] message = flooding.takeNext();
      for (AsyncBroadcastFromSignatures protocol : protocols) {
        protocol.incoming(SafeDataInputStream.createFromBytes(message));
      }
    }

    for (AsyncBroadcastFromSignatures protocol : protocols) {
      Assertions.assertThat(protocol.isDone()).isTrue();
      Assertions.assertThat(protocol.getResult()).isEqualTo(expectedBroadcast);
    }
  }

  @Test
  @SuppressWarnings("EnumOrdinal")
  public void receiveSignatureSet() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();

    SignatureSet signatureSet = createSignatureSet(broadcast, false);
    Assertions.assertThat(broadcast.isDone()).isFalse();
    broadcast.incoming(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeEnum(MessageType.SIGNATURE_SET);
                  signatureSet.write(stream);
                })));
    Assertions.assertThat(broadcast.isDone()).isTrue();
    Assertions.assertThat(broadcast.getResult()).isEqualTo(falseBroadcast);
    Assertions.assertThat(flooding.hasNext()).isTrue();
    byte[] bytes = flooding.takeNext();
    Assertions.assertThat(bytes).startsWith(MessageType.SIGNATURE_SET.ordinal());
    SignatureSet actual =
        SignatureSet.read(
            SafeDataInputStream.createFromBytes(Arrays.copyOfRange(bytes, 1, bytes.length)));
    Assertions.assertThat(actual.getRequest())
        .isNotNull()
        .usingRecursiveComparison()
        .isEqualTo(signatureSet.getRequest());
    Assertions.assertThat(actual.getSignatures())
        .isNotNull()
        .containsExactlyInAnyOrderElementsOf(signatureSet.getSignatures());
    Assertions.assertThat(flooding.hasNext()).isFalse();
  }

  @Test
  public void shouldFloodWhenCreated() {
    KeyPair myKey = keyPairs.get(0);
    AsyncBroadcastFromSignatures broadcast =
        AsyncBroadcastFromSignatures.sendBroadcast(
            broadcastId,
            new PartyConfiguration(parties, blsPublicKeys, myKey, blsKeyPairs.get(0)),
            flooding::flood,
            trueBroadcast);
    Signature expectedSignature = myKey.sign(broadcast.createHashForRequest(trueBroadcast));
    Assertions.assertThat(flooding.hasNext()).isTrue();
    Assertions.assertThat(flooding.takeNext())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeEnum(MessageType.SIGNATURE_REQUEST);
                  stream.writeDynamicBytes(trueBroadcast);
                  expectedSignature.write(stream);
                }));
    Assertions.assertThat(flooding.hasNext()).isFalse();
  }

  @Test
  public void collectSignatureShouldFlood() {
    KeyPair myKey = keyPairs.get(0);
    AsyncBroadcastFromSignatures broadcast =
        AsyncBroadcastFromSignatures.sendBroadcast(
            broadcastId,
            new PartyConfiguration(parties, blsPublicKeys, myKey, blsKeyPairs.get(0)),
            flooding::flood,
            trueBroadcast);
    flooding.takeNext();

    Signature expectedSignature =
        keyPairs.get(1).sign(broadcast.createHashForRequest(trueBroadcast));

    byte[] expectedFlood =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeEnum(MessageType.SIGNATURE);
              expectedSignature.write(stream);
            });
    broadcast.incoming(SafeDataInputStream.createFromBytes(expectedFlood));
    Assertions.assertThat(flooding.hasNext()).isTrue();
    Assertions.assertThat(flooding.takeNext()).isEqualTo(expectedFlood);
    Assertions.assertThat(flooding.hasNext()).isFalse();
    Assertions.assertThat(broadcast.isDone()).isFalse();
  }

  @Test
  public void ignoreSignatureFromBroadcaster() {
    KeyPair myKey = keyPairs.get(0);
    AsyncBroadcastFromSignatures broadcast =
        AsyncBroadcastFromSignatures.sendBroadcast(
            broadcastId,
            new PartyConfiguration(parties, blsPublicKeys, myKey, blsKeyPairs.get(0)),
            flooding::flood,
            trueBroadcast);
    flooding.takeNext();

    Signature expectedSignature = myKey.sign(broadcast.createHashForRequest(trueBroadcast));

    byte[] expectedFlood =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeEnum(MessageType.SIGNATURE);
              expectedSignature.write(stream);
            });
    broadcast.incoming(SafeDataInputStream.createFromBytes(expectedFlood));
    Assertions.assertThat(flooding.hasNext()).isFalse();
  }

  @Test
  public void signatureSetWithInvalidRequest() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();

    SignatureSet signatureSet =
        createSignatureSet(broadcast, true, keyPairs.get(0), keyPairs.get(2), keyPairs.get(3));

    broadcast.incoming(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeEnum(MessageType.SIGNATURE_SET);
                  signatureSet.write(stream);
                })));
    Assertions.assertThat(broadcast.isDone()).isFalse();
  }

  private AsyncBroadcastFromSignatures createBroadcast() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    BlockchainAddress party1 = keyPairs.get(1).getPublic().createAddress();
    return AsyncBroadcastFromSignatures.receiveBroadcast(
        broadcastId, party1, partyConfiguration, flooding::flood);
  }

  @Test
  public void signatureSetIgnoreInvalidSigner() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();

    SignatureSet signatureSet =
        createSignatureSet(broadcast, false, broadcastSigner, keyPairs.get(0), new KeyPair());

    broadcast.incoming(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeEnum(MessageType.SIGNATURE_SET);
                  signatureSet.write(stream);
                })));
    Assertions.assertThat(broadcast.isDone()).isFalse();
  }

  @Test
  public void shouldFloodRequestAndSign() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();
    Signature requestSignature =
        broadcastSigner.sign(broadcast.createHashForRequest(trueBroadcast));
    byte[] signatureRequest =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeEnum(MessageType.SIGNATURE_REQUEST);
              stream.writeDynamicBytes(trueBroadcast);
              requestSignature.write(stream);
            });
    broadcast.incoming(SafeDataInputStream.createFromBytes(signatureRequest));

    Signature expectedSignature =
        keyPairs.get(0).sign(broadcast.createHashForRequest(trueBroadcast));
    Assertions.assertThat(flooding.hasNext()).isTrue();
    Assertions.assertThat(flooding.takeNext()).isEqualTo(signatureRequest);
    Assertions.assertThat(flooding.hasNext()).isTrue();
    byte[] expectedSignatureMessage =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeEnum(MessageType.SIGNATURE);
              expectedSignature.write(stream);
            });
    Assertions.assertThat(flooding.takeNext()).isEqualTo(expectedSignatureMessage);
  }

  @Test
  public void invalidRequest() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();
    Signature requestSignature =
        keyPairs.get(0).sign(broadcast.createHashForRequest(trueBroadcast));
    byte[] signatureRequest =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeEnum(MessageType.SIGNATURE_REQUEST);
              stream.writeDynamicBytes(trueBroadcast);
              requestSignature.write(stream);
            });
    broadcast.incoming(SafeDataInputStream.createFromBytes(signatureRequest));
    Assertions.assertThat(flooding.hasNext()).isFalse();
  }

  @Test
  public void getResultBeforeReady() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();
    Assertions.assertThatThrownBy(() -> Objects.requireNonNull(broadcast.getResult()))
        .isInstanceOf(IllegalStateException.class);
  }

  @Test
  public void signatureSetShouldOverwritePreviousRequest() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();
    Signature requestSignature =
        broadcastSigner.sign(broadcast.createHashForRequest(trueBroadcast));
    byte[] signatureRequest =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeEnum(MessageType.SIGNATURE_REQUEST);
              stream.writeDynamicBytes(falseBroadcast);
              requestSignature.write(stream);
            });
    broadcast.incoming(SafeDataInputStream.createFromBytes(signatureRequest));

    broadcast.incoming(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeEnum(MessageType.SIGNATURE_SET);
                  createSignatureSet(broadcast, true).write(stream);
                })));
    Assertions.assertThat(broadcast.isDone()).isTrue();
    Assertions.assertThat(broadcast.getResult()).isEqualTo(trueBroadcast);
  }

  @Test
  public void requestHash() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();
    Hash expectedHash =
        Hash.create(
            stream -> {
              stream.write(broadcastId);
              parties.get(1).write(stream);
              stream.writeDynamicBytes(trueBroadcast);
            });
    Assertions.assertThat(broadcast.createHashForRequest(trueBroadcast)).isEqualTo(expectedHash);
  }

  @Test
  public void getBroadcaster() {
    AsyncBroadcastFromSignatures broadcast = createBroadcast();
    Assertions.assertThat(broadcast.getBroadcaster()).isEqualTo(parties.get(1));
  }

  private SignatureSet createSignatureSet(
      AsyncBroadcastFromSignatures broadcast,
      boolean broadcastValue,
      KeyPair broadcastSigner,
      KeyPair... signers) {
    byte[] broadcastBytes = broadcastValue ? trueBroadcast : falseBroadcast;
    Hash hash = broadcast.createHashForRequest(broadcastBytes);
    return new SignatureSet(
        new SignatureRequest(broadcastBytes, broadcastSigner.sign(hash)),
        Arrays.stream(signers).map(key -> key.sign(hash)).collect(Collectors.toList()));
  }

  private SignatureSet createSignatureSet(
      AsyncBroadcastFromSignatures broadcast, boolean broadcastValue) {
    SignatureSet signatureSet =
        createSignatureSet(
            broadcast, broadcastValue, keyPairs.get(1), keyPairs.get(0), keyPairs.get(2));
    Assertions.assertThat(signatureSet.getSignatures()).hasSize(2);
    return signatureSet;
  }
}
