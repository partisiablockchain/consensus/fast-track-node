package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FinalizationMessageHelperTest {

  @Test
  public void createMessage() {
    Hash hash = Hash.create(s -> s.writeInt(123));
    BlsSignature sign = BlockchainTestHelper.notPayable.blsKey.sign(hash);
    Hash message = FinalizationMessageHelper.createMessage(hash, sign);
    Assertions.assertThat(message)
        .isEqualTo(
            Hash.create(
                s -> {
                  s.write(hash.getBytes());
                  sign.write(s);
                  s.writeString(FinalizationMessageHelper.FINALIZATION_DOMAIN_SEPARATOR);
                }));
  }
}
