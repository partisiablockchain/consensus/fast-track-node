package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class BlockStateStorageTest {
  @TempDir private File temporaryFolder;
  private File root;
  private BlockchainLedger blockchain;

  @BeforeEach
  public void setup() {
    root = new File(temporaryFolder, "directory");
    blockchain = BlockchainTestHelper.createWithFastTrack(temporaryFolder.toPath()).blockchain;
  }

  @AfterEach
  public void close() {
    blockchain.close();
    Assertions.assertThat(root.delete()).isTrue();
  }

  @Test
  void truncateFileWhenInitialising() throws Exception {
    RandomAccessFile randomAccessFile = new RandomAccessFile(root, "rw");
    BlockStateStorage storage = BlockStateStorage.createEmpty(randomAccessFile);
    BlockchainLedger.BlockAndState latest = blockchain.latest();

    // Block with 10 transactions
    Block block =
        new Block(
            System.currentTimeMillis(),
            latest.getBlock().getBlockTime() + 1,
            latest.getBlock().getBlockTime(),
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            List.of(),
            Collections.nCopies(10, Block.GENESIS_PARENT),
            (short) 0);
    KeyPair producer = BlockchainTestHelper.firstProducer.key;
    Hash messageHash = ProducerHash.create(block);
    Signature producerSignature = producer.sign(messageHash);

    ConsensusState consensus = new ConsensusState(latest.getState());
    List<BlockchainPublicKey> verifiers = consensus.getCommittee();
    List<BlsPublicKey> verifierBlsKeys = consensus.getCommitteeBlsKeys();
    Function<Hash, byte[]> rawStateAccess = (Hash hash) -> blockchain.getStateStorage().read(hash);

    storage.setCurrentState(block, producerSignature, verifiers, verifierBlsKeys, rawStateAccess);
    // Block with 0 transactions
    messageHash = ProducerHash.create(block);
    producerSignature = producer.sign(messageHash);

    Block emptyBlock =
        new Block(
            System.currentTimeMillis(),
            latest.getBlock().getBlockTime() + 2,
            latest.getBlock().getProductionTime(),
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            List.of(),
            List.of(),
            (short) 0);
    storage.setCurrentState(
        emptyBlock, producerSignature, verifiers, verifierBlsKeys, rawStateAccess);

    randomAccessFile.close();

    randomAccessFile = new RandomAccessFile(root, "rw");
    BlockStateStorage reInitStorage =
        BlockStateStorage.createFromFile(randomAccessFile, List.of(latest), rawStateAccess);

    Assertions.assertThat(reInitStorage.hasValue()).isTrue();

    storage.close();
    reInitStorage.close();
    randomAccessFile.close();
  }
}
