package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.BroadcastCreator;
import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.JustifiedVote;
import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.MessageType;
import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.Vote;
import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.VoteResult;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class VoteUpdatingProtocolTest {

  private final byte[] protocolId = new byte[1];
  private final List<KeyPair> keyPairs = createKeyPairs();
  private final List<BlsKeyPair> blsKeyPairs = createBlsKeyPairs();
  private final List<BlsPublicKey> blsPublicKeys =
      blsKeyPairs.stream().map(BlsKeyPair::getPublicKey).collect(Collectors.toList());
  private final List<BlockchainAddress> parties =
      keyPairs.stream().map(KeyPair::getPublic).map(BlockchainPublicKey::createAddress).toList();
  private final Flooding flooding = new Flooding();

  static List<KeyPair> createKeyPairs() {
    return IntStream.range(1, 5).mapToObj(BigInteger::valueOf).map(KeyPair::new).toList();
  }

  static List<BlsKeyPair> createBlsKeyPairs() {
    return IntStream.range(11, 15).mapToObj(BigInteger::valueOf).map(BlsKeyPair::new).toList();
  }

  @Test
  public void detectingWithAgreement() {
    List<Boolean> updatedVotes =
        runFullTest(
            configuration ->
                VoteUpdatingProtocol.createDetecting(
                    configuration, protocolId, flooding::flood, false),
            true);
    Assertions.assertThat(updatedVotes).containsOnly(false);
  }

  @Test
  public void detectingWithoutAgreementExpectedFalse() {
    List<Boolean> updatedVotes =
        runFullTest(
            configuration ->
                VoteUpdatingProtocol.createDetecting(
                    configuration,
                    protocolId,
                    flooding::flood,
                    parties.indexOf(configuration.getMyAddress()) % 2 != 0),
            false);
    Assertions.assertThat(updatedVotes).containsOnly(false);
  }

  @Test
  public void detectingWithoutAgreementExpectedTrue() {
    List<Boolean> updatedVotes =
        runFullTest(
            configuration ->
                VoteUpdatingProtocol.createDetecting(
                    configuration,
                    protocolId,
                    flooding::flood,
                    parties.indexOf(configuration.getMyAddress()) % 2 == 0),
            false);
    Assertions.assertThat(updatedVotes).containsOnly(true);
  }

  @Test
  public void stabilizingWithAgreement() {
    List<Boolean> updatedVotes =
        runFullTest(
            configuration ->
                VoteUpdatingProtocol.createStabilizing(
                    configuration, protocolId, flooding::flood, () -> true, true),
            true);
    Assertions.assertThat(updatedVotes).containsOnly(true);
  }

  @Test
  public void stabilizingWithoutAgreement() {
    List<Boolean> result =
        runFullTest(
            configuration ->
                VoteUpdatingProtocol.createStabilizing(
                    configuration,
                    protocolId,
                    flooding::flood,
                    () -> parties.indexOf(configuration.getMyAddress()) % 2 == 0,
                    parties.indexOf(configuration.getMyAddress()) % 2 == 0),
            false);
    Assertions.assertThat(result).containsExactly(true, false, true, false);
  }

  @Test
  public void createHashForVote() {
    byte[] protocolId = {1, 2, 3, 8};
    Assertions.assertThat(VoteUpdatingProtocol.createHashForVote(protocolId, true))
        .isEqualTo(
            Hash.create(
                stream -> {
                  stream.writeString("VOTE");
                  stream.write(protocolId);
                  stream.writeBoolean(true);
                }));
  }

  @Test
  public void createBroadcastId() {
    byte[] protocolId = {1, 2, 3, 8};
    Assertions.assertThat(
            VoteUpdatingProtocol.createBroadcastId(
                protocolId, keyPairs.get(0).getPublic().createAddress()))
        .isEqualTo(
            Hash.create(
                stream -> {
                  stream.writeString("JUSTIFIED_VOTE");
                  stream.write(protocolId);
                  parties.get(0).write(stream);
                }));
  }

  @Test
  public void justifiedMessageWhileWaitingForVotes() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    VoteUpdatingProtocol voteUpdatingProtocol =
        VoteUpdatingProtocol.createDetecting(partyConfiguration, protocolId, flooding::flood, true);
    voteUpdatingProtocol.incoming(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                stream -> stream.writeEnum(MessageType.JUSTIFIED_VOTE))));
  }

  @Test
  public void shouldFloodWhenCreated() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    VoteUpdatingProtocol.createDetecting(partyConfiguration, protocolId, flooding::flood, true);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
    Assertions.assertThat(flooding.takeNext())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeEnum(MessageType.VOTE);
                  stream.writeBoolean(true);
                  keyPairs
                      .get(0)
                      .sign(VoteUpdatingProtocol.createHashForVote(protocolId, true))
                      .write(stream);
                }));
  }

  @Test
  public void shouldIgnoreNonParticipatingSigner() {
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    VoteUpdatingProtocol protocol =
        VoteUpdatingProtocol.createDetecting(partyConfiguration, protocolId, flooding::flood, true);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
    protocol.incoming(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeEnum(MessageType.VOTE);
                  stream.writeBoolean(true);
                  new KeyPair()
                      .sign(VoteUpdatingProtocol.createHashForVote(protocolId, true))
                      .write(stream);
                })));
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
  }

  @Test
  public void invalidJustifiedVote() {
    MyBroadcastCreator broadcastCreator = new MyBroadcastCreator();
    PartyConfiguration partyConfiguration =
        new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(0), blsKeyPairs.get(0));
    VoteUpdatingProtocol voteUpdatingProtocol =
        VoteUpdatingProtocol.createDetectingInternal(
            partyConfiguration, protocolId, flooding::flood, true, broadcastCreator);
    List<Signature> signatures = new ArrayList<>();
    for (int i = 0; i < partyConfiguration.honestCount(); i++) {
      Hash hashForVote = VoteUpdatingProtocol.createHashForVote(protocolId, true);
      Signature signature = keyPairs.get(i).sign(hashForVote);
      signatures.add(signature);
      voteUpdatingProtocol.incoming(
          SafeDataInputStream.createFromBytes(
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeEnum(MessageType.VOTE);
                    new Vote(true, signature).write(stream);
                  })));
    }
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(partyConfiguration.honestCount());
    flooding.runToEmpty(List.of(voteUpdatingProtocol));
    Assertions.assertThat(voteUpdatingProtocol.collectingSignatures()).isFalse();
    Assertions.assertThat(voteUpdatingProtocol.collectingJustifiedVotes()).isTrue();

    JustifiedVote validJustifiedVote = new JustifiedVote(true, signatures);
    Assertions.assertThat(validJustifiedVote.isValid(partyConfiguration, protocolId)).isTrue();
    JustifiedVote invalidJustifiedVote = new JustifiedVote(false, signatures);
    Assertions.assertThat(invalidJustifiedVote.isValid(partyConfiguration, protocolId)).isFalse();

    broadcastCreator.floods.get(0).accept(invalidJustifiedVote::write);
    for (int i = 1; i < partyConfiguration.honestCount(); i++) {
      broadcastCreator.floods.get(i).accept(validJustifiedVote::write);
    }
    flooding.runToEmpty(List.of(voteUpdatingProtocol));
    Assertions.assertThat(voteUpdatingProtocol.collectingJustifiedVotes()).isTrue();
    // Should ignore any updates to an already finished broadcast
    broadcastCreator.floods.get(0).accept(validJustifiedVote::write);
    flooding.runToEmpty(List.of(voteUpdatingProtocol));
    Assertions.assertThat(voteUpdatingProtocol.collectingJustifiedVotes()).isTrue();

    broadcastCreator.floods.get(partyConfiguration.honestCount()).accept(validJustifiedVote::write);
    flooding.runToEmpty(List.of(voteUpdatingProtocol));
    Assertions.assertThat(voteUpdatingProtocol.collectingJustifiedVotes()).isFalse();
    Assertions.assertThat(voteUpdatingProtocol.isDone()).isTrue();
    VoteResult result = voteUpdatingProtocol.getResult();
    Assertions.assertThat(result.isHardVote()).isTrue();
    Assertions.assertThat(result.getVote()).isTrue();
  }

  private List<Boolean> runFullTest(
      Function<PartyConfiguration, VoteUpdatingProtocol> creator, boolean isHardVote) {
    List<VoteUpdatingProtocol> protocols = new ArrayList<>();

    for (int i = 0; i < parties.size(); i++) {
      PartyConfiguration configuration =
          new PartyConfiguration(parties, blsPublicKeys, keyPairs.get(i), blsKeyPairs.get(i));
      protocols.add(creator.apply(configuration));
    }

    flooding.runToEmpty(protocols);

    List<Boolean> updatedVotes = new ArrayList<>();
    for (VoteUpdatingProtocol protocol : protocols) {
      Assertions.assertThat(protocol.isDone()).isTrue();
      VoteResult result = protocol.getResult();
      Assertions.assertThat(result.isHardVote()).isEqualTo(isHardVote);
      updatedVotes.add(result.getVote());
    }
    return updatedVotes;
  }

  private static final class MyBroadcastCreator implements BroadcastCreator {

    private final List<Consumer<DataStreamSerializable>> floods = new ArrayList<>();

    @Override
    public Protocol<byte[]> sendBroadcast(
        byte[] broadcastId,
        PartyConfiguration partyConfiguration,
        Consumer<DataStreamSerializable> flood,
        byte[] broadcastValue) {
      floods.add(flood);
      return new TestBroadcastProtocol(partyConfiguration);
    }

    @Override
    public Protocol<byte[]> receiveBroadcast(
        byte[] broadcastId,
        BlockchainAddress broadcaster,
        PartyConfiguration partyConfiguration,
        Consumer<DataStreamSerializable> flood) {
      floods.add(flood);
      return new TestBroadcastProtocol(partyConfiguration);
    }

    private static final class TestBroadcastProtocol implements Protocol<byte[]> {

      private final PartyConfiguration configuration;
      JustifiedVote result = null;

      private TestBroadcastProtocol(PartyConfiguration configuration) {
        this.configuration = configuration;
      }

      @Override
      public byte[] getResult() {
        return SafeDataOutputStream.serialize(result::write);
      }

      @Override
      public boolean isDone() {
        return result != null;
      }

      @Override
      public void incoming(SafeDataInputStream message) {
        result = JustifiedVote.read(configuration, message);
      }
    }
  }
}
