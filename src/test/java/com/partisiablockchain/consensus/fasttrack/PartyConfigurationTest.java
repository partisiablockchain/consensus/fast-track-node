package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PartyConfigurationTest {

  private final List<KeyPair> keyPairs =
      IntStream.range(1, 10).mapToObj(BigInteger::valueOf).map(KeyPair::new).toList();
  private final List<BlsKeyPair> blsKeyPairs =
      IntStream.range(11, 20).mapToObj(BigInteger::valueOf).map(BlsKeyPair::new).toList();
  private final List<BlockchainAddress> parties =
      keyPairs.stream()
          .map(KeyPair::getPublic)
          .map(BlockchainPublicKey::createAddress)
          .collect(Collectors.toList());
  private final List<BlsPublicKey> blsPublicKeys =
      blsKeyPairs.stream().map(BlsKeyPair::getPublicKey).collect(Collectors.toList());
  private final PartyConfiguration fourParty =
      new PartyConfiguration(
          parties.subList(0, 4),
              blsKeyPairs.subList(0, 4).stream()
                  .map(BlsKeyPair::getPublicKey)
                  .collect(Collectors.toList()),
          keyPairs.get(1), blsKeyPairs.get(1));
  private final PartyConfiguration sevenParty =
      new PartyConfiguration(
          parties.subList(0, 7),
          blsKeyPairs.subList(0, 7).stream()
              .map(BlsKeyPair::getPublicKey)
              .collect(Collectors.toList()),
          keyPairs.get(1),
          blsKeyPairs.get(1));

  @Test
  public void validationConstructor() {
    KeyPair keyPair0 = keyPairs.get(0);
    BlsKeyPair blsKey0 = blsKeyPairs.get(0);

    PartyConfiguration partyConfiguration5 =
        new PartyConfiguration(
            parties.subList(0, 5), blsPublicKeys.subList(0, 5), keyPair0, blsKey0);

    Assertions.assertThat(partyConfiguration5.maliciousCount()).isEqualTo(1);

    PartyConfiguration partyConfiguration6 =
        new PartyConfiguration(
            parties.subList(0, 6), blsPublicKeys.subList(0, 6), keyPair0, blsKey0);

    Assertions.assertThat(partyConfiguration6.maliciousCount()).isEqualTo(1);

    KeyPair keyPair4 = keyPairs.get(4);
    BlsKeyPair blsKey4 = blsKeyPairs.get(4);
    Assertions.assertThatThrownBy(
            () ->
                new PartyConfiguration(
                    parties.subList(0, 4), blsPublicKeys.subList(0, 4), keyPair4, blsKey4))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("not participating");

    Assertions.assertThatThrownBy(
            () ->
                new PartyConfiguration(
                    parties.subList(0, 3), blsPublicKeys.subList(0, 3), keyPair0, blsKey0))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void honestCount() {
    Assertions.assertThat(fourParty.honestCount()).isEqualTo(3);
    Assertions.assertThat(sevenParty.honestCount()).isEqualTo(5);
  }

  @Test
  public void maliciousCount() {
    Assertions.assertThat(fourParty.maliciousCount()).isEqualTo(1);
    Assertions.assertThat(sevenParty.maliciousCount()).isEqualTo(2);
  }

  @Test
  public void partyCount() {
    Assertions.assertThat(fourParty.partyCount()).isEqualTo(4);
    Assertions.assertThat(sevenParty.partyCount()).isEqualTo(7);
  }
}
