package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;

final class Flooding {

  private final HashSet<ByteBuffer> seen = new HashSet<>();
  private final Deque<byte[]> floods = new ArrayDeque<>();

  void flood(Consumer<SafeDataOutputStream> messageBuilder) {
    byte[] bytes = SafeDataOutputStream.serialize(messageBuilder);
    ByteBuffer wrappedForEquality = ByteBuffer.wrap(bytes);
    if (this.seen.add(wrappedForEquality)) {
      this.floods.add(bytes);
    }
  }

  boolean hasNext() {
    return !floods.isEmpty();
  }

  byte[] takeNext() {
    return floods.removeFirst();
  }

  void runToEmpty(List<? extends Protocol<?>> receivers) {
    while (hasNext()) {
      floodNext(receivers);
    }
  }

  void floodNext(List<? extends Protocol<?>> receivers) {
    byte[] message = takeNext();
    for (Protocol<?> protocol : receivers) {
      protocol.incoming(SafeDataInputStream.createFromBytes(message));
    }
  }

  int pendingSize() {
    return floods.size();
  }
}
