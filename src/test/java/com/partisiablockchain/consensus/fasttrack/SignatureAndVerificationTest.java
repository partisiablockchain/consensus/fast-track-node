package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import java.math.BigInteger;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SignatureAndVerificationTest {

  @Test
  public void equals() {
    BlsKeyPair k = new BlsKeyPair(BigInteger.TWO);
    BlsSignature s0 = k.sign(Hash.create(stream -> stream.writeInt(123)));
    BlsSignature s1 = k.sign(Hash.create(stream -> stream.writeInt(321)));
    EqualsVerifier.forClass(SignatureAndVerification.class)
        .withNonnullFields("signature", "blsSignature", "proof")
        .withPrefabValues(BlsSignature.class, s0, s1)
        .verify();
  }
}
