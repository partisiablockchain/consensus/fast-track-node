package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockAndStateWithParent;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.consensus.fasttrack.ShutdownState.ShutdownMessage;
import com.partisiablockchain.consensus.fasttrack.ShutdownState.ShutdownStage;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.coverage.WithCloseableResources;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShutdownStateTest {

  private final AtomicInteger syncRequested = new AtomicInteger();
  private Path temporaryFolder;
  private final List<KeyPair> keyPairs =
      List.of(
          BlockchainTestHelper.firstProducer.key,
          BlockchainTestHelper.secondProducer.key,
          BlockchainTestHelper.thirdProducer.key,
          BlockchainTestHelper.fourthProducer.key,
          BlockchainTestHelper.fifthProducer.key,
          BlockchainTestHelper.sixthProducer.key);
  private final List<BlsKeyPair> blsKeyPairs =
      List.of(
          BlockchainTestHelper.firstProducer.blsKey,
          BlockchainTestHelper.secondProducer.blsKey,
          BlockchainTestHelper.thirdProducer.blsKey,
          BlockchainTestHelper.fourthProducer.blsKey,
          BlockchainTestHelper.fifthProducer.blsKey,
          BlockchainTestHelper.sixthProducer.blsKey);
  private final Flooding flooding = new Flooding();
  private File dataFile;
  BlockchainTestHelper fastTrack;
  private ShutdownState shutdownState;

  /**
   * Setup temporary files for use in test.
   *
   * @throws IOException on error
   */
  @BeforeEach
  public void setUp() throws IOException {
    temporaryFolder = Files.createTempDirectory("junit-fasttrack");
    dataFile = randomFile();
  }

  @AfterEach
  void tearDown() {
    if (shutdownState != null) {
      shutdownState.close();
    }
    if (fastTrack != null) {
      fastTrack.blockchain.close();
    }
  }

  @Test
  public void fullTest() throws Exception {
    List<File> dataFiles = keyPairs.stream().map(ignored -> randomFile()).toList();
    List<ShutdownState> protocols = new ArrayList<>();
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);

    runFullShutdown(dataFiles, protocols, fastTrack, 1);
    // Listener is invoked async - sleep a bit
    Thread.sleep(100);
    for (ShutdownState protocol : protocols) {
      Assertions.assertThat(protocol.isShuttingDown()).isFalse();
      Assertions.assertThat(protocol.isStorageEmpty()).isTrue();
      // make pitest happy
      Assertions.assertThat(protocol.resources().findAny().isPresent()).isTrue();
    }
    protocols.forEach(WithCloseableResources::close);
  }

  private File randomFile() {
    return new File(temporaryFolder.toFile(), UUID.randomUUID().toString());
  }

  @Test
  public void fullTestSharded() {
    List<File> dataFiles =
        keyPairs.stream()
            .map(ignored -> new File(temporaryFolder.toFile(), UUID.randomUUID().toString()))
            .collect(Collectors.toList());
    List<ShutdownState> protocols = new ArrayList<>();
    File shard = newFolder("shard");
    fastTrack =
        BlockchainTestHelper.createWithShardedFastTrack(
            BlockchainTestHelper.ENABLED_SHARD_ID, shard);

    runFullShutdown(dataFiles, protocols, fastTrack, 1);
  }

  private File newFolder(String shard) {
    File file = new File(temporaryFolder.toFile(), shard);
    Assertions.assertThat(file.mkdirs()).isTrue();
    return file;
  }

  private void runFullShutdown(
      List<File> dataFiles,
      List<ShutdownState> protocols,
      BlockchainTestHelper fastTrack,
      int expectedShutdownBlocks) {
    Block blockBefore = fastTrack.blockchain.getLatestBlock();
    for (int i = 0; i < keyPairs.size(); i++) {
      KeyPair keyPair = keyPairs.get(i);
      BlsKeyPair blsKeyPair = blsKeyPairs.get(i);
      ShutdownState shutdownState =
          new ShutdownState(
              fastTrack.blockchain,
              dataFiles.get(i),
              keyPair,
              blsKeyPair,
              syncRequested::getAndIncrement,
              flooding::flood,
              60_000,
              60_000);
      shutdownState.triggerShutdown();
      protocols.add(shutdownState);
    }

    boolean triggeredShutdown = false;
    while (flooding.hasNext()) {
      byte[] bytes = flooding.takeNext();
      for (ShutdownState protocol : protocols) {
        protocol.incoming(SafeDataInputStream.createFromBytes(bytes));
        if (!triggeredShutdown && protocols.get(0).getStage() == ShutdownStage.CREATE_BLOCK) {
          System.out.println("Shutting down nodes and restarting");
          triggeredShutdown = true;
          for (int i = 0; i < 3; i++) {
            protocols.get(i).close();
            ShutdownState rebooted =
                new ShutdownState(
                    fastTrack.blockchain,
                    dataFiles.get(i),
                    keyPairs.get(i),
                    blsKeyPairs.get(i),
                    syncRequested::getAndIncrement,
                    flooding::flood,
                    60_000,
                    60_000);
            Assertions.assertThat(rebooted.isShuttingDown()).isTrue();
            protocols.set(i, rebooted);
          }
        }
      }
    }
    Block latestBlock = fastTrack.blockchain.getLatestBlock();
    Assertions.assertThat(latestBlock.getBlockTime())
        .isEqualTo(blockBefore.getBlockTime() + expectedShutdownBlocks);
  }

  @Test
  public void ignoreSignatureOnOldBlock() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    final long blockTime = fastTrack.blockchain.getBlockTime();
    appendBlocks(fastTrack, 2);
    Assertions.assertThat(fastTrack.blockchain.getBlockTime()).isEqualTo(blockTime + 1);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    shutdownState.triggerShutdown();
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
    incomingSignature(shutdownState, keyPairs.get(1), blockTime);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
  }

  @Test
  public void floodOnlySignatureOnce() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    appendBlocks(fastTrack, 10);
    final long blockTime = fastTrack.blockchain.getBlockTime();
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    shutdownState.triggerShutdown();
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
    incomingSignature(shutdownState, keyPairs.get(1), blockTime);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(2);
    incomingSignature(shutdownState, keyPairs.get(1), blockTime);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(2);
  }

  @Test
  public void createOwnSignatureOnNewBlock() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    shutdownState.triggerShutdown();
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
    appendBlocks(fastTrack, 1);
    // Mimic that the listener is invoked to avoid waiting for async listener
    shutdownState.newBlockProposal(fastTrack.blockchain.getProposals().get(0).getBlock());
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(2);
  }

  @Test
  public void abortsShutdownAfterEnoughBlocks() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000L,
            60_000L);
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.isShuttingDown()).isTrue();
    long startBlockTime = fastTrack.blockchain.getBlockTime();
    newBlockProposal(fastTrack, 3, startBlockTime);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.ABORTING);
    Assertions.assertThat(shutdownState.isStorageEmpty()).isFalse();
    incomingSignature(shutdownState, keyPairs.get(1), fastTrack.blockchain.getBlockTime());
    newBlockProposal(fastTrack, 1, startBlockTime + 3);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.ABORTING);
    Assertions.assertThat(shutdownState.isStorageEmpty()).isFalse();
    newBlockProposal(fastTrack, 1, startBlockTime + 4);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.WAITING);
    Assertions.assertThat(shutdownState.isStorageEmpty()).isTrue();
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.COLLECT_SIGNATURES);
    newBlockProposal(fastTrack, 1, startBlockTime + 5);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.COLLECT_SIGNATURES);
  }

  @Test
  public void livenessMonitorActivatesSignaturesAgain() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000L,
            60_000L);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(0);
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.isShuttingDown()).isTrue();
    appendBlocks(fastTrack, 1);
    shutdownState.newBlockProposal(fastTrack.blockchain.getProposals().get(0).getBlock());
    appendBlocks(fastTrack, 1);
    shutdownState.newBlockProposal(fastTrack.blockchain.getProposals().get(0).getBlock());
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(3);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.COLLECT_SIGNATURES);
    appendBlocks(fastTrack, 1);
    shutdownState.newBlockProposal(fastTrack.blockchain.getProposals().get(0).getBlock());
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.ABORTING);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(3);
    incomingSignature(shutdownState, keyPairs.get(1), fastTrack.blockchain.getBlockTime());
    // Mimic liveness monitor triggers shutdown
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.isShuttingDown()).isTrue();
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.COLLECT_SIGNATURES);
    // our own signature + keypair1 + 3 previous.
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(5);
    newBlockProposal(fastTrack, 6, fastTrack.blockchain.getBlockTime());
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.WAITING);
    Assertions.assertThat(shutdownState.isStorageEmpty()).isTrue();
  }

  @Test
  public void shutdownTriggerWhileAbortingGoesToCutOff() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000L,
            60_000L);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(0);
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.isShuttingDown()).isTrue();
    long blockTime = fastTrack.blockchain.getBlockTime();
    newBlockProposal(fastTrack, 3, blockTime);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.ABORTING);
    for (int i = 0; i < 3; i++) {
      incomingSignature(shutdownState, keyPairs.get(i), blockTime + 4);
    }
    // Mimic liveness monitor triggers shutdown.
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.isShuttingDown()).isTrue();
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.DETERMINE_CUT_OFF);
  }

  @Test
  public void checkSignatureCountDoesNothingWhenWaiting() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    for (int i = 0; i < 3; i++) {
      incomingSignature(shutdownState, keyPairs.get(i), fastTrack.blockchain.getBlockTime());
    }
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.WAITING);
    shutdownState.checkSignatureCount();
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.WAITING);
  }

  @Test
  public void collectEnoughSignaturesBeforeTriggeringShutdown() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    for (int i = 0; i < 3; i++) {
      incomingSignature(shutdownState, keyPairs.get(i), fastTrack.blockchain.getBlockTime());
    }
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.DETERMINE_CUT_OFF);
  }

  @Test
  public void collectEnoughSignaturesBeforeTriggeringShutdownSixMembers() {
    fastTrack = BlockchainTestHelper.createWithFastTrackHavingSixMembers(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    for (int i = 0; i < 4; i++) {
      incomingSignature(shutdownState, keyPairs.get(i), fastTrack.blockchain.getBlockTime());
    }
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.COLLECT_SIGNATURES);
    incomingSignature(shutdownState, keyPairs.get(4), fastTrack.blockchain.getBlockTime());
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.DETERMINE_CUT_OFF);
  }

  @Test
  public void ignoreNotParticipating() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    final long blockTime = fastTrack.blockchain.getBlockTime();
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    shutdownState.triggerShutdown();
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
    incomingSignature(shutdownState, new KeyPair(), blockTime);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
  }

  @Test
  public void shutdownHash() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    Assertions.assertThat(shutdownState.shutdownHash(27L))
        .isEqualTo(
            Hash.create(
                stream -> {
                  stream.writeString("SHUTDOWN");
                  deriveProtocolIdentifier(shutdownState).write(stream);
                  stream.writeLong(27L);
                }));
  }

  @Test
  public void shutdownHashComponents() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    Hash hash = shutdownState.shutdownHash(27L);
    Assertions.assertThat(hash.toString())
        .isEqualToIgnoringCase("f05143cc5fc9192b1e3183b7da45163c4cfe8207aa03f4376912ce4aff3b970d");
  }

  @Test
  public void signatureWhenNotExpected() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            new KeyPair(),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);

    incomingSignature(shutdownState, keyPairs.get(0), fastTrack.blockchain.getBlockTime());
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(0);
  }

  @Test
  public void messageWhenCollecting() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);

    incoming(shutdownState, ShutdownMessage.CUT_OFF, FunctionUtility.noOpConsumer());
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(0);
  }

  @Test
  public void collectSignaturesWhileWaitingAndFloodOnTriggerShutdown() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    Assertions.assertThat(shutdownState.getShutdownSigners()).hasSize(0);
    incomingSignature(shutdownState, keyPairs.get(1), fastTrack.blockchain.getBlockTime());
    Assertions.assertThat(shutdownState.getShutdownSigners()).hasSize(1);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(0);
    Assertions.assertThat(shutdownState.getStorageLength()).isEqualTo(0);
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.getStorageLength()).isEqualTo(dataFile.length());
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(2);
  }

  @Test
  public void missingBlockAfterCutOffVote() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            (partyConfiguration, flood, protocolId, maximalBlockTime, generator) ->
                new FixedCutOffVote(),
            dataFile,
            fastTrack.blockchain,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.WAITING);
    Block latestBlock = fastTrack.blockchain.getLatestBlock();
    long blockTime = latestBlock.getBlockTime();
    for (int i = 0; i < 3; i++) {
      Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.WAITING);
      incomingSignature(shutdownState, keyPairs.get(i), blockTime);
    }
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.DETERMINE_CUT_OFF);
    incoming(shutdownState, ShutdownMessage.CUT_OFF, stream -> stream.writeLong(blockTime + 1));
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.CREATE_BLOCK);
    // Should ignore incoming RESET_BLOCK when the block has not yet been created
    incoming(shutdownState, ShutdownMessage.RESET_BLOCK, FunctionUtility.noOpConsumer());

    int floodedSoFar = flooding.pendingSize();
    Block block =
        new Block(
            System.currentTimeMillis(),
            blockTime + 1,
            latestBlock.getCommitteeId(),
            latestBlock.identifier(),
            latestBlock.getState(),
            List.of(),
            List.of(),
            0);
    fastTrack.blockchain.appendBlock(FastTrackProducerTest.createFinalBlock(block));
    shutdownState.newBlockProposal(block);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(floodedSoFar + 1);
    Block resetBlock = shutdownState.getResetBlocks();
    Hash resetHash = resetBlock.identifier();
    for (int i = 1; i < 3; i++) {
      KeyPair keyPair = keyPairs.get(i);
      BlsKeyPair blsKeyPair = blsKeyPairs.get(i);
      BlsSignature sign = blsKeyPair.sign(resetHash);
      incoming(
          shutdownState,
          ShutdownMessage.RESET_BLOCK,
          s -> {
            keyPair.sign(FinalizationMessageHelper.createMessage(resetHash, sign)).write(s);
            sign.write(s);
          });
    }
    Assertions.assertThat(fastTrack.blockchain.getLatestBlock().getBlockTime())
        .isEqualTo(blockTime + 2);
  }

  @Test
  public void fastTrackProducersDoesNotProduceGivenNewAvailableCommittee() {
    fastTrack = BlockchainTestHelper.createWithFastTrackThreeCommittees(temporaryFolder);
    BlockAndState latest = fastTrack.blockchain.latest();
    Block latestBlock = latest.getBlock();
    Block block =
        new Block(
            System.currentTimeMillis(),
            latestBlock.getBlockTime() + 1,
            latestBlock.getCommitteeId(),
            latestBlock.identifier(),
            latestBlock.getState(),
            List.of(),
            List.of(),
            -1);
    fastTrack.blockchain.appendBlock(FastTrackProducerTest.createFinalBlock(block));

    FastTrackProducer blockProducer =
        new FastTrackProducer(
            MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
            fastTrack.blockchain,
            BlockchainTestHelper.firstProducer.key,
            BlockchainTestHelper.firstProducer.blsKey,
            100,
            1_000,
            new LoggingTransactionSender(),
            new DummyAdditionalStatusProviders());

    latest = fastTrack.blockchain.latest();
    ConsensusState consensus = new ConsensusState(latest.getState());
    Assertions.assertThat(consensus.getActiveCommittee()).isEqualTo(1);

    Assertions.assertThat(blockProducer.shouldProduceBlock(latest.getBlock(), consensus)).isFalse();
    blockProducer.close();
  }

  @Test
  public void newBlockProposalWithNoProposal() {
    fastTrack = BlockchainTestHelper.createWithFastTrackThreeCommittees(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    BlockAndState latest = fastTrack.blockchain.latest();
    Block block =
        new Block(
            latest.getBlock().getProductionTime() + 1,
            latest.getBlock().getBlockTime() + 1,
            latest.getBlock().getCommitteeId(),
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            List.of(),
            List.of(),
            -1);
    shutdownState.newBlockProposal(block);
    Assertions.assertThat(shutdownState.isShuttingDown()).isFalse();
  }

  @Test
  public void newCommitteeResetsBlocksSeendoingShutdown() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    shutdownState.triggerShutdown();
    long blockTime = fastTrack.blockchain.getBlockTime();
    newBlockProposal(fastTrack, 3, blockTime);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.ABORTING);
    newBlockProposal(fastTrack, 1, blockTime + 3);
    shutdownState.triggerShutdown();
    newBlockProposal(fastTrack, 1, blockTime + 4);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.COLLECT_SIGNATURES);
  }

  @Test
  public void newCommitteeTriggersShutDown() {
    fastTrack = BlockchainTestHelper.createWithFastTrackThreeCommittees(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    Block block = createBlockFromLatest(fastTrack.blockchain.latest());
    FinalBlock finalBlock = FastTrackProducerTest.createFinalBlock(block);
    fastTrack.blockchain.appendBlock(finalBlock);

    shutdownState.newBlockProposal(block);
    Assertions.assertThat(shutdownState.isShuttingDown()).isTrue();
    fastTrack.blockchain.close();
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    final ShutdownState newShutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    block = createBlockFromLatest(fastTrack.blockchain.latest());
    finalBlock = FastTrackProducerTest.createFinalBlock(block);
    fastTrack.blockchain.appendBlock(finalBlock);

    newShutdownState.newBlockProposal(block);
    Assertions.assertThat(newShutdownState.isShuttingDown()).isFalse();
    newShutdownState.close();
  }

  @Test
  public void triggeringListenerShouldOnlyClearStorageWhenNewCommittee() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    shutdownState.triggerShutdown();
    BlockAndState latest = fastTrack.blockchain.latest();
    shutdownState.newFinalBlock(latest.getBlock(), latest.getState());
    Assertions.assertThat(shutdownState.isStorageEmpty()).isFalse();
  }

  @Test
  public void latestEvent() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    long latestEvent = System.currentTimeMillis();
    shutdownState.setLatestEvent(latestEvent);
    Assertions.assertThat(shutdownState.getLatestEvent()).isEqualTo(latestEvent);
  }

  @Test
  public void shutdownTimeout() {
    Assertions.assertThat(ShutdownState.shutdownTimeout(13, 17)).isEqualTo(2 * 13 + 17 * 5);
  }

  @Test
  public void shutdownLiveness() {
    Assertions.assertThat(ShutdownState.shutdownLiveness(17)).isEqualTo(17 * 10);
  }

  @Test
  public void determineCutOffShouldHaveSameSeedAfterInitializingFromStorage() {
    AtomicReference<byte[]> firstBytes = new AtomicReference<>();
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            (partyConfiguration, flood, protocolId, maximalBlockTime, generator) -> {
              firstBytes.set(randomBytes(generator));
              return new FixedCutOffVote();
            },
            dataFile,
            fastTrack.blockchain,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.WAITING);
    shutdownState.triggerShutdown();
    Block latestBlock = fastTrack.blockchain.getLatestBlock();
    long blockTime = latestBlock.getBlockTime();
    for (int i = 0; i < 3; i++) {
      Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.COLLECT_SIGNATURES);
      incomingSignature(shutdownState, keyPairs.get(i), blockTime);
    }
    Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.DETERMINE_CUT_OFF);
    shutdownState.close();

    AtomicReference<byte[]> secondBytes = new AtomicReference<>();
    ShutdownState restarted =
        new ShutdownState(
            (partyConfiguration, flood, protocolId, maximalBlockTime, generator) -> {
              secondBytes.set(randomBytes(generator));
              return new FixedCutOffVote();
            },
            dataFile,
            fastTrack.blockchain,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    byte[] bytesFromEmptySeed = randomBytes(new DeterministicRandomBitGenerator(new byte[32]));
    Assertions.assertThat(restarted.getStage()).isEqualTo(ShutdownStage.DETERMINE_CUT_OFF);
    Assertions.assertThat(secondBytes)
        .hasValue(firstBytes.get())
        .doesNotHaveValue(bytesFromEmptySeed);
    restarted.close();
  }

  private byte[] randomBytes(DeterministicRandomBitGenerator generator) {
    byte[] bytes = new byte[32];
    generator.nextBytes(bytes);
    return bytes;
  }

  @Test
  public void resetStorageWhenOutdated() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.isStorageEmpty()).isFalse();
    shutdownState.close();
    appendResetBlock(fastTrack);
    ShutdownState rebooted =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    Assertions.assertThat(rebooted.isShuttingDown()).isFalse();
    Assertions.assertThat(rebooted.isStorageEmpty()).isTrue();
    rebooted.close();
  }

  @Test
  public void sendSendResponse() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    Assertions.assertThat(shutdownState.isShuttingDown()).isFalse();
    final List<byte[]> syncMessages = new ArrayList<>();
    shutdownState.sendSync(
        m -> syncMessages.add(SafeDataOutputStream.serialize(m)), ShutdownStage.NOT_PARTICIPATING);
    Assertions.assertThat(syncMessages).hasSize(0);

    shutdownState.triggerShutdown();
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);

    shutdownState.sendSync(
        m -> syncMessages.add(SafeDataOutputStream.serialize(m)), ShutdownStage.NOT_PARTICIPATING);

    Assertions.assertThat(syncMessages).hasSize(1);
    Assertions.assertThat(flooding.pendingSize()).isEqualTo(1);
    Assertions.assertThat(syncMessages.get(0)).isEqualTo(flooding.takeNext());
  }

  @Test
  public void abortingResets() {
    AtomicReference<byte[]> bytes = new AtomicReference<>();
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            (partyConfiguration, flood, protocolId, maximalBlockTime, generator) -> {
              bytes.set(randomBytes(generator));
              return new FixedCutOffVote();
            },
            dataFile,
            fastTrack.blockchain,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);

    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.isStorageEmpty()).isFalse();
    Block latestBlock = fastTrack.blockchain.getLatestBlock();
    long blockTime = latestBlock.getBlockTime();
    for (int i = 0; i < 3; i++) {
      Assertions.assertThat(shutdownState.getStage()).isEqualTo(ShutdownStage.COLLECT_SIGNATURES);
      incomingSignature(shutdownState, keyPairs.get(i), blockTime);
    }
    final byte[] beforeShutdown = Arrays.copyOf(shutdownState.getRandomSeed(), 32);
    shutdownState.abortShutdown();
    Assertions.assertThat(shutdownState.isStorageEmpty()).isTrue();
    byte[] randomSeed = shutdownState.getRandomSeed();
    Assertions.assertThat(randomSeed).isNotNull();
    Assertions.assertThat(bytes.get()).isNotEqualTo(randomSeed);
    Assertions.assertThat(beforeShutdown).isNotEqualTo(randomSeed);
  }

  @Test
  public void triggerLiveness() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    shutdownState.triggerLiveness();
    Assertions.assertThat(syncRequested).hasValue(0);

    shutdownState.triggerShutdown();
    Assertions.assertThat(syncRequested).hasValue(0);

    shutdownState.triggerLiveness();
    Assertions.assertThat(syncRequested).hasValue(1);
  }

  @Test
  public void bumpLatestEventOnFinalBlock() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    appendResetBlock(fastTrack);
    BlockAndState latest = fastTrack.blockchain.latest();

    long before = getLatestEventAndWaitForLocalClock(shutdownState);
    shutdownState.newFinalBlock(latest.getBlock(), latest.getState());
    Assertions.assertThat(shutdownState.getLatestEvent()).isGreaterThan(before);
  }

  private long getLatestEventAndWaitForLocalClock(ShutdownState shutdownState) {
    long latestEvent = shutdownState.getLatestEvent();
    while (latestEvent >= System.currentTimeMillis()) {
      try {
        Thread.sleep(1);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
    return latestEvent;
  }

  @Test
  public void bumpLatestEventOnProposal() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    appendBlocks(fastTrack, 1);
    BlockAndState latest = fastTrack.blockchain.latest();

    long before = getLatestEventAndWaitForLocalClock(shutdownState);
    shutdownState.newBlockProposal(latest.getBlock());
    Assertions.assertThat(shutdownState.getLatestEvent()).isGreaterThan(before);
  }

  @Test
  public void bumpLatestEventOnFlood() {
    fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    shutdownState =
        new ShutdownState(
            fastTrack.blockchain,
            dataFile,
            keyPairs.get(0),
            blsKeyPairs.get(0),
            syncRequested::getAndIncrement,
            flooding::flood,
            60_000,
            60_000);
    long before = getLatestEventAndWaitForLocalClock(shutdownState);
    shutdownState.triggerShutdown();
    Assertions.assertThat(shutdownState.getLatestEvent()).isGreaterThan(before);
  }

  private void appendResetBlock(BlockchainTestHelper fastTrack) {
    BlockAndState latest = fastTrack.blockchain.latest();
    Block previous = latest.getBlock();
    Block created =
        new Block(
            System.currentTimeMillis(),
            previous.getBlockTime() + 1,
            previous.getCommitteeId(),
            previous.identifier(),
            latest.getState().getHash(),
            List.of(),
            List.of());

    Hash identifier = created.identifier();
    BlsSignature sign =
        FastTrackStateTest.aggregateSign(
            identifier,
            BlockchainTestHelper.firstProducer,
            BlockchainTestHelper.fourthProducer,
            BlockchainTestHelper.thirdProducer);
    fastTrack.blockchain.appendBlock(
        new FinalBlock(
            created,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeDynamicBytes(new byte[] {0b1101});
                  sign.write(stream);
                })));
  }

  @Test
  public void resetBlockAddSignatures() {
    Hash hash = Hash.create(s -> s.writeInt(12));
    Block block = new Block(1, 2, 3, hash, hash, List.of(), List.of());
    List<BlockchainAddress> producers =
        List.of(
            BlockchainTestHelper.firstProducer.address,
            BlockchainTestHelper.secondProducer.address,
            BlockchainTestHelper.thirdProducer.address,
            BlockchainTestHelper.fourthProducer.address);
    List<BlsPublicKey> producerKeys =
        List.of(
            BlockchainTestHelper.firstProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.secondProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.thirdProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.fourthProducer.blsKey.getPublicKey());
    PartyConfiguration configuration =
        new PartyConfiguration(
            producers,
            producerKeys,
            BlockchainTestHelper.firstProducer.key,
            BlockchainTestHelper.firstProducer.blsKey);
    ShutdownState.ResetBlock resetBlock = new ShutdownState.ResetBlock(block, configuration);
    ShutdownState.PairOfSignatures legalSignature = resetBlock.createSignatures();
    Assertions.assertThat(resetBlock.addSignatures(legalSignature)).isTrue();
    Assertions.assertThat(resetBlock.addSignatures(legalSignature)).isFalse();
  }

  @Test
  public void deriveProtocolIdentifier() {
    BiFunction<String, Integer, String> method =
        (subChainId, resetBlocksSeen) ->
            ShutdownState.deriveProtocolIdentifier(
                    BlockchainTestHelper.INITIAL_FT_COMMITTEE, subChainId, resetBlocksSeen)
                .toString();

    Assertions.assertThat(method.apply(null, 0))
        .isEqualTo("f2a62dfa3c9319768c498e2cd3ce35d2350614b1c308bf597e6adff05c4ce653");
    Assertions.assertThat(method.apply(null, 1))
        .isEqualTo("a7dad140af1592e041b38e267a6a1838eeb635e00bff8816f3890c06b534f39c");

    String subChainId = "subChainId";
    Assertions.assertThat(method.apply(subChainId, 0))
        .isEqualTo("0f828444d738386b8e891aa21bca162a87ae7f58c397feacbf7be6c9517dd013");
    Assertions.assertThat(method.apply(subChainId, 1))
        .isEqualTo("323ed50123b790c28f1fc44329e0082c2620e3d210d4c32e8fe61ff79cbbf587");
  }

  private Hash deriveProtocolIdentifier(ShutdownState shutdownState) {
    ImmutableChainState chainState = fastTrack.blockchain.getChainState();
    ConsensusState consensusState = new ConsensusState(chainState);

    return shutdownState.deriveProtocolIdentifier(
        consensusState.getCommitteeId(), consensusState.getResetBlocksSeen());
  }

  /**
   * Creates a new block from given the latest block.
   *
   * @param latest latest block
   * @return new valid block
   */
  public static Block createBlockFromLatest(BlockAndState latest) {
    return new Block(
        latest.getBlock().getProductionTime() + 1,
        latest.getBlock().getBlockTime() + 1,
        latest.getBlock().getCommitteeId(),
        latest.getBlock().identifier(),
        latest.getState().getHash(),
        List.of(),
        List.of(),
        -1);
  }

  private void incomingSignature(ShutdownState shutdownState, KeyPair keyPair, long blockTime) {
    incoming(
        shutdownState,
        ShutdownMessage.SIGNATURE,
        stream -> {
          stream.writeLong(blockTime);
          keyPair.sign(shutdownState.shutdownHash(blockTime)).write(stream);
        });
  }

  private void incoming(
      ShutdownState shutdownState,
      ShutdownMessage message,
      Consumer<SafeDataOutputStream> content) {
    shutdownState.incoming(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                stream -> {
                  shutdownState.getProtocolIdentifier().write(stream);
                  stream.writeEnum(message);
                  content.accept(stream);
                })));
  }

  private void appendBlocks(BlockchainTestHelper fastTrack, int count) {
    for (int i = 0; i < count; i++) {
      BlockAndStateWithParent state = fastTrack.blockchain.getPossibleHeads().get(0);
      Block previous =
          new Block(
              System.currentTimeMillis(),
              state.currentBlock().getBlockTime() + 1,
              state.currentBlock().getCommitteeId(),
              state.currentBlock().identifier(),
              state.currentState().getHash(),
              List.of(),
              List.of(),
              0);
      fastTrack.blockchain.appendBlock(FastTrackProducerTest.createFinalBlock(previous));
    }
  }

  private void newBlockProposal(BlockchainTestHelper fastTrack, int count, long startBlockTime) {
    for (int i = 0; i < count; i++) {
      BlockAndStateWithParent state = fastTrack.blockchain.getPossibleHeads().get(0);
      Block previous =
          new Block(
              System.currentTimeMillis(),
              startBlockTime + 1 + i,
              state.currentBlock().getCommitteeId(),
              state.currentBlock().identifier(),
              state.currentState().getHash(),
              List.of(),
              List.of(),
              0);
      shutdownState.newBlockProposal(FastTrackProducerTest.createFinalBlock(previous).getBlock());
    }
  }

  private static final class FixedCutOffVote implements Protocol<Long> {

    private Long result;

    @Override
    public Long getResult() {
      return result;
    }

    @Override
    public boolean isDone() {
      return result != null;
    }

    @Override
    public void incoming(SafeDataInputStream message) {
      result = message.readLong();
    }
  }
}
