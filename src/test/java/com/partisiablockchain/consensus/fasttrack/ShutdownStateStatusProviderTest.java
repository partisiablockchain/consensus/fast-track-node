package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import java.util.Map;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
final class ShutdownStateStatusProviderTest {

  @Test
  public void getName() {
    String subChainId = "Shard1";
    ShutdownStateStatusProvider shutdownStateStatus =
        new ShutdownStateStatusProvider(subChainId, null);
    Assertions.assertThat(shutdownStateStatus.getName())
        .isEqualTo(subChainId + "-ShutdownStateStatusProvider");

    shutdownStateStatus = new ShutdownStateStatusProvider(null, null);
    Assertions.assertThat(shutdownStateStatus.getName())
        .isEqualTo("Gov-ShutdownStateStatusProvider");
  }

  @Test
  void getStatus() {
    long blockTimeA = 1;
    Set<BlockchainAddress> signersA = Set.of(createAddress(1), createAddress(2));
    long blockTimeB = 2;
    Set<BlockchainAddress> signersB = Set.of(createAddress(3), createAddress(4));
    long storageLength = 1234L;
    Map<Long, Set<BlockchainAddress>> shutdownSigners =
        Map.of(blockTimeA, signersA, blockTimeB, signersB);
    ShutdownState.ShutdownStage stage = ShutdownState.ShutdownStage.WAITING;

    ShutdownStateStatusProvider shutdownStateStatus =
        new ShutdownStateStatusProvider(
            "subChainId", new DummyShutdownStateStatus(storageLength, stage, shutdownSigners));

    Map<String, String> status = shutdownStateStatus.getStatus();
    Assertions.assertThat(status).hasSize(4);
    String storageLengthResponse = status.get("storageLength");
    Assertions.assertThat(storageLengthResponse).isEqualTo(Long.toString(storageLength));

    String stageResponse = status.get("stage");
    Assertions.assertThat(stageResponse).isEqualTo(stage.toString());

    String signersResponseA = status.get("signers1");
    Assertions.assertThat(signersResponseA)
        .isEqualTo(signersA.stream().map(BlockchainAddress::writeAsString).toList().toString());
    String signersResponseB = status.get("signers2");
    Assertions.assertThat(signersResponseB)
        .isEqualTo(signersB.stream().map(BlockchainAddress::writeAsString).toList().toString());
  }

  private record DummyShutdownStateStatus(
      long shutdownStorageLength,
      ShutdownState.ShutdownStage shutdownStage,
      Map<Long, Set<BlockchainAddress>> shutdownSigners)
      implements AdditionalShutdownStateStatus {}

  private BlockchainAddress createAddress(int i) {
    return BlockchainAddress.fromHash(
        BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeInt(i)));
  }
}
