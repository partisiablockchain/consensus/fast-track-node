package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.serialization.StateString;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Test for {@link ConsensusState}. */
public final class ConsensusStateTest {
  @TempDir public Path temporaryFolder;

  private List<DetermineCurrentProducerIndexTest.PairOfKeys> testProducerToPairOfKeys(
      List<BlockchainTestHelper.TestProducer> producers) {
    List<DetermineCurrentProducerIndexTest.PairOfKeys> poks = new ArrayList<>();
    for (BlockchainTestHelper.TestProducer producer : producers) {
      poks.add(
          new DetermineCurrentProducerIndexTest.PairOfKeys(
              producer.publicKey, producer.blsKey.getPublicKey()));
    }
    return poks;
  }

  @Test
  public void testIsFastTrackGlobal() {
    ConsensusState global =
        DetermineCurrentProducerIndexTest.createState(
            1,
            testProducerToPairOfKeys(
                List.of(
                    BlockchainTestHelper.firstProducer,
                    BlockchainTestHelper.secondProducer,
                    BlockchainTestHelper.thirdProducer,
                    BlockchainTestHelper.fourthProducer)));
    Assertions.assertThat(global.isFastTrack()).isTrue();
    Assertions.assertThat(new ConsensusState(null, null).isFastTrack()).isFalse();
    Assertions.assertThat(new ConsensusState(new StateString(), null).isFastTrack()).isFalse();
  }

  @Test
  public void resetBlocksSeen() {
    FastTrackLocal local = new FastTrackLocal().increaseBlocksSeen(0);
    Assertions.assertThat(local.getResetBlocksSeen()).isEqualTo(1);
    Assertions.assertThat(new ConsensusState(null, local).getResetBlocksSeen()).isEqualTo(1);
  }

  @Test
  public void activeCommittee() {
    BlockchainTestHelper helper =
        BlockchainTestHelper.createWithFastTrackThreeCommittees(temporaryFolder);
    BlockchainLedger.BlockAndState latest = helper.blockchain.latest();
    Block latestBlock = latest.getBlock();
    Block block =
        new Block(
            System.currentTimeMillis(),
            latestBlock.getBlockTime() + 1,
            latestBlock.getCommitteeId(),
            latestBlock.identifier(),
            latestBlock.getState(),
            List.of(),
            List.of(),
            -1);
    helper.blockchain.appendBlock(FastTrackProducerTest.createFinalBlock(block));
    latest = helper.blockchain.latest();
    ConsensusState consensusState = new ConsensusState(latest.getState());
    Assertions.assertThat(consensusState.getActiveCommittee()).isEqualTo(1);
    helper.blockchain.close();
  }

  @Test
  public void safeInvokeWithIntParameterCantFindFunction() {
    Assertions.assertThatThrownBy(
            () ->
                ConsensusState.safeInvokeWithLongParameter(
                    new StateString("Some state"), "someName", List.class, 2L))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void safeInvokeNoSuchMethod() {
    Assertions.assertThatThrownBy(
            () -> ConsensusState.safeInvoke(new StateString("Some state"), "someName", Long.TYPE))
        .isInstanceOf(RuntimeException.class);
  }
}
