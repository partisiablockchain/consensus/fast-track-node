package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.crypto.bls.BlsVerifier;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlsSignatureHelperTest {

  private final Hash message = Hash.create(s -> s.writeInt(1234));
  private BlsSignatureHelper<Dummy> helper;
  private final List<BlockchainAddress> signers =
      List.of(
          BlockchainTestHelper.firstProducer.address,
          BlockchainTestHelper.secondProducer.address,
          BlockchainTestHelper.thirdProducer.address,
          BlockchainTestHelper.fourthProducer.address);

  static final class Dummy implements BlsSignatureHelper.HasBlsSignature {

    private final BlsSignature signature;

    Dummy(BlsSignature signature) {
      this.signature = signature;
    }

    @Override
    public BlsSignature get() {
      return signature;
    }
  }

  /** Setup. */
  @BeforeEach
  public void before() {
    helper =
        new BlsSignatureHelper<>(
            Map.of(
                BlockchainTestHelper.firstProducer.address,
                BlockchainTestHelper.firstProducer.blsKey.getPublicKey(),
                BlockchainTestHelper.secondProducer.address,
                BlockchainTestHelper.secondProducer.blsKey.getPublicKey(),
                BlockchainTestHelper.thirdProducer.address,
                BlockchainTestHelper.thirdProducer.blsKey.getPublicKey(),
                BlockchainTestHelper.fourthProducer.address,
                BlockchainTestHelper.fourthProducer.blsKey.getPublicKey()),
            message);
  }

  private ShutdownState.PairOfSignatures createSignatures(
      BlsKeyPair blsKeyPair, KeyPair keyPair, Hash message) {
    BlsSignature sig = blsKeyPair.sign(message);
    Signature sigSig = keyPair.sign(Hash.create(sig::write));
    return new ShutdownState.PairOfSignatures(sigSig, sig);
  }

  private ShutdownState.PairOfSignatures createSignatures(
      BlockchainTestHelper.TestProducer producer) {
    return createSignatures(producer, message);
  }

  private ShutdownState.PairOfSignatures createSignatures(
      BlockchainTestHelper.TestProducer producer, Hash message) {
    return createSignatures(producer.blsKey, producer.key, message);
  }

  private Dummy wrap(BlsSignature signature) {
    return new Dummy(signature);
  }

  @Test
  public void threshold() {
    BlsSignatureHelper<Dummy> smallHelper = getSmallHelper();
    Assertions.assertThat(smallHelper.getThreshold()).isEqualTo(3);
  }

  @Test
  public void unknownSigner() {
    ShutdownState.PairOfSignatures signatures = createSignatures(BlockchainTestHelper.notPayable);
    Assertions.assertThat(helper.currentSignatureCount()).isEqualTo(0);
    helper.addSignature(signatures.signature, wrap(signatures.blsSignature), message);
    Assertions.assertThat(helper.currentSignatureCount()).isEqualTo(0);
  }

  @Test
  public void knownSigner() {
    ShutdownState.PairOfSignatures signatures =
        createSignatures(BlockchainTestHelper.firstProducer);
    Assertions.assertThat(helper.currentSignatureCount()).isEqualTo(0);
    BlsSignature blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));
    Assertions.assertThat(helper.currentSignatureCount()).isEqualTo(1);
    // not enough signatures yet though
    Assertions.assertThat(helper.check()).isFalse();
  }

  private BlsPublicKey getKeyOfProducers(BlockchainTestHelper.TestProducer... producers) {
    BlsPublicKey key = null;
    for (BlockchainTestHelper.TestProducer producer : producers) {
      BlsPublicKey publicKey = producer.blsKey.getPublicKey();
      if (key == null) {
        key = publicKey;
      } else {
        key = key.addPublicKey(publicKey);
      }
    }
    return key;
  }

  @Test
  public void allGood() {
    ShutdownState.PairOfSignatures signatures =
        createSignatures(BlockchainTestHelper.firstProducer);
    BlsSignature blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));

    // there's no signature yet
    BlsSignatureHelper.SignatureAndBitVector nullSignature = helper.getSignature(signers);
    Assertions.assertThat(nullSignature).isNull();

    signatures = createSignatures(BlockchainTestHelper.secondProducer);
    blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));
    signatures = createSignatures(BlockchainTestHelper.fourthProducer);
    blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));

    BlsSignatureHelper.SignatureAndBitVector signature = helper.getSignature(signers);
    Assertions.assertThat(signature).isNotNull();
    Assertions.assertThat(signature.bitVector).containsExactly(0b1011);
    BlsPublicKey key =
        getKeyOfProducers(
            BlockchainTestHelper.firstProducer,
            BlockchainTestHelper.secondProducer,
            BlockchainTestHelper.fourthProducer);
    Assertions.assertThat(BlsVerifier.verify(message, signature.signature, key)).isTrue();
  }

  @Test
  public void badSigners() {
    ShutdownState.PairOfSignatures signatures =
        createSignatures(BlockchainTestHelper.firstProducer);
    BlsSignature blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));
    Hash badMessage = Hash.create(s -> s.writeInt(4444));
    signatures = createSignatures(BlockchainTestHelper.secondProducer, badMessage);
    blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));
    signatures = createSignatures(BlockchainTestHelper.fourthProducer);
    blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));

    Assertions.assertThat(helper.check()).isFalse();
    Assertions.assertThat(helper.getBadSigners())
        .containsExactly(BlockchainTestHelper.secondProducer.address);

    // secondProducer cannot later add another signature. Regardless if it's good or not.
    signatures = createSignatures(BlockchainTestHelper.secondProducer);
    blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));

    Assertions.assertThat(helper.check()).isFalse();

    signatures = createSignatures(BlockchainTestHelper.thirdProducer);
    blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));

    Assertions.assertThat(helper.check()).isTrue();
    BlsSignatureHelper.SignatureAndBitVector signature = helper.getSignature(signers);
    Assertions.assertThat(signature).isNotNull();
    Assertions.assertThat(signature.bitVector).containsExactly(0b1101);
  }

  @Test
  public void oneSigner() {
    BlsSignatureHelper<Dummy> smallHelper =
        new BlsSignatureHelper<>(
            Map.of(
                BlockchainTestHelper.firstProducer.address,
                BlockchainTestHelper.firstProducer.blsKey.getPublicKey()),
            message);
    ShutdownState.PairOfSignatures signatures =
        createSignatures(BlockchainTestHelper.firstProducer);
    BlsSignature blsSignature = signatures.blsSignature;
    Assertions.assertThat(
            smallHelper.addSignature(
                signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write)))
        .isTrue();
    BlsSignatureHelper.SignatureAndBitVector signature =
        smallHelper.getSignature(List.of(BlockchainTestHelper.firstProducer.address));
    Assertions.assertThat(signature).isNotNull();
  }

  @Test
  public void manySigners() {
    Map<BlockchainAddress, BlsPublicKey> map = new HashMap<>();
    List<BlockchainAddress> signers = new ArrayList<>();
    List<BlsKeyPair> blsKeys = new ArrayList<>();
    List<KeyPair> keys = new ArrayList<>();
    for (int i = 0; i < 10; i++) {
      KeyPair key = new KeyPair(BigInteger.valueOf(41 + i));
      keys.add(key);
      BlockchainAddress address = key.getPublic().createAddress();
      signers.add(address);
      BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(42 + i));
      BlsPublicKey blsKey = blsKeyPair.getPublicKey();
      blsKeys.add(blsKeyPair);
      map.put(address, blsKey);
    }
    BlsSignatureHelper<Dummy> bigHelper = new BlsSignatureHelper<>(map, message);
    List<Integer> baddies = List.of(1, 5, 7);

    for (int i = 0; i < 10; i++) {
      ShutdownState.PairOfSignatures signatures;
      BlsKeyPair blsKeyPair = blsKeys.get(i);
      KeyPair keyPair = keys.get(i);
      if (baddies.contains(i)) {
        int finalI = i;
        Hash badMessage = Hash.create(s -> s.writeInt(1111 + finalI));
        signatures = createSignatures(blsKeyPair, keyPair, badMessage);
      } else {
        signatures = createSignatures(blsKeyPair, keyPair, message);
      }
      BlsSignature blsSignature = signatures.blsSignature;
      boolean added =
          bigHelper.addSignature(
              signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));
      Assertions.assertThat(added).isTrue();
    }

    boolean check = bigHelper.check();
    Assertions.assertThat(check).isTrue();

    BlsSignatureHelper.SignatureAndBitVector signature = bigHelper.getSignature(signers);
    Assertions.assertThat(signature).isNotNull();
    // 1110101101
    byte[] bitVector = signature.bitVector;
    int c = 0;
    for (byte b : bitVector) {
      for (int i = 0; i < 8; i++) {
        byte expected = (byte) (baddies.contains(c) ? 0 : 1);
        if (c >= signers.size()) {
          expected = 0;
        }
        byte bit = (byte) ((b >> i) & 1);
        Assertions.assertThat(bit).isEqualTo(expected);
        c++;
      }
    }
  }

  private BlsSignatureHelper<Dummy> getSmallHelper() {
    return new BlsSignatureHelper<>(
        Map.of(
            BlockchainTestHelper.firstProducer.address,
            BlockchainTestHelper.firstProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.secondProducer.address,
            BlockchainTestHelper.secondProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.thirdProducer.address,
            BlockchainTestHelper.thirdProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.fourthProducer.address,
            BlockchainTestHelper.fourthProducer.blsKey.getPublicKey()),
        message);
  }

  private void addSignature(
      BlsSignatureHelper<Dummy> helper, BlockchainTestHelper.TestProducer producer, Hash message) {
    ShutdownState.PairOfSignatures signatures = createSignatures(producer, message);
    BlsSignature blsSignature = signatures.blsSignature;
    helper.addSignature(signatures.signature, wrap(blsSignature), Hash.create(blsSignature::write));
  }

  @Test
  public void removeBadSignatures() {
    BlsSignatureHelper<Dummy> smallHelper = getSmallHelper();
    addSignature(smallHelper, BlockchainTestHelper.firstProducer, message);
    addSignature(smallHelper, BlockchainTestHelper.secondProducer, message);
    Hash badMessage = Hash.create(s -> s.writeInt(4321));
    addSignature(smallHelper, BlockchainTestHelper.thirdProducer, badMessage);
    addSignature(smallHelper, BlockchainTestHelper.fourthProducer, this.message);

    BlsSignatureHelper.SignatureAndBitVector signature = smallHelper.getSignature(signers);
    Assertions.assertThat(signature).isNotNull();
    Assertions.assertThat(signature.bitVector).containsExactly(0b1011);
    Assertions.assertThat(smallHelper.getBadSigners())
        .containsExactly(BlockchainTestHelper.thirdProducer.address);
    BlsSignature sign0 = BlockchainTestHelper.firstProducer.blsKey.sign(message);
    BlsSignature sign1 = BlockchainTestHelper.secondProducer.blsKey.sign(message);
    BlsSignature sign2 = BlockchainTestHelper.fourthProducer.blsKey.sign(message);
    Assertions.assertThat(sign0.addSignature(sign1).addSignature(sign2))
        .isEqualTo(signature.signature);

    smallHelper = getSmallHelper();
    addSignature(smallHelper, BlockchainTestHelper.firstProducer, message);
    addSignature(smallHelper, BlockchainTestHelper.secondProducer, message);
    addSignature(smallHelper, BlockchainTestHelper.thirdProducer, message);
    addSignature(smallHelper, BlockchainTestHelper.fourthProducer, badMessage);

    signature = smallHelper.getSignature(signers);
    Assertions.assertThat(signature).isNotNull();

    Assertions.assertThat(signature.bitVector).containsExactly(0b0111);
    Assertions.assertThat(smallHelper.getBadSigners())
        .containsExactly(BlockchainTestHelper.fourthProducer.address);
    sign0 = BlockchainTestHelper.secondProducer.blsKey.sign(message);
    sign1 = BlockchainTestHelper.thirdProducer.blsKey.sign(message);
    sign2 = BlockchainTestHelper.firstProducer.blsKey.sign(message);
    Assertions.assertThat(sign0.addSignature(sign1).addSignature(sign2))
        .isEqualTo(signature.signature);
  }

  @Test
  public void honestOnly() {
    BlsSignatureHelper<Dummy> helper =
        new BlsSignatureHelper<>(
            Map.of(
                BlockchainTestHelper.firstProducer.address,
                BlockchainTestHelper.firstProducer.blsKey.getPublicKey(),
                BlockchainTestHelper.secondProducer.address,
                BlockchainTestHelper.secondProducer.blsKey.getPublicKey(),
                BlockchainTestHelper.thirdProducer.address,
                BlockchainTestHelper.thirdProducer.blsKey.getPublicKey()),
            message);
    addSignature(helper, BlockchainTestHelper.firstProducer, message);
    BlsSignatureHelper.SignatureAndBitVector signature = helper.getSignature(signers);
    Assertions.assertThat(signature).isNull();
    addSignature(helper, BlockchainTestHelper.secondProducer, message);
    signature = helper.getSignature(signers);
    Assertions.assertThat(signature).isNull();
    addSignature(helper, BlockchainTestHelper.thirdProducer, message);
    signature = helper.getSignature(signers);
    Assertions.assertThat(signature).isNotNull();
  }

  @Test
  public void removeBadSignaturesCases() {
    BlsSignatureHelper<Dummy> smallHelper = getSmallHelper();
    Hash junk = Hash.create(s -> s.writeString("aaaa"));
    List<Dummy> signatures =
        List.of(
            new Dummy(BlockchainTestHelper.firstProducer.blsKey.sign(junk)),
            new Dummy(BlockchainTestHelper.secondProducer.blsKey.sign(message)),
            new Dummy(BlockchainTestHelper.thirdProducer.blsKey.sign(junk)),
            new Dummy(BlockchainTestHelper.fourthProducer.blsKey.sign(junk)));
    List<Dummy> badSignatures = new ArrayList<>();
    List<BlsPublicKey> keys =
        List.of(
            BlockchainTestHelper.firstProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.secondProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.thirdProducer.blsKey.getPublicKey(),
            BlockchainTestHelper.fourthProducer.blsKey.getPublicKey());
    BlsSignature blsSignature = smallHelper.removeBadSignatures(signatures, keys, badSignatures);
    Assertions.assertThat(blsSignature).isEqualTo(signatures.get(1).signature);
  }
}
