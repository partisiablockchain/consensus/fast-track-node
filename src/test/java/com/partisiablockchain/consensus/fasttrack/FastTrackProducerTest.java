package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockAndStateWithParent;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.FinalizationData;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.consensus.fasttrack.FastTrackProducer.FinalizationMessage;
import com.partisiablockchain.consensus.fasttrack.rewards.EpochEndPayload;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.flooding.Connection;
import com.partisiablockchain.flooding.Packet;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.coverage.WithCloseableResources;
import com.secata.util.NumericConversion;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

/** Test. */
public final class FastTrackProducerTest {

  @TempDir public Path temporaryFolder;

  private TransactionSender transactionSender;

  private FastTrackProducer blockProducer;
  private BlockchainTestHelper helper;

  private AdditionalStatusProviders additionalStatusProviders;

  /** Close the producer. */
  @AfterEach
  public void tearDown() {
    if (blockProducer != null) {
      blockProducer.close();
    }
    if (helper != null) {
      helper.blockchain.close();
    }
  }

  /** Set up the test. */
  @BeforeEach
  public void setUp() {
    transactionSender = new LoggingTransactionSender();
    additionalStatusProviders = new DummyAdditionalStatusProviders();
  }

  @Test
  public void createMaximumWait() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);

    ToLongFunction<BlockAndStateWithParent> maximumWait =
        FastTrackProducer.createMaximumWait(System::currentTimeMillis, 777);

    Assertions.assertThat(maximumWait.applyAsLong(helper.blockchain.getPossibleHeads().get(0)))
        .describedAs("Parent is very old")
        .isEqualTo(0L);
    helper.appendBlock(
        BlockchainTestHelper.firstProducer.key,
        List.of(helper.createTransaction(BlockchainTestHelper.firstProducer.key)));
    Assertions.assertThat(maximumWait.applyAsLong(latestProposal())).isEqualTo(0);
    helper.appendBlock(
        BlockchainTestHelper.firstProducer.key, List.of(), latestProposal().current());
    // Finalizes transactions
    Assertions.assertThat(maximumWait.applyAsLong(latestProposal())).isEqualTo(0);
    helper.appendBlock(
        BlockchainTestHelper.firstProducer.key, List.of(), latestProposal().current());
    Assertions.assertThat(maximumWait.applyAsLong(latestProposal())).isEqualTo(777L);
  }

  @Test
  public void maximumWaitWithEvent() {
    ToLongFunction<BlockAndStateWithParent> maximumWait =
        FastTrackProducer.createMaximumWait(() -> 0, 777);

    long wait = maximumWait.applyAsLong(BlockchainTestHelper.parentWithEvent());
    Assertions.assertThat(wait).isEqualTo(0);
  }

  @Test
  public void createMaximumWaitOldParent() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);

    AtomicLong now = new AtomicLong(System.currentTimeMillis());

    ToLongFunction<BlockAndStateWithParent> maximumWait =
        FastTrackProducer.createMaximumWait(now::get, 10_000);

    Assertions.assertThat(maximumWait.applyAsLong(latestProposal()))
        .describedAs("Parent is very old")
        .isEqualTo(0L);
    helper.appendBlock(
        BlockchainTestHelper.firstProducer.key, List.of(), latestProposal().current());
    Assertions.assertThat(maximumWait.applyAsLong(latestProposal())).isEqualTo(10_000L);
    now.set(latestProposal().currentBlock().getProductionTime() + 10 * 10_000 + 30_000);
    Assertions.assertThat(maximumWait.applyAsLong(latestProposal())).isEqualTo(10_000L);
    now.getAndIncrement();
    Assertions.assertThat(maximumWait.applyAsLong(latestProposal())).isEqualTo(0L);
  }

  @Test
  public void nullAdditionalStatusProviders_noExceptionsThrown() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    Assertions.assertThatNoException()
        .isThrownBy(
            () ->
                new FastTrackProducer(
                    MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
                    helper.blockchain,
                    BlockchainTestHelper.firstProducer.key,
                    BlockchainTestHelper.firstProducer.blsKey,
                    0,
                    1_000,
                    transactionSender,
                    null));
  }

  @Test
  public void getLatestProductionTime() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    blockProducer =
        new FastTrackProducer(
            MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
            helper.blockchain,
            BlockchainTestHelper.firstProducer.key,
            BlockchainTestHelper.firstProducer.blsKey,
            0,
            1_000,
            transactionSender,
            additionalStatusProviders);

    Assertions.assertThat(blockProducer.getLatestProductionTime()).isEqualTo(0);

    helper.appendEmptyBlock(BlockchainTestHelper.firstProducer.key);
    Assertions.assertThat(blockProducer.getLatestProductionTime())
        .isEqualTo(helper.blockchain.getLatestBlock().getProductionTime());
  }

  @Test
  public void sendSync() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    blockProducer =
        new FastTrackProducer(
            MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
            helper.blockchain,
            BlockchainTestHelper.firstProducer.key,
            BlockchainTestHelper.firstProducer.blsKey,
            0,
            1_000,
            transactionSender,
            additionalStatusProviders);
    blockProducer.sendSync();

    Assertions.assertThat(helper.getNetworkRequests()).hasSize(1);
    verifySyncRequest(helper.getNetworkRequests().get(0));
  }

  @Test
  @SuppressWarnings("EnumOrdinal")
  public void sendShutdownSync() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    blockProducer =
        new FastTrackProducer(
            MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
            helper.blockchain,
            BlockchainTestHelper.firstProducer.key,
            BlockchainTestHelper.firstProducer.blsKey,
            0,
            1_000,
            transactionSender,
            additionalStatusProviders);
    blockProducer.sendShutdownSync();

    Assertions.assertThat(helper.getNetworkRequests()).hasSize(1);
    FinalizationData payload = verifyFinalizationPacket(helper.getNetworkRequests().get(0));
    Assertions.assertThat(payload.getData())
        .isEqualTo(
            new byte[] {
              (byte) FinalizationMessage.SHUTDOWN_SYNC_LIMIT_SEND.ordinal(),
              (byte) ShutdownState.ShutdownStage.NOT_PARTICIPATING.ordinal()
            });
  }

  @Test
  public void doNotProduceWhenNotFastTrack() throws IOException {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    RootDirectory dataDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    blockProducer =
        new FastTrackProducer(
            dataDirectory,
            helper.blockchain,
            BlockchainTestHelper.firstProducer.key,
            BlockchainTestHelper.firstProducer.blsKey,
            0,
            1_000,
            transactionSender,
            additionalStatusProviders);

    sleep(300);
    Assertions.assertThat(helper.getFloods()).hasSize(0);
    Assertions.assertThat(
            Files.readAllBytes(dataDirectory.createFile("latestProducedBlock").toPath()))
        .isEqualTo(NumericConversion.longToBytes(0));
  }

  @Test
  public void doNotProduceWhenNotProducer() throws IOException {
    helper = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    RootDirectory dataDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    blockProducer =
        new FastTrackProducer(
            dataDirectory,
            helper.blockchain,
            BlockchainTestHelper.secondProducer.key,
            BlockchainTestHelper.secondProducer.blsKey,
            0,
            1_000,
            transactionSender,
            additionalStatusProviders);

    sleep(300);
    Assertions.assertThat(helper.getFloods()).hasSize(0);
    Assertions.assertThat(
            Files.readAllBytes(dataDirectory.createFile("latestProducedBlock").toPath()))
        .isEqualTo(NumericConversion.longToBytes(0));
  }

  @Test
  public void produceWhenFastTrackAndProducer() {
    createFastTrack(BlockchainTestHelper.firstProducer);

    sleep(300);

    Map<String, String> blockProducerStatus =
        additionalStatusProviders.getProviders().get(2).getStatus();
    Assertions.assertThat(blockProducerStatus.get("latestProducedBlockTime"))
        .isEqualTo(Long.toString(1));

    Map<String, String> fastTrackStatus =
        additionalStatusProviders.getProviders().get(0).getStatus();
    Assertions.assertThat(fastTrackStatus).hasSize(6);
    Assertions.assertThat(fastTrackStatus.get("latestSignedBlockTime")).isEqualTo(Long.toString(1));
    BlockState blockState = blockProducer.getBlockStates().get(0);
    Block blockStateBlock = blockState.getBlock();
    String prefix = "blockState0";
    Assertions.assertThat(fastTrackStatus.get(prefix + "BlockIdentifier"))
        .isEqualTo(blockStateBlock.identifier().toString());
    Assertions.assertThat(fastTrackStatus.get(prefix + "BlockTime"))
        .isEqualTo(Long.toString(blockStateBlock.getBlockTime()));
    Assertions.assertThat(fastTrackStatus.get(prefix + "BlockProducerIndex"))
        .isEqualTo(Short.toString(blockStateBlock.getProducerIndex()));
    Assertions.assertThat(fastTrackStatus.get(prefix + "GoodSigners"))
        .isEqualTo(
            blockState.getGoodSigners().stream()
                .map(BlockchainAddress::writeAsString)
                .toList()
                .toString());
    Assertions.assertThat(fastTrackStatus.get(prefix + "BadSigners"))
        .isEqualTo(
            blockState.getBadSigners().stream()
                .map(BlockchainAddress::writeAsString)
                .toList()
                .toString());
    List<Packet<?>> floods = helper.getFloods();
    Assertions.assertThat(floods).hasSize(2);
    verifyBlock(verifyFinalizationPacket(floods.get(0)));
    Hash stateHash = helper.blockchain.getLatestBlock().getState();
    verifyEcho(
        verifyFinalizationPacket(floods.get(1)), BlockchainTestHelper.firstProducer.key, stateHash);
  }

  @Test
  public void doNotProduceWhenShuttingDown() {
    helper = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    blockProducer =
        new FastTrackProducer(
            MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
            helper.blockchain,
            BlockchainTestHelper.firstProducer.key,
            BlockchainTestHelper.firstProducer.blsKey,
            100,
            1_000,
            transactionSender,
            additionalStatusProviders);
    Assertions.assertThat(blockProducer.isShuttingDown()).isFalse();
    blockProducer.triggerShutdown();
    Assertions.assertThat(blockProducer.isShuttingDown()).isTrue();
    Assertions.assertThat(
            blockProducer.shouldProduceBlock(
                null, new ConsensusState(latestProposal().currentState())))
        .isFalse();
  }

  private BlockAndStateWithParent latestProposal() {
    return helper.blockchain.getPossibleHeads().stream()
        .max(Comparator.comparingLong(value -> value.currentBlock().getBlockTime()))
        .orElseThrow();
  }

  @Test
  public void livenessTimeout() {
    Assertions.assertThat(FastTrackProducer.livenessTimeout(13, 17)).isEqualTo(13 + 17 * 2);
  }

  @Test
  public void triggerShutdown() {
    helper = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    blockProducer =
        new FastTrackProducer(
            MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
            helper.blockchain,
            BlockchainTestHelper.firstProducer.key,
            BlockchainTestHelper.firstProducer.blsKey,
            0,
            0,
            transactionSender,
            additionalStatusProviders);
    sleep(300);
    Assertions.assertThat(blockProducer.isShuttingDown()).isTrue();
    Map<String, String> shutdownStatus =
        additionalStatusProviders.getProviders().get(1).getStatus();
    Assertions.assertThat(shutdownStatus.get("signers" + helper.blockchain.getBlockTime()))
        .isEqualTo(List.of(BlockchainTestHelper.firstProducer.address.writeAsString()).toString());
    Assertions.assertThat(Long.parseLong(shutdownStatus.get("storageLength"))).isGreaterThan(0);
  }

  private void verifyBlock(FinalizationData finalizationData) {
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(finalizationData.getData());
    FinalizationMessage finalizationMessage = stream.readEnum(FinalizationMessage.values());
    Assertions.assertThat(finalizationMessage).isEqualTo(FinalizationMessage.BLOCK);
    Block read = Block.read(stream);
    Assertions.assertThat(read).isNotNull();
  }

  @Test
  public void incomingUnknownParentHash() {
    createFastTrack(BlockchainTestHelper.secondProducer);

    BlockchainLedger blockchain = helper.blockchain;
    BlockAndState latest = blockchain.latest();
    Block block =
        new Block(
            System.currentTimeMillis(),
            blockchain.getLatestBlock().getBlockTime() + 1,
            blockchain.getLatestBlock().getCommitteeId(),
            Hash.create(FunctionUtility.noOpConsumer()),
            latest.getState().getHash(),
            List.of(),
            List.of());

    blockProducer.onFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.BLOCK);
              block.write(stream);
              BlockchainTestHelper.firstProducer.key.sign(block.identifier()).write(stream);
            }));
    Assertions.assertThat(helper.getFloods()).hasSize(0);
  }

  @Test
  public void incomingNotFastTrack() {
    helper = BlockchainTestHelper.createEmpty(temporaryFolder);
    blockProducer =
        new FastTrackProducer(
            MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
            helper.blockchain,
            BlockchainTestHelper.secondProducer.key,
            BlockchainTestHelper.secondProducer.blsKey,
            0,
            1_000,
            transactionSender,
            additionalStatusProviders);

    BlockchainLedger blockchain = helper.blockchain;
    BlockAndState latest = blockchain.latest();
    Block block = createBlock(latest);

    blockProducer.onFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.BLOCK);
              block.write(stream);

              BlockchainTestHelper.firstProducer.key.sign(block.identifier()).write(stream);
            }));
    Assertions.assertThat(helper.getFloods()).hasSize(0);
  }

  @Test
  public void incomingBlockMissingTransaction() {
    createFastTrack(BlockchainTestHelper.secondProducer);

    BlockchainLedger blockchain = helper.blockchain;
    BlockAndState latest = blockchain.latest();
    Block block =
        new Block(
            System.currentTimeMillis(),
            latest.getBlock().getBlockTime() + 1,
            latest.getBlock().getCommitteeId(),
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            List.of(),
            List.of(Hash.create(FunctionUtility.noOpConsumer())));

    Connection mock = Mockito.mock(Connection.class);
    blockProducer.onFinalizationMessage(
        mock,
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.BLOCK);
              block.write(stream);
              BlockchainTestHelper.firstProducer.key.sign(block.identifier()).write(stream);
            }));
    Assertions.assertThat(helper.getFloods()).hasSize(0);
    verifySyncRequest(verifySend(mock));
  }

  @Test
  public void syncResponse() {
    createFastTrack(BlockchainTestHelper.secondProducer);
    Connection connection = Mockito.mock(Connection.class);
    blockProducer.onFinalizationMessage(
        connection, finalizationData(stream -> stream.writeEnum(FinalizationMessage.SYNC_REQUEST)));

    FinalizationData finalizationData = verifyFinalizationPacket(verifySend(connection));
    Assertions.assertThat(finalizationData.getData())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeEnum(FinalizationMessage.SYNC_RESPONSE);
                  stream.writeInt(0);
                }));
  }

  @Test
  public void shutdownSyncResponse() {
    createFastTrack(BlockchainTestHelper.secondProducer);
    Connection connection = Mockito.mock(Connection.class);
    blockProducer.triggerShutdown();

    blockProducer.onFinalizationMessage(
        connection,
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.SHUTDOWN_SYNC_LIMIT_SEND);
              stream.writeEnum(ShutdownState.ShutdownStage.DETERMINE_CUT_OFF);
            }));
    Mockito.verifyNoInteractions(connection);

    // Sending a bunch of messages, so we go into CUT_OFF, but also have a bunch of messages we
    // don't
    // want to send.
    sendSignatureMessage(connection, new KeyPair(BigInteger.TWO));
    sendSignatureMessage(connection, new KeyPair(BigInteger.valueOf(3)));
    sendSignatureMessage(connection, new KeyPair(BigInteger.valueOf(4)));

    blockProducer.onFinalizationMessage(
        connection,
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.SHUTDOWN_SYNC_LIMIT_SEND);
              stream.writeEnum(ShutdownState.ShutdownStage.DETERMINE_CUT_OFF);
            }));

    FinalizationData finalizationData = verifyFinalizationPacket(verifySend(connection, 4));
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(finalizationData.getData());
    FinalizationMessage message = input.readEnum(FinalizationMessage.values());

    Assertions.assertThat(message).isEqualTo(FinalizationMessage.SHUTDOWN);

    Mockito.reset(connection);
    blockProducer.onFinalizationMessage(
        connection,
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.SHUTDOWN_SYNC_LIMIT_SEND);
              stream.writeEnum(ShutdownState.ShutdownStage.NOT_PARTICIPATING);
            }));
    // Old amount of packages that the node would have received.
    verifyFinalizationPacket(verifySend(connection, 8));
  }

  @Test
  void shutdownSyncResponseBackwardCompatible() {
    createFastTrack(BlockchainTestHelper.secondProducer);
    Connection connection = Mockito.mock(Connection.class);
    blockProducer.triggerShutdown();

    blockProducer.onFinalizationMessage(
        connection,
        finalizationData(stream -> stream.writeEnum(FinalizationMessage.SHUTDOWN_SYNC)));

    FinalizationData finalizationData = verifyFinalizationPacket(verifySend(connection));
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(finalizationData.getData());
    FinalizationMessage message = input.readEnum(FinalizationMessage.values());

    Assertions.assertThat(message).isEqualTo(FinalizationMessage.SHUTDOWN);
  }

  private void sendSignatureMessage(Connection connection, KeyPair producer) {
    Hash protocolIdentifier =
        Hash.fromString("f2a62dfa3c9319768c498e2cd3ce35d2350614b1c308bf597e6adff05c4ce653");
    blockProducer.onFinalizationMessage(
        connection,
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.SHUTDOWN);
              stream.write(
                  SafeDataOutputStream.serialize(
                      s -> {
                        protocolIdentifier.write(s);
                        s.writeEnum(ShutdownState.ShutdownMessage.SIGNATURE);
                        s.writeLong(1L);
                        producer
                            .sign(
                                Hash.create(
                                    ss -> {
                                      ss.writeString("SHUTDOWN");
                                      protocolIdentifier.write(ss);
                                      ss.writeLong(1L);
                                    }))
                            .write(s);
                      }));
            }));
  }

  private Packet<?> verifySend(Connection mock) {
    return verifySend(mock, 1);
  }

  @SuppressWarnings("unchecked")
  private Packet<?> verifySend(Connection mock, int expectedPackages) {
    ArgumentCaptor<Packet<?>> captor = ArgumentCaptor.forClass(Packet.class);
    Mockito.verify(mock, Mockito.times(expectedPackages)).send(captor.capture());
    return captor.getValue();
  }

  @SuppressWarnings("EnumOrdinal")
  private void verifySyncRequest(Packet<?> packet) {
    FinalizationData payload = verifyFinalizationPacket(packet);
    Assertions.assertThat(payload.getData())
        .isEqualTo(new byte[] {(byte) FinalizationMessage.SYNC_REQUEST.ordinal()});
  }

  private FinalizationData verifyFinalizationPacket(Packet<?> packet) {
    Assertions.assertThat(packet.getType()).isEqualTo(Packet.Type.FINALIZATION);
    return (FinalizationData) packet.getPayload();
  }

  @Test
  public void incomingValidBlock() {
    createFastTrackWithNotPayableSigner();

    BlockchainLedger blockchain = helper.blockchain;
    BlockAndState latest = blockchain.latest();
    Block block = createBlock(latest);

    blockProducer.onFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.BLOCK);
              block.write(stream);
              BlockchainTestHelper.firstProducer.key.sign(ProducerHash.create(block)).write(stream);
            }));
    Assertions.assertThat(helper.getFloods()).hasSize(1);
  }

  @Test
  public void incomingValidBlockThroughBlockchain() {
    createFastTrackWithNotPayableSigner();

    BlockchainLedger blockchain = helper.blockchain;
    BlockAndState latest = blockchain.latest();
    Block block = createBlock(latest);

    helper.incomingFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.BLOCK);
              block.write(stream);
              BlockchainTestHelper.firstProducer.key.sign(ProducerHash.create(block)).write(stream);
            }));
    waitForFlood(helper.getFloods());
    Assertions.assertThat(helper.getFloods()).hasSize(1);
  }

  private void waitForFlood(List<Packet<?>> floods) {
    for (int i = 0; i < 20 && floods.size() == 0; i++) {
      sleep(100);
    }
  }

  @Test
  public void incomingValidEcho() {
    createFastTrackWithNotPayableSigner();

    BlockchainLedger blockchain = helper.blockchain;
    BlockAndState latest = blockchain.latest();
    Block block = createBlock(latest);

    blockProducer.onFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.BLOCK);
              block.write(stream);
              BlockchainTestHelper.firstProducer.key.sign(ProducerHash.create(block)).write(stream);
            }));
    helper.getFloods().clear();

    BlsSignature sign = BlockchainTestHelper.secondProducer.blsKey.sign(block.identifier());
    Hash messageHash = FinalizationMessageHelper.createMessage(block.identifier(), sign);
    Signature signature = BlockchainTestHelper.secondProducer.key.sign(messageHash);
    Hash proof =
        Hash.create(
            stream -> {
              BlockchainTestHelper.secondProducer.publicKey.write(stream);
              stream.write(blockchain.getStateStorage().read(block.getState()));
            });
    blockProducer.onFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.ECHO);
              signature.write(stream);
              sign.write(stream);
              proof.write(stream);
            }));
    Assertions.assertThat(helper.getFloods()).hasSize(1);
    verifyEcho(
        verifyFinalizationPacket(helper.getFloods().get(0)),
        BlockchainTestHelper.secondProducer.key,
        block.getState());
  }

  private void verifyEcho(FinalizationData finalizationData, KeyPair producer, Hash stateHash) {
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(finalizationData.getData());
    FinalizationMessage finalizationMessage = stream.readEnum(FinalizationMessage.values());
    Assertions.assertThat(finalizationMessage).isEqualTo(FinalizationMessage.ECHO);
    Signature read = Signature.read(stream);
    BlsSignature read1 = BlsSignature.read(stream);
    Assertions.assertThat(read).isNotNull();
    Assertions.assertThat(read1).isNotNull();
    Hash hash =
        Hash.create(
            s -> {
              producer.getPublic().write(s);
              s.write(helper.blockchain.getStateStorage().read(stateHash));
            });
    Hash proof = Hash.read(stream);
    Assertions.assertThat(proof).isEqualTo(hash);
  }

  @Test
  public void syncResponseWithFullBlock() {
    createFastTrackWithNotPayableSigner();

    BlockAndState latest = helper.blockchain.latest();
    SignedTransaction transaction =
        helper.createTransaction(BlockchainTestHelper.firstProducer.key);
    Block block =
        new Block(
            System.currentTimeMillis(),
            latest.getBlock().getBlockTime() + 1,
            latest.getBlock().getCommitteeId(),
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            List.of(),
            List.of(transaction.identifier()),
            (short) 0);

    helper.blockchain.addPendingTransaction(transaction);
    fullBlockResponse(block);

    List<BlockAndState> proposals = helper.blockchain.getProposals();
    Assertions.assertThat(proposals).hasSize(1);
    Assertions.assertThat(proposals.get(0).getBlock()).isEqualTo(block);
  }

  @Test
  public void invalidProducer() {
    createFastTrackWithNotPayableSigner();

    for (int i = 0; i < 100; i++) {
      BlockAndState latest = latestProposal().current();
      Block block = createBlock(latest);
      fullBlockResponse(block);
    }

    BlockAndState latest = latestProposal().current();
    Block block = createBlock(latest, 1);

    Assertions.assertThatThrownBy(() -> fullBlockResponse(block)).hasMessage("Invalid block");
    Assertions.assertThat(latest.getBlock().getBlockTime()).isEqualTo(100);

    fullBlockResponse(block, BlockchainTestHelper.secondProducer.key);
    Assertions.assertThat(latestProposal().current().getBlock().getBlockTime()).isEqualTo(101);
  }

  @Test
  public void determineNextProducer() {
    createFastTrackWithNotPayableSigner();
    BlockAndState parent = latestProposal().current();
    Block block2 =
        new Block(
            System.currentTimeMillis(),
            0,
            parent.getBlock().getCommitteeId(),
            parent.getBlock().identifier(),
            parent.getState().getHash(),
            List.of(),
            List.of(),
            (short) 0);
    BlockchainLedger blockchain = helper.blockchain;
    Assertions.assertThat(
            FastTrackProducer.determineNextProducer(
                block2,
                new ConsensusState(parent.getState()),
                blockchain::getBlock,
                y -> new ConsensusState(blockchain.getState(y.getBlockTime() - 1)),
                Map.of()))
        .isEqualTo(0);

    for (int i = 0; i < 10; i++) {
      BlockAndState latest = latestProposal().current();
      Block block = createBlock(latest, 0);
      fullBlockResponse(block);
    }

    for (int i = 0; i < 10; i++) {
      BlockAndState latest = latestProposal().current();
      Block block = createBlock(latest, -1);
      blockProducer.appendBlock(createFinalBlock(block));
    }

    BlockAndState latest = latestProposal().current();

    Block next = createBlock(latest, 1);
    Assertions.assertThat(determineNextProducer(next, latest)).isEqualTo(1);

    Block next2 = createBlock(latest, -1, next.getProductionTime() + 1);
    Assertions.assertThat(determineNextProducer(next2, latest)).isEqualTo(3);
  }

  private int determineNextProducer(Block block, BlockAndState parent) {
    return blockProducer.determineNextProducer(block, new ConsensusState(parent.getState()));
  }

  private void createFastTrackWithNotPayableSigner() {
    createFastTrack(BlockchainTestHelper.notPayable);
  }

  private void createFastTrack(BlockchainTestHelper.TestProducer producer) {
    helper = BlockchainTestHelper.createWithFastTrack(temporaryFolder);
    blockProducer =
        new FastTrackProducer(
            MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
            helper.blockchain,
            producer.key,
            producer.blsKey,
            0,
            1_000,
            transactionSender,
            additionalStatusProviders);
  }

  private void fullBlockResponse(Block block) {
    fullBlockResponse(block, BlockchainTestHelper.firstProducer.key);
  }

  private void fullBlockResponse(Block block, KeyPair keyPair) {
    Hash blockIdentifier = block.identifier();
    Hash stateIdentifier = block.getState();
    blockProducer.onFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.SYNC_RESPONSE);
              stream.writeInt(1);
              block.write(stream);
              keyPair.sign(ProducerHash.create(block)).write(stream);
              writeSignatures(
                  stream,
                  blockIdentifier,
                  stateIdentifier,
                  BlockchainTestHelper.firstProducer,
                  BlockchainTestHelper.fourthProducer,
                  BlockchainTestHelper.thirdProducer);
            }));
  }

  private void writeSignatures(
      SafeDataOutputStream stream,
      Hash identifier,
      Hash stateIdentifier,
      BlockchainTestHelper.TestProducer... producers) {

    List<SignatureAndVerification> signatures =
        Arrays.stream(producers)
            .map(
                producer -> {
                  BlsSignature sign = producer.blsKey.sign(identifier);
                  Hash message = FinalizationMessageHelper.createMessage(identifier, sign);
                  Signature sigSig = producer.key.sign(message);
                  return new SignatureAndVerification(
                      sigSig, sign, createProofOfVerification(producer.key, stateIdentifier));
                })
            .collect(Collectors.toList());
    FastTrackProducer.SIGNATURE_AND_VERIFICATION_LIST.writeDynamic(stream, signatures);
  }

  private Hash createProofOfVerification(KeyPair keyPair, Hash identifier) {
    byte[] data = helper.blockchain.getStateStorage().read(identifier);
    return Hash.create(
        stream -> {
          keyPair.getPublic().write(stream);
          stream.write(data);
        });
  }

  @Test
  public void shouldCleanUpStateAfterFinalizingBlock() {
    createFastTrackWithNotPayableSigner();

    BlockchainLedger blockchain = helper.blockchain;
    BlockAndState latest = blockchain.latest();
    Block block = createBlock(latest);
    fullBlockResponse(block);

    List<BlockAndState> proposals = blockchain.getProposals();
    Assertions.assertThat(proposals).hasSize(1);
    BlockAndState proposal = proposals.get(0);
    Assertions.assertThat(proposal.getBlock()).isEqualTo(block);

    Block nextBlock = createBlock(proposal);
    fullBlockResponse(nextBlock);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(block);

    // Should have cleaned and signature for block should thus be ignored
    helper.getFloods().clear();
    BlsSignature sign = BlockchainTestHelper.secondProducer.blsKey.sign(block.identifier());
    Hash messageHash = FinalizationMessageHelper.createMessage(block.identifier(), sign);
    Signature signature = BlockchainTestHelper.secondProducer.key.sign(messageHash);
    Hash proof =
        createProofOfVerification(BlockchainTestHelper.secondProducer.key, block.getState());

    blockProducer.onFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.ECHO);
              signature.write(stream);
              sign.write(stream);
              proof.write(stream);
            }));
    Assertions.assertThat(helper.getFloods()).isEmpty();
  }

  @Test
  public void appendBlock() {
    createFastTrackWithNotPayableSigner();

    BlockchainLedger blockchain = helper.blockchain;
    BlockAndState latest = blockchain.latest();
    SignedTransaction transaction =
        helper.createTransaction(BlockchainTestHelper.firstProducer.key);
    Block block =
        new Block(
            System.currentTimeMillis(),
            latest.getBlock().getBlockTime() + 1,
            latest.getBlock().getCommitteeId(),
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            List.of(),
            List.of(transaction.identifier()),
            0);

    FinalBlock finalBlock = createFinalBlock(block);
    blockchain.addPendingTransaction(transaction);
    blockProducer.appendBlock(finalBlock);

    List<BlockAndState> proposals = blockchain.getProposals();
    Assertions.assertThat(proposals).hasSize(1);
    Assertions.assertThat(proposals.get(0).getBlock()).isEqualTo(block);
  }

  @Test
  void cleanupKnownBlocksInLedger() {
    createFastTrackWithNotPayableSigner();
    Block block = createBlock(helper.blockchain.latest());
    helper.blockchain.appendBlock(createFinalBlock(block));

    blockProducer.onFinalizationMessage(
        Mockito.mock(Connection.class),
        finalizationData(
            stream -> {
              stream.writeEnum(FinalizationMessage.BLOCK);
              block.write(stream);
              BlockchainTestHelper.firstProducer.key.sign(ProducerHash.create(block)).write(stream);
            }));
    // Does not flood, the flood stems from the ledger before invoking the fast track producer.
    Assertions.assertThat(helper.getFloods().size()).isEqualTo(1);
  }

  @Test
  public void shutdown() {
    final List<BlockchainTestHelper.TestProducer> producers =
        List.of(
            BlockchainTestHelper.firstProducer,
            BlockchainTestHelper.secondProducer,
            BlockchainTestHelper.thirdProducer,
            BlockchainTestHelper.fourthProducer);
    List<FastTrackProducer> protocols = new ArrayList<>();
    BlockchainTestHelper fastTrack = BlockchainTestHelper.createWithFastTrack(temporaryFolder);

    Block blockBefore = fastTrack.blockchain.getLatestBlock();
    for (BlockchainTestHelper.TestProducer producer : producers) {
      File newFolder = new File(temporaryFolder.toFile(), UUID.randomUUID().toString());
      Assertions.assertThat(newFolder.mkdirs()).isTrue();
      FastTrackProducer fastTrackProducer =
          new FastTrackProducer(
              MemoryStorage.createRootDirectory(newFolder),
              fastTrack.blockchain,
              producer.key,
              producer.blsKey,
              60_000,
              60_000,
              transactionSender,
              additionalStatusProviders);
      fastTrackProducer.triggerShutdown();
      protocols.add(fastTrackProducer);
    }

    Set<Hash> seenFloods = new HashSet<>();
    List<Packet<?>> floods = fastTrack.getFloods();
    handleAllFloods(protocols, seenFloods, floods);
    Block latestBlock = fastTrack.blockchain.getLatestBlock();
    Assertions.assertThat(latestBlock.getBlockTime()).isGreaterThan(blockBefore.getBlockTime());
    // Listener is invoked async - sleep a bit
    sleep(100);
    for (FastTrackProducer protocol : protocols) {
      Assertions.assertThat(protocol.isShuttingDown()).isFalse();
    }
    protocols.forEach(WithCloseableResources::close);
    fastTrack.blockchain.close();
  }

  @Test
  public void produceAndThenShutdownShard() {
    final List<BlockchainTestHelper.TestProducer> producers =
        List.of(
            BlockchainTestHelper.firstProducer,
            BlockchainTestHelper.secondProducer,
            BlockchainTestHelper.thirdProducer,
            BlockchainTestHelper.fourthProducer);
    List<FastTrackProducer> protocols = new ArrayList<>();
    BlockchainTestHelper fastTrack =
        BlockchainTestHelper.createWithShardedFastTrack(
            BlockchainTestHelper.ENABLED_SHARD_ID, newFolder());

    for (BlockchainTestHelper.TestProducer producer : producers) {
      FastTrackProducer fastTrackProducer =
          new FastTrackProducer(
              MemoryStorage.createRootDirectory(newFolder()),
              fastTrack.blockchain,
              producer.key,
              producer.blsKey,
              2000,
              60_000,
              transactionSender,
              additionalStatusProviders);
      protocols.add(fastTrackProducer);
    }

    waitForFlood(fastTrack.getFloods());

    Set<Hash> seenFloods = new HashSet<>();
    List<Packet<?>> floods = fastTrack.getFloods();
    handleAllFloods(protocols, seenFloods, floods);

    List<BlockAndState> proposals = fastTrack.blockchain.getProposals();
    Assertions.assertThat(proposals).hasSize(1);

    protocols.forEach(FastTrackProducer::triggerShutdown);

    Block latestBlockForShutdown = fastTrack.blockchain.getLatestBlock();
    handleAllFloods(protocols, seenFloods, floods);
    Assertions.assertThat(fastTrack.blockchain.getBlockTime())
        .isGreaterThan(latestBlockForShutdown.getBlockTime() + 1);
    protocols.forEach(WithCloseableResources::close);
    fastTrack.blockchain.close();
  }

  private File newFolder() {
    File file = new File(temporaryFolder.toFile(), UUID.randomUUID().toString());
    Assertions.assertThat(file.mkdirs()).isTrue();
    return file;
  }

  private void handleAllFloods(
      List<FastTrackProducer> protocols, Set<Hash> seenFloods, List<Packet<?>> floods) {
    while (!floods.isEmpty()) {
      Packet<?> next = floods.remove(0);
      if (next.getType() == Packet.Type.FINALIZATION) {
        FinalizationData payload = (FinalizationData) next.getPayload();
        if (seenFloods.add(Hash.create(stream -> stream.write(payload.getData())))) {
          for (FastTrackProducer protocol : protocols) {
            protocol.onFinalizationMessage(Mockito.mock(Connection.class), payload);
          }
        }
      }
      // Ensure we do not loop indefinitely

      Assertions.assertThat(seenFloods).hasSizeLessThan(1000);
    }
  }

  static BlsSignature signAndAggregate(List<BlsKeyPair> keys, Hash message) {
    return keys.stream()
        .map(k -> k.sign(message))
        .reduce(null, (s0, s1) -> s0 == null ? s1 : s0.addSignature(s1));
  }

  /** Create a final fast track block. */
  public static FinalBlock createFinalBlock(Block block) {
    Hash echoHash = block.identifier();
    return new FinalBlock(
        block,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeDynamicBytes(new byte[] {0b1101});
              BlsSignature blsSignature =
                  signAndAggregate(
                      List.of(
                          BlockchainTestHelper.firstProducer.blsKey,
                          BlockchainTestHelper.fourthProducer.blsKey,
                          BlockchainTestHelper.thirdProducer.blsKey),
                      echoHash);
              blsSignature.write(s);
            }));
  }

  @Test
  public void resources() {
    createFastTrackWithNotPayableSigner();

    Assertions.assertThat(blockProducer.resources()).hasSize(4);
  }

  @Test
  public void epochChangeTriggersRewardTransaction() throws InterruptedException {
    List<DataStreamSerializable> payloads = new ArrayList<>();

    transactionSender = payloads::add;
    createFastTrack(BlockchainTestHelper.secondProducer);

    long initialBlockProductionTime = System.currentTimeMillis() - (1 << 21);
    fullResponse(initialBlockProductionTime);
    fullResponse(initialBlockProductionTime + 1);
    fullResponse(initialBlockProductionTime + (1 << 21));

    BlockAndState latest = latestProposal().current();
    long productionTime = latest.getBlock().getProductionTime() + 1;
    Block block = createBlock(latest, 0, productionTime);
    fullBlockResponse(block);

    Thread.sleep(1000);

    Assertions.assertThat(payloads).hasSize(1);
    DataStreamSerializable interact = payloads.get(0);

    byte[] expectedPayload =
        SafeDataOutputStream.serialize(
            s ->
                new EpochEndPayload(null, initialBlockProductionTime >>> 21, new int[] {1, 1, 1, 1})
                    .write(s));

    Assertions.assertThat(SafeDataOutputStream.serialize(interact)).isEqualTo(expectedPayload);
  }

  @Test
  void sizeConstrainedCacheContainsLatest100Entries() {
    FastTrackProducer.SizeConstrainedCache sizeConstrainedCache =
        new FastTrackProducer.SizeConstrainedCache();
    HashMap<Hash, Integer> map = new HashMap<>();
    for (int i = 0; i < 100; i++) {
      putEntry(map, i, sizeConstrainedCache);
    }
    Assertions.assertThat(sizeConstrainedCache)
        .hasSize(100)
        .containsExactlyInAnyOrderEntriesOf(map);

    Map.Entry<Hash, Integer> firstEntry = sizeConstrainedCache.entrySet().iterator().next();
    Assertions.assertThat(firstEntry.getValue()).isEqualTo(0);
    map.remove(firstEntry.getKey());

    putEntry(map, 100, sizeConstrainedCache);
    Assertions.assertThat(sizeConstrainedCache)
        .hasSize(100)
        .containsExactlyInAnyOrderEntriesOf(map);
  }

  private static void putEntry(
      Map<Hash, Integer> map,
      int uniqueKey,
      FastTrackProducer.SizeConstrainedCache sizeConstrainedCache) {
    Hash key = Hash.create(s -> s.writeInt(uniqueKey));
    map.put(key, uniqueKey);
    sizeConstrainedCache.put(key, uniqueKey);
  }

  private void fullResponse(long productionTime) {
    BlockAndState latest = latestProposal().current();
    Block block = createBlock(latest, 0, productionTime);
    fullBlockResponse(block);
  }

  private Block createBlock(BlockAndState latest) {
    return createBlock(latest, 0);
  }

  private Block createBlock(BlockAndState latest, int expectedProducer) {
    return createBlock(latest, expectedProducer, System.currentTimeMillis());
  }

  private Block createBlock(BlockAndState latest, int expectedProducer, long productionTime) {
    return new Block(
        productionTime,
        latest.getBlock().getBlockTime() + 1,
        latest.getBlock().getCommitteeId(),
        latest.getBlock().identifier(),
        latest.getState().getHash(),
        List.of(),
        List.of(),
        (short) expectedProducer);
  }

  private FinalizationData finalizationData(Consumer<SafeDataOutputStream> data) {
    return FinalizationData.create(SafeDataOutputStream.serialize(data));
  }

  private void sleep(int sleepTime) {
    ExceptionConverter.run(() -> Thread.sleep(sleepTime), "");
  }
}
