package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class DeterministicRandomBitGeneratorTest {

  private final Random random = new Random(897897);

  @Test
  public void updateAndReseed() {
    DeterministicRandomBitGenerator bitGenerator =
        DeterministicRandomBitGenerator.createForTest(new byte[32]);
    bitGenerator.nextBytes(new byte[32]);
    Assertions.assertThatThrownBy(() -> bitGenerator.nextBytes(new byte[8]))
        .hasMessageContaining("Ran out of AES values");
  }

  @Test
  public void loopFallthroughIncrementCounter() {
    // Combination of key and counter that ensures that the initial counter is all -1 bytes
    byte[] entropy =
        new byte[] {
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          -72, -114, -25, -23, 22, -30, -112, 15, -90, 68, 64, -44, 10, 113, -16, 44
        };
    DeterministicRandomBitGenerator bitGenerator = new DeterministicRandomBitGenerator(entropy);
    byte[] result = new byte[16];
    bitGenerator.nextBytes(result);
    Assertions.assertThat(result)
        .isEqualTo(new byte[] {96, -69, 51, 36, 19, 1, -61, -32, 23, 34, -61, -95, 10, 3, 18, 67});
  }

  @Test
  public void remainingBytesBoundary() {
    DeterministicRandomBitGenerator generator = new DeterministicRandomBitGenerator(new byte[32]);
    byte[] result = new byte[1];
    generator.nextBytes(result);
    Assertions.assertThat(result[0]).isEqualTo((byte) 48);
  }

  @Test
  public void generateMultipleBlocksShouldBeSameAsOne() {
    DeterministicRandomBitGenerator first = new DeterministicRandomBitGenerator(new byte[32]);
    DeterministicRandomBitGenerator second = new DeterministicRandomBitGenerator(new byte[32]);

    byte[] firstResult = new byte[15];
    first.nextBytes(firstResult);

    byte[] secondResult = new byte[70];
    second.nextBytes(secondResult);

    Assertions.assertThat(secondResult).startsWith(firstResult);
  }

  @Test
  public void withInitialEntropy() {
    byte[] entropy = new byte[32];
    random.nextBytes(entropy);

    DeterministicRandomBitGenerator generator = new DeterministicRandomBitGenerator(entropy);
    byte[] generatedBytes = new byte[32];
    generator.nextBytes(generatedBytes);
    Assertions.assertThat(generatedBytes)
        .isEqualTo(
            new byte[] {
              91, 56, 77, -76, 75, -102, 97, 19, 13, -44, 117, -35, 89, -111, -98, -34,
              -34, -80, -73, -42, -26, 57, -118, -89, -64, -22, -117, 45, -47, 29, -26, -1
            });
  }

  @Test
  public void withOffset() {
    DeterministicRandomBitGenerator generator = new DeterministicRandomBitGenerator(new byte[32]);
    byte[] generatedBytes = new byte[16];
    generator.nextBytes(generatedBytes, 1, 15);
    Assertions.assertThat(generatedBytes)
        .isEqualTo(
            new byte[] {0, 48, 30, 62, 116, -71, 4, 106, -63, -80, 35, -66, -86, 113, 86, -42});
  }
}
