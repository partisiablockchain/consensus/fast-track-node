package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.JarHelper;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.consensus.fasttrack.CommitteeMember;
import com.partisiablockchain.consensus.fasttrack.FastTrackProducerTest;
import com.partisiablockchain.consensus.fasttrack.MemoryStorage;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.BlockChainNetwork;
import com.partisiablockchain.flooding.Connection;
import com.partisiablockchain.flooding.NetworkFloodable;
import com.partisiablockchain.flooding.Packet;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.thread.ExecutionFactoryThreaded;
import java.io.File;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;

/** Test helper. */
public final class BlockchainTestHelper {

  /** Container for producer information. */
  public static final class TestProducer {

    public final KeyPair key;
    public final BlsKeyPair blsKey;
    public final BlockchainAddress address;
    public final BlockchainPublicKey publicKey;

    TestProducer(BigInteger key, BigInteger blsKey) {
      this.key = new KeyPair(key);
      this.blsKey = new BlsKeyPair(blsKey);
      this.address = addressFromKey(this.key);
      this.publicKey = this.key.getPublic();
    }
  }

  public static final TestProducer firstProducer =
      new TestProducer(BigInteger.ONE, BigInteger.valueOf(11));
  public static final TestProducer secondProducer =
      new TestProducer(BigInteger.TWO, BigInteger.valueOf(12));
  public static final TestProducer thirdProducer =
      new TestProducer(BigInteger.valueOf(3), BigInteger.valueOf(13));
  public static final TestProducer fourthProducer =
      new TestProducer(BigInteger.valueOf(4), BigInteger.valueOf(14));
  public static final TestProducer fifthProducer =
      new TestProducer(BigInteger.valueOf(5), BigInteger.valueOf(15));
  public static final TestProducer sixthProducer =
      new TestProducer(BigInteger.valueOf(6), BigInteger.valueOf(16));

  public static final List<KeyPair> keyPairs =
      IntStream.range(0, 0xFF + 2)
          .mapToObj(i -> new KeyPair(BigInteger.valueOf(88888 + i)))
          .collect(Collectors.toList());

  private static final List<BlockchainAddress> accounts =
      keyPairs.stream().map(BlockchainTestHelper::addressFromKey).collect(Collectors.toList());

  public static final TestProducer notPayable =
      new TestProducer(BigInteger.valueOf(11223344), BigInteger.valueOf(44332211));

  public static final String ENABLED_SHARD_ID = "Shard";
  private final EmptyBlockchainNetwork network;
  public static final Hash INITIAL_FT_COMMITTEE = Hash.create(stream -> stream.writeInt(0));
  private static final List<TestProducer> PRODUCERS =
      List.of(
          firstProducer,
          secondProducer,
          thirdProducer,
          fourthProducer,
          fifthProducer,
          sixthProducer);
  private static final BlockchainAddress UPDATE_FT_CONTRACT =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV, Hash.create(s -> s.writeString("UpdateFastTrack")));

  private static BlockchainAddress addressFromKey(KeyPair keyPair) {
    return keyPair.getPublic().createAddress();
  }

  public final BlockchainLedger blockchain;

  /** Construct with only root and no round robin consensus. */
  public static BlockchainTestHelper createEmpty(Path folder) {
    return new BlockchainTestHelper(folder, BlockchainTestHelper::setRoot);
  }

  /** Construct with fast track with three committees. */
  public static BlockchainTestHelper createWithFastTrackThreeCommittees(Path folder) {
    return new BlockchainTestHelper(
        folder,
        mutableChainState -> {
          setFastTrack(mutableChainState);
          for (int i = 0; i < 2; i++) {
            int finalI = i;
            updateGlobalPluginState(mutableChainState, finalI);
          }
        });
  }

  private static void updateGlobalPluginState(MutableChainState mutableChainState, int finalI) {
    mutableChainState.updateGlobalPluginState(
        0,
        ChainPluginType.CONSENSUS,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(0);
              Hash.create(hashStream -> hashStream.writeString("NewCommittee" + finalI)).write(s);
              CommitteeMember.LIST_STREAM.writeDynamic(
                  s,
                  PRODUCERS.stream()
                      .map(
                          producer ->
                              new CommitteeMember(
                                  producer.address,
                                  producer.publicKey,
                                  producer.blsKey.getPublicKey()))
                      .collect(Collectors.toList()));
            }));
  }

  /** Create with fast track with 6 members. */
  public static BlockchainTestHelper createWithFastTrackHavingSixMembers(Path folder) {
    return new BlockchainTestHelper(folder, BlockchainTestHelper::setFastTrackSixMembers);
  }

  /** Construct with fast track. */
  public static BlockchainTestHelper createWithFastTrack(Path folder) {
    return new BlockchainTestHelper(folder, BlockchainTestHelper::setFastTrack);
  }

  private static BlockchainTestHelper createWithFastTrack(File folder, Hash committeeId) {
    BlockchainTestHelper blockchainTestHelper =
        new BlockchainTestHelper(BlockchainTestHelper::initFastTrackProducers, folder);
    blockchainTestHelper.appendBlock(
        firstProducer.key,
        List.of(
            blockchainTestHelper.createTransaction(
                firstProducer.key,
                Long.MAX_VALUE,
                InteractWithContractTransaction.create(
                    UPDATE_FT_CONTRACT,
                    SafeDataOutputStream.serialize(
                        s -> {
                          s.writeDynamicBytes(JarHelper.getFastTrackPluginJar());
                          s.writeDynamicBytes(fastTrackInit(committeeId));
                        })))));
    blockchainTestHelper.appendFastTrackBlock();
    blockchainTestHelper.appendFastTrackBlock();
    return blockchainTestHelper;
  }

  private void appendFastTrackBlock() {
    List<BlockAndState> proposals = blockchain.getProposals();
    BlockAndState parent = blockchain.latest();
    if (!proposals.isEmpty()) {
      parent = proposals.get(0);
    }
    Block block = createBlock(List.of(), parent);
    FinalBlock finalBlock = FastTrackProducerTest.createFinalBlock(block);
    blockchain.appendBlock(finalBlock);
  }

  /** Construct with fast track an enable shard. */
  public static BlockchainTestHelper createWithFastTrackAndEnabledShard(File folder) {
    return createWithFastTrack(folder, INITIAL_FT_COMMITTEE);
  }

  /**
   * Initialize the blockchain with a sharded fast track.
   *
   * @param shardId shard id to start
   * @param folder folder used for storage
   * @return the created helper
   */
  public static BlockchainTestHelper createWithShardedFastTrack(String shardId, File folder) {
    return new BlockchainTestHelper(folder, shardId);
  }

  private static void initFastTrackProducers(MutableChainState mutableChainState) {
    setRoot(mutableChainState);
    for (TestProducer producer : PRODUCERS) {
      createAccount(mutableChainState, producer.address);
    }
    Hash binder = mutableChainState.saveJar(JarHelper.getSysBinder());
    byte[] jar = JarBuilder.buildJar(ConsensusPluginContract.class);
    Hash contract = mutableChainState.saveJar(jar);
    Hash abi = mutableChainState.saveJar(new byte[0]);

    mutableChainState.createContract(
        UPDATE_FT_CONTRACT, CoreContractState.create(binder, contract, abi, jar.length));
    mutableChainState.setContractState(UPDATE_FT_CONTRACT, null);
  }

  private static void setFastTrackSixMembers(MutableChainState mutableChainState) {
    setFastTrackSixMembers(mutableChainState, INITIAL_FT_COMMITTEE);
  }

  private static void setFastTrackSixMembers(MutableChainState mutableChainState, Hash committee) {
    initFastTrackProducers(mutableChainState);
    mutableChainState.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.CONSENSUS,
        JarHelper.getFastTrackPluginJar(),
        SafeDataOutputStream.serialize(
            stream -> {
              CommitteeMember.LIST_STREAM.writeDynamic(
                  stream,
                  PRODUCERS.stream()
                      .map(
                          producer ->
                              new CommitteeMember(
                                  producer.address,
                                  producer.publicKey,
                                  producer.blsKey.getPublicKey()))
                      .collect(Collectors.toList()));
              committee.write(stream);
            }));
  }

  private static void setFastTrack(MutableChainState mutableChainState) {
    setFastTrack(mutableChainState, INITIAL_FT_COMMITTEE);
  }

  private static void setFastTrack(MutableChainState mutableChainState, Hash committee) {
    initFastTrackProducers(mutableChainState);
    mutableChainState.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.CONSENSUS,
        JarHelper.getFastTrackPluginJar(),
        fastTrackInit(committee));
  }

  /** Construct with large amount of accounts and an active fee plugin. */
  public static BlockchainTestHelper withManyAccountsAndFee(Path folder) {
    return new BlockchainTestHelper(
        folder,
        mutableChainState -> {
          setRoot(mutableChainState);
          createAdditionAccounts(mutableChainState);
          createFeePlugin(mutableChainState);
        });
  }

  private static void createFeePlugin(MutableChainState mutableChainState) {
    mutableChainState.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ACCOUNT,
        JarBuilder.buildJar(MyFeePlugin.class),
        new byte[0]);
    createAccount(mutableChainState, notPayable.address);
  }

  private static void createAdditionAccounts(MutableChainState mutableChainState) {
    for (BlockchainAddress account : accounts) {
      createAccount(mutableChainState, account);
    }
  }

  private static void setRoot(MutableChainState mutableChainState) {
    createAccount(mutableChainState, firstProducer.address);
  }

  private static void createAccount(
      MutableChainState mutableChainState, BlockchainAddress firstProducerAccount) {
    if (mutableChainState.getAccount(firstProducerAccount) == null) {
      mutableChainState.createAccount(firstProducerAccount);
    }
  }

  private BlockchainTestHelper(Path folder, Consumer<MutableChainState> initial) {
    this(initial, folder.toFile());
  }

  private BlockchainTestHelper(Consumer<MutableChainState> initial, File root) {
    this.network = new EmptyBlockchainNetwork();
    this.blockchain =
        BlockchainLedger.createForTest(
            MemoryStorage.createRootDirectory(root), initial, network, null);
  }

  private BlockchainTestHelper(File folder, String shardId) {
    this.network = new EmptyBlockchainNetwork();
    this.blockchain =
        BlockchainLedger.createShardForTest(
            MemoryStorage.createRootDirectory(folder),
            mut -> {
              setFastTrack(mut, INITIAL_FT_COMMITTEE);
              mut.addActiveShard(FunctionUtility.noOpBiConsumer(), null, ENABLED_SHARD_ID);
            },
            shardId,
            network,
            null,
            FunctionUtility.noOpConsumer(),
            ExecutionFactoryThreaded.create());
  }

  /**
   * Create a BlockAndStateWithParent where the block contains a single event transaction.
   *
   * @return the created BlockAndStateWithParent
   */
  public static BlockAndStateWithParent parentWithEvent() {
    ImmutableChainState state = stateWithoutFastTrack("Shard");
    return new BlockAndStateWithParent(
        new BlockAndState(
            new FinalBlock(
                new Block(
                    0,
                    0,
                    0,
                    Block.GENESIS_PARENT,
                    Block.GENESIS_PARENT,
                    List.of(INITIAL_FT_COMMITTEE),
                    List.of()),
                new byte[0]),
            state,
            null),
        state,
        null);
  }

  private static byte[] fastTrackInit(Hash committee) {
    return SafeDataOutputStream.serialize(
        stream -> {
          CommitteeMember.LIST_STREAM.writeDynamic(
              stream,
              PRODUCERS.subList(0, 4).stream()
                  .map(
                      producer ->
                          new CommitteeMember(
                              producer.address, producer.publicKey, producer.blsKey.getPublicKey()))
                  .collect(Collectors.toList()));
          committee.write(stream);
        });
  }

  /**
   * Create an immutable chain state that is in sync mode.
   *
   * @return the created state
   */
  public static ImmutableChainState syncingState() {
    StateStorage storage = createMemoryStateStorage();
    MutableChainState state = MutableChainState.emptyMaster(new ChainStateCache(storage));
    state.addActiveShard(FunctionUtility.noOpBiConsumer(), null, ENABLED_SHARD_ID);
    state.addActiveShard(FunctionUtility.noOpBiConsumer(), null, "AdditionalShard");
    Assertions.assertThat(state.isSyncing()).isTrue();
    return state.asImmutable("ChainId", null, AvlTree.create(), AvlTree.create());
  }

  /** Create a state without fast track enabled. */
  public static ImmutableChainState stateWithoutFastTrack(String shardId) {
    StateStorage storage = createMemoryStateStorage();
    MutableChainState state = MutableChainState.emptyMaster(new ChainStateCache(storage));
    return state.asImmutable("ChainId", shardId, AvlTree.create(), AvlTree.create());
  }

  public List<Packet<?>> getFloods() {
    return network.queuedFlood;
  }

  public List<Packet<?>> getNetworkRequests() {
    return network.queuedRequest;
  }

  /**
   * Append a new block to the contained blockchain.
   *
   * @param producer the keypair to use in block
   */
  public void appendEmptyBlock(KeyPair producer) {
    appendBlock(producer, List.of());
  }

  /** Append a new block to the contained blockchain. */
  public void appendBlock(KeyPair producer, List<SignedTransaction> transactions) {
    appendBlock(producer, transactions, blockchain.latest());
  }

  /** Append a new block to the contained blockchain. */
  public void appendBlock(
      KeyPair producer, List<SignedTransaction> transactions, BlockAndState latest) {
    appendBlock(producer, transactions, latest, -1);
  }

  /** Append a new block to the contained blockchain. */
  public void appendBlock(
      KeyPair producer,
      List<SignedTransaction> transactions,
      BlockAndState latest,
      int producerIndex) {
    transactions.forEach(blockchain::addPendingTransaction);
    Block block = createBlock(transactions, latest, producerIndex);
    blockchain.appendBlock(
        new FinalBlock(block, SafeDataOutputStream.serialize(producer.sign(block.identifier()))));
  }

  private Block createBlock(List<SignedTransaction> transactions, BlockAndState latest) {
    return createBlock(transactions, latest, -1);
  }

  /**
   * Create a block.
   *
   * @param transactions the list of transactions to include
   * @param latest the latest state
   * @param producerIndex the index of the producer
   * @return the new block
   */
  public Block createBlock(
      List<SignedTransaction> transactions, BlockAndState latest, int producerIndex) {
    return new Block(
        System.currentTimeMillis(),
        latest.getBlock().getBlockTime() + 1,
        latest.getBlock().getCommitteeId(),
        latest.getBlock().identifier(),
        latest.getState().getHash(),
        List.of(),
        transactions.stream().map(SignedTransaction::identifier).collect(Collectors.toList()),
        producerIndex);
  }

  /**
   * Create a transaction from the supplied signer.
   *
   * @param signer sender of transaction
   * @return the created transaction
   */
  public SignedTransaction createTransaction(KeyPair signer) {
    return createTransaction(signer, Long.MAX_VALUE);
  }

  /** Create a valid transaction. */
  public SignedTransaction createTransaction(KeyPair signer, long validTo) {
    return createTransaction(signer, validTo, createDeploy());
  }

  /** Create a valid transaction. */
  public SignedTransaction createTransaction(
      KeyPair signer, long validTo, InteractWithContractTransaction deploy) {
    AccountState accountState = blockchain.getAccountState(addressFromKey(signer));
    return SignedTransaction.create(
            CoreTransactionPart.create(accountState.getNonce(), validTo, 10000), deploy)
        .sign(signer, chainId());
  }

  /**
   * Create an empty deploy transaction.
   *
   * @return the created transaction
   */
  public InteractWithContractTransaction createDeploy() {
    return InteractWithContractTransaction.create(
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(s -> s.writeInt(1))),
        new byte[32]);
  }

  /**
   * Get chain id from the blockchain.
   *
   * @return chain id
   */
  public String chainId() {
    return blockchain.getChainId();
  }

  /**
   * Send an incoming finalization message.
   *
   * @param mock the sending connection
   * @param finalizationData the message to send
   */
  public void incomingFinalizationMessage(Connection mock, FinalizationData finalizationData) {
    blockchain.incomingFinalizationData(mock, finalizationData);
  }

  /** Create memory state storage. */
  public static StateStorageRaw createMemoryStateStorage() {
    return new StateStorageRaw() {

      private final Map<Hash, byte[]> serialized = new HashMap<>();

      @Override
      public synchronized boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
        if (serialized.containsKey(hash)) {
          return false;
        }
        serialized.put(hash, SafeDataOutputStream.serialize(writer));
        return true;
      }

      @Override
      public synchronized <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
        if (serialized.containsKey(hash)) {
          SafeDataInputStream bytes = SafeDataInputStream.createFromBytes(serialized.get(hash));
          return reader.apply(bytes);
        } else {
          return null;
        }
      }

      @Override
      public synchronized byte[] read(Hash hash) {
        return serialized.get(hash);
      }
    };
  }

  private static final class EmptyBlockchainNetwork implements BlockChainNetwork {

    private final List<Packet<?>> queuedFlood = new ArrayList<>();
    private final List<Packet<?>> queuedRequest = new ArrayList<>();

    @Override
    public <T extends NetworkFloodable> void sendToAll(Packet<T> packet) {
      queuedFlood.add(packet);
    }

    @Override
    public void sendToAny(Packet<?> packet) {
      queuedRequest.add(packet);
    }

    @Override
    public void close() {}
  }

  /** Noop fee plugin. */
  @Immutable
  public static final class MyFeePlugin
      extends BlockchainAccountPlugin<StateVoid, StateVoid, StateVoid, StateVoid> {

    @Override
    public long convertNetworkFee(StateVoid stateVoid, long l) {
      return 0;
    }

    @Override
    public boolean canCoverCost(
        PluginContext pluginContext,
        StateVoid globalState,
        BlockchainAccountPlugin.AccountState<StateVoid, StateVoid> localState,
        SignedTransaction transaction) {
      BlockchainAddress nonPayable =
          new KeyPair(BigInteger.valueOf(11223344)).getPublic().createAddress();
      return !transaction.getSender().equals(nonPayable);
    }

    @Override
    public PayCostResult<StateVoid, StateVoid> payCost(
        PluginContext pluginContext,
        StateVoid globalState,
        BlockchainAccountPlugin.AccountState<StateVoid, StateVoid> localState,
        SignedTransaction transaction) {
      return null;
    }

    @Override
    public ContractState<StateVoid, StateVoid> contractCreated(
        PluginContext pluginContext,
        StateVoid globalState,
        StateVoid contextFreeState,
        BlockchainAddress address) {
      return null;
    }

    @Override
    public StateVoid removeContract(
        PluginContext pluginContext,
        StateVoid globalState,
        ContractState<StateVoid, StateVoid> localState) {
      return null;
    }

    @Override
    public ContractState<StateVoid, StateVoid> updateContractGasBalance(
        PluginContext pluginContext,
        StateVoid globalState,
        ContractState<StateVoid, StateVoid> localState,
        BlockchainAddress contract,
        long gas) {
      return null;
    }

    @Override
    public StateVoid registerBlockchainUsage(
        PluginContext pluginContext, StateVoid globalState, StateVoid contextFreeState, long gas) {
      return null;
    }

    @Override
    public PayServiceFeesResult<ContractState<StateVoid, StateVoid>> payServiceFees(
        PluginContext pluginContext,
        StateVoid globalState,
        ContractState<StateVoid, StateVoid> contractState,
        List<PendingFee> pendingFees) {
      return null;
    }

    @Override
    public StateVoid payInfrastructureFees(
        PluginContext pluginContext,
        StateVoid stateVoid,
        StateVoid stateVoid2,
        List<PendingFee> list) {
      return null;
    }

    @Override
    public StateVoid updateForBlock(
        StateVoid stateVoid, StateVoid stateVoid2, BlockContext<StateVoid> blockContext) {
      return null;
    }

    @Override
    public ContractState<StateVoid, StateVoid> updateActiveContract(
        PluginContext pluginContext,
        StateVoid globalState,
        ContractState<StateVoid, StateVoid> currentState,
        BlockchainAddress contract,
        long size) {
      return null;
    }

    @Override
    protected StateVoid migrateContextFree(StateAccessor stateAccessor) {
      return null;
    }

    @Override
    protected StateVoid migrateContract(
        BlockchainAddress blockchainAddress, StateAccessor stateAccessor) {
      return null;
    }

    @Override
    protected StateVoid migrateAccount(StateAccessor current) {
      return null;
    }

    @Override
    protected InvokeResult<ContractState<StateVoid, StateVoid>> invokeContract(
        PluginContext pluginContext,
        StateVoid global,
        ContractState<StateVoid, StateVoid> current,
        BlockchainAddress address,
        byte[] rpc) {
      return null;
    }

    @Override
    protected InvokeResult<BlockchainAccountPlugin.AccountState<StateVoid, StateVoid>>
        invokeAccount(
            PluginContext pluginContext,
            StateVoid global,
            BlockchainAccountPlugin.AccountState<StateVoid, StateVoid> current,
            byte[] rpc) {
      return null;
    }

    @Override
    protected InvokeResult<StateVoid> invokeContextFree(
        PluginContext pluginContext, StateVoid global, StateVoid contextFree, byte[] rpc) {
      return null;
    }

    @Override
    protected StateVoid payByocFees(
        PluginContext pluginContext,
        StateVoid globalState,
        StateVoid localState,
        Unsigned256 amount,
        String symbol,
        FixedList<BlockchainAddress> nodes) {
      return null;
    }

    @Override
    public Class<StateVoid> getGlobalStateClass() {
      return StateVoid.class;
    }

    @Override
    public List<Class<?>> getLocalStateClassTypeParameters() {
      return List.of(StateVoid.class, StateVoid.class, StateVoid.class);
    }

    @Override
    public InvokeResult<StateVoid> invokeGlobal(
        PluginContext pluginContext, StateVoid state, byte[] rpc) {
      return null;
    }

    @Override
    public StateVoid migrateGlobal(
        StateAccessor stateAccessor, SafeDataInputStream safeDataInputStream) {
      return null;
    }
  }

  /** Test contract for deploying a consensus plugin. */
  public static final class ConsensusPluginContract extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      byte[] jar = rpc.readDynamicBytes();
      byte[] init = rpc.readDynamicBytes();
      context.getInvocationCreator().updateConsensusPlugin(jar, init);
      return null;
    }
  }
}
