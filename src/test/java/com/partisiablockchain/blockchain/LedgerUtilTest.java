package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.SyncEvent;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LedgerUtilTest {

  private static final Hash hash = Hash.create(s -> s.writeInt(897645312));

  @Test
  public void shouldSortToEnsureNonceAreCheckedInOrder() {
    List<ExecutableEvent> selected =
        LedgerUtil.selectValidEvents(
            false,
            AvlTree.create(),
            List.of(event("1", 2, 10), event(null, 1, 10), event("1", 1, 10)),
            100L);
    Assertions.assertThat(selected).hasSize(3);
  }

  @Test
  public void shouldFilterInvalidNonce() {
    List<ExecutableEvent> selected =
        LedgerUtil.selectValidEvents(
            false,
            AvlTree.<ShardId, ShardNonces>create()
                .set(new ShardId("1"), ShardNonces.EMPTY.bumpInbound(10)),
            List.of(event("1", 1, 10), event("1", 3, 10)),
            100L);
    Assertions.assertThat(selected).hasSize(0);
  }

  @Test
  public void shouldNotAllowDecreasingCommitteeId() {
    List<ExecutableEvent> selected =
        LedgerUtil.selectValidEvents(
            false, AvlTree.create(), List.of(event("1", 1, 11), event("1", 2, 10)), 100L);
    Assertions.assertThat(selected).hasSize(1);
  }

  @Test
  public void filterInvalidCommitteeId() {
    List<ExecutableEvent> selected =
        LedgerUtil.selectValidEvents(
            false,
            AvlTree.<ShardId, ShardNonces>create()
                .set(new ShardId("1"), ShardNonces.EMPTY.bumpInbound(10)),
            List.of(event("1", 2, 9)),
            100L);
    Assertions.assertThat(selected).hasSize(0);
  }

  @Test
  public void filterTooLargeGovernanceVersion() {
    List<ExecutableEvent> selected =
        LedgerUtil.selectValidEvents(
            false,
            AvlTree.<ShardId, ShardNonces>create().set(new ShardId("1"), ShardNonces.EMPTY),
            List.of(event("1", 1, 1, 9)),
            9L);
    Assertions.assertThat(selected).hasSize(1);
    selected =
        LedgerUtil.selectValidEvents(
            false,
            AvlTree.<ShardId, ShardNonces>create().set(new ShardId("1"), ShardNonces.EMPTY),
            List.of(event("1", 1, 1, 10)),
            9L);
    Assertions.assertThat(selected).hasSize(0);
  }

  @Test
  public void areEventsValidSync() {
    ExecutableEvent syncEvent =
        new ExecutableEvent(
            "1",
            new EventTransaction(
                hash,
                new ShardRoute("dest", 1L),
                2,
                0,
                0,
                null,
                new SyncEvent(List.of(), List.of(), List.of())));

    Assertions.assertThat(
            LedgerUtil.selectValidEvents(true, AvlTree.create(), List.of(syncEvent), 100))
        .describedAs("Sync not allowed when not waiting for sync")
        .hasSize(0);

    Assertions.assertThat(
            LedgerUtil.selectValidEvents(false, AvlTree.create(), List.of(syncEvent), 100))
        .describedAs("Sync not allowed when not sync")
        .hasSize(0);

    AvlTree<ShardId, ShardNonces> pendingSync =
        AvlTree.<ShardId, ShardNonces>create()
            .set(new ShardId("1"), ShardNonces.EMPTY.markMissingSync());
    Assertions.assertThat(LedgerUtil.selectValidEvents(true, pendingSync, List.of(syncEvent), 100))
        .describedAs("Sync allowed when waiting for sync")
        .hasSize(1);

    ExecutableEvent first = event("1", 2, 7);
    Assertions.assertThat(
            LedgerUtil.selectValidEvents(true, pendingSync, List.of(syncEvent, first), 100))
        .describedAs("Event not allowed after sync")
        .hasSize(1);
  }

  private ExecutableEvent event(String shard, long nonce, int committeeId) {
    return event(shard, nonce, committeeId, 0);
  }

  private ExecutableEvent event(String shard, long nonce, int committeeId, int governanceVersion) {
    return new ExecutableEvent(
        shard,
        new EventTransaction(
            hash,
            new ShardRoute("dest", nonce),
            committeeId,
            governanceVersion,
            1,
            null,
            new InnerSystemEvent.SetFeatureEvent("", "")));
  }
}
