package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

final class JarBuilder {

  /**
   * Builds a jar containing the supplied class as main class and any inner classes declared.
   *
   * @param mainClass the main contract class
   * @return the jar
   */
  static byte[] buildJar(Class<?> mainClass) {
    return writeClassAndInner(mainClass);
  }

  private static byte[] writeClassAndInner(Class<?> mainClass) {
    return ExceptionConverter.call(
        () -> {
          ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
          ZipOutputStream jar = new ZipOutputStream(byteStream);
          String className = mainClass.getName();
          jar.putNextEntry(new ZipEntry("main"));
          jar.write(className.getBytes(StandardCharsets.UTF_8));

          writeDirectoryStructure(jar, className);

          writeClass(jar, className);
          for (Class<?> subClasses : mainClass.getDeclaredClasses()) {
            writeClass(jar, subClasses.getName());
          }
          jar.closeEntry();
          jar.close();

          return byteStream.toByteArray();
        },
        "Unable to create jar file");
  }

  private static void writeDirectoryStructure(ZipOutputStream jar, String className)
      throws IOException {
    String classNameAsPath = nameToPath(className);
    int nextPathSeparator = classNameAsPath.indexOf('/');
    while (nextPathSeparator > -1) {
      int endIndex = nextPathSeparator + 1;
      jar.putNextEntry(new ZipEntry(classNameAsPath.substring(0, endIndex)));
      nextPathSeparator = classNameAsPath.indexOf('/', endIndex);
    }
  }

  private static void writeClass(ZipOutputStream jar, String className) throws IOException {
    String classNameAsPath = nameToPath(className);
    String fileName = classNameAsPath + ".class";
    jar.putNextEntry(new ZipEntry(fileName));
    WithResource.accept(
        () -> JarBuilder.class.getResourceAsStream("/" + fileName),
        stream -> stream.transferTo(jar),
        "");
  }

  private static String nameToPath(String className) {
    return className.replace('.', '/');
  }
}
