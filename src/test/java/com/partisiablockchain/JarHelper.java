package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.jarutil.Dependency;
import com.secata.jarutil.PomDependencies;

/** Helper to load jar files. */
public final class JarHelper {

  private static final PomDependencies pom = PomDependencies.readFromPom();

  private static byte[] readJar(String groupId, String artifactId) {
    Dependency dependency = pom.lookup(groupId, artifactId);
    return dependency.readFromRepository();
  }

  public static byte[] getFastTrackPluginJar() {
    return readJar("com.partisiablockchain.consensus.fasttrack", "plugin");
  }

  public static byte[] getSysBinder() {
    return readJar("com.partisiablockchain.governance", "sys-binder");
  }
}
