package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.tree.AvlTree;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/** Utility for accessing the ledger. */
public final class LedgerUtil {

  private LedgerUtil() {}

  /**
   * Select all events that are valid according to the current shardNonces.
   *
   * @param syncing true if we are currently awaiting sync messages
   * @param shardNonces the current shard nonces
   * @param values all available events
   * @param maximalGovernanceVersion the maximal allowed governance version
   * @return the list of valid events
   */
  public static List<ExecutableEvent> selectValidEvents(
      boolean syncing,
      AvlTree<ShardId, ShardNonces> shardNonces,
      Collection<ExecutableEvent> values,
      long maximalGovernanceVersion) {
    List<ExecutableEvent> sortedValues = new ArrayList<>(values);
    sortedValues.sort(Comparator.comparingLong(e -> e.getEvent().getNonce()));
    Map<String, Long> nextExpectedNonce = new HashMap<>();
    Map<String, Long> nextMinimalCommitteeId = new HashMap<>();
    Map<String, Boolean> missingSync = new HashMap<>();
    List<ExecutableEvent> events = new ArrayList<>();

    for (ExecutableEvent eventTransaction : sortedValues) {
      String originShard = eventTransaction.getOriginShard();
      long nextNonce =
          nextExpectedNonce.computeIfAbsent(
              originShard, shard -> getOrEmpty(shardNonces, shard).getInbound());
      long minimalCommitteeId =
          nextMinimalCommitteeId.computeIfAbsent(
              originShard, shard -> getOrEmpty(shardNonces, shard).getLatestInboundCommitteeId());
      EventTransaction event = eventTransaction.getEvent();
      boolean valid =
          event.getCommitteeId() >= minimalCommitteeId
              && event.getGovernanceVersion() <= maximalGovernanceVersion
              && event.getNonce() == nextNonce;
      boolean isSync = event.getInner().getEventType() == EventTransaction.EventType.SYNC;
      boolean syncValid;
      if (syncing) {
        // When syncing all events are allowed from shards not yet fully synced
        syncValid =
            Objects.requireNonNullElseGet(
                missingSync.get(originShard), () -> isMissingSync(shardNonces, originShard));
      } else {
        // Do not allow sync when not syncing
        syncValid = !isSync;
      }
      if (valid && syncValid) {
        nextMinimalCommitteeId.put(originShard, event.getCommitteeId());
        nextExpectedNonce.put(originShard, nextNonce + 1);
        missingSync.put(originShard, !isSync);
        events.add(eventTransaction);
      }
    }
    return events;
  }

  private static boolean isMissingSync(
      AvlTree<ShardId, ShardNonces> shardNonces, String originShard) {
    return getOrEmpty(shardNonces, originShard).isMissingSync();
  }

  private static ShardNonces getOrEmpty(AvlTree<ShardId, ShardNonces> shardNonces, String shard) {
    return Objects.requireNonNullElse(shardNonces.getValue(new ShardId(shard)), ShardNonces.EMPTY);
  }
}
