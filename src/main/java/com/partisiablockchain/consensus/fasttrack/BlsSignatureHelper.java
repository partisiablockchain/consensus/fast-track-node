package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.crypto.bls.BlsVerifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

final class BlsSignatureHelper<T extends BlsSignatureHelper.HasBlsSignature> {

  private BlsSignature signature = null;
  private BlsPublicKey publicKey = null;
  private boolean isDone = false;

  // contains all the signatures and the signers that contributed to the aggregated signature.
  private final Map<BlockchainAddress, T> goodSignatures = new HashMap<>();
  // contains signers that had previously contributed a bad BLS signature on the aggregated
  // signature.
  private final Map<BlockchainAddress, T> badSignatures = new HashMap<>();
  // establish a reverse lookup used for updating the two above maps.
  private final Map<BlsSignature, BlockchainAddress> signatureAuthors = new HashMap<>();

  private final Map<BlockchainAddress, BlsPublicKey> signers;
  private final Hash message;
  private final int threshold;

  public BlsSignatureHelper(Map<BlockchainAddress, BlsPublicKey> signers, Hash message) {
    this.signers = signers;
    this.message = message;
    int maliciousCount = (signers.size() - 1) / 3;
    this.threshold = signers.size() - maliciousCount;
  }

  int currentSignatureCount() {
    return goodSignatures.size();
  }

  /**
   * Add a new BLS signature.
   *
   * @param sigSig a signature used to recover the sender of the BLS signature
   * @param signature the BLS signature
   * @param message the message of sigSig
   * @return true if the BLS signature was accepted and false otherwise.
   */
  boolean addSignature(Signature sigSig, T signature, Hash message) {
    // check that we recognize the singer of the BLS signature
    BlsSignature blsSignature = signature.get();
    BlockchainAddress signer = sigSig.recoverSender(message);
    boolean expected = signers.containsKey(signer);
    // .. and check that this signer hasn't given us a signature already.
    boolean seen = goodSignatures.containsKey(signer) || badSignatures.containsKey(signer);
    if (!seen && expected) {
      this.signature = addSignature(this.signature, blsSignature);
      this.publicKey = addPublicKey(this.publicKey, signers.get(signer));
      goodSignatures.put(signer, signature);
      signatureAuthors.put(blsSignature, signer);
      return true;
    }
    return false;
  }

  private static BlsSignature addSignature(BlsSignature left, BlsSignature right) {
    if (left == null) {
      return right;
    } else {
      return left.addSignature(right);
    }
  }

  List<BlockchainAddress> getGoodSigners() {
    return new ArrayList<>(goodSignatures.keySet());
  }

  List<BlockchainAddress> getBadSigners() {
    return new ArrayList<>(badSignatures.keySet());
  }

  List<T> getGoodSignatures() {
    return new ArrayList<>(goodSignatures.values());
  }

  SignatureAndBitVector getSignature(List<BlockchainAddress> signers) {
    if (!check()) {
      return null;
    } else {
      byte[] signersBitVector = getSignersAsBitVector(signers);
      return new SignatureAndBitVector(signersBitVector, signature);
    }
  }

  int getThreshold() {
    return threshold;
  }

  boolean check() {
    if (!isDone && goodSignatures.size() >= threshold) {
      if (BlsVerifier.verify(message, signature, publicKey)) {
        isDone = true;
      } else {
        removeBadSignatures();
        return check();
      }
    }
    return isDone;
  }

  private byte[] getSignersAsBitVector(List<BlockchainAddress> signers) {
    byte[] bits = new byte[signers.size() / 8 + 1];
    for (BlockchainAddress goodSigner : goodSignatures.keySet()) {
      int index = signers.indexOf(goodSigner);
      bits[index / 8] = (byte) (bits[index / 8] | 1 << (index % 8));
    }
    return bits;
  }

  static BlsSignature aggregateSignatures(List<BlsSignature> signatures) {
    return signatures.stream()
        .reduce(null, (left, right) -> left == null ? right : left.addSignature(right));
  }

  static BlsPublicKey aggregatePublicKeys(List<BlsPublicKey> signatures) {
    return signatures.stream()
        .reduce(null, (left, right) -> left == null ? right : left.addPublicKey(right));
  }

  BlsSignature removeBadSignatures(
      List<T> signatures, List<BlsPublicKey> keys, List<T> badSignatures) {

    int start = 0;
    int end = signatures.size();
    int mid = end / 2;

    if (start == mid) {
      // if we only have a single signature, then it's guaranteed to be bad since this method is
      // only called if the aggregated signature fails to verify.
      badSignatures.add(signatures.get(start));
      return null;
    }

    // at least 2 signatures to check
    List<T> leftPart = signatures.subList(start, mid);
    BlsSignature left =
        aggregateSignatures(
            leftPart.stream().map(HasBlsSignature::get).collect(Collectors.toList()));
    List<BlsPublicKey> leftPartKeys = keys.subList(start, mid);
    BlsPublicKey leftKey = aggregatePublicKeys(leftPartKeys);
    boolean leftGood = BlsVerifier.verify(message, left, leftKey);

    List<T> rightPart = signatures.subList(mid, end);
    BlsSignature right =
        aggregateSignatures(
            rightPart.stream().map(HasBlsSignature::get).collect(Collectors.toList()));
    List<BlsPublicKey> rightPartKeys = keys.subList(mid, end);
    BlsPublicKey rightKey = aggregatePublicKeys(rightPartKeys);
    boolean rightGood = BlsVerifier.verify(message, right, rightKey);

    if (!leftGood) {
      left = removeBadSignatures(leftPart, leftPartKeys, badSignatures);
    }
    if (!rightGood) {
      right = removeBadSignatures(rightPart, rightPartKeys, badSignatures);
    }

    // left and right are guaranteed to be either null or good, so we return accordingly
    if (left == null) {
      return right;
    } else {
      if (right == null) {
        return left;
      } else {
        return left.addSignature(right);
      }
    }
  }

  void removeBadSignatures() {
    List<T> signatures = new ArrayList<>();
    List<BlsPublicKey> publicKeys = new ArrayList<>();
    for (Map.Entry<BlockchainAddress, T> entry : goodSignatures.entrySet()) {
      signatures.add(entry.getValue());
      BlockchainAddress key = entry.getKey();
      publicKeys.add(signers.get(key));
    }
    List<T> invalidSignatures = new ArrayList<>();
    BlsSignature good = removeBadSignatures(signatures, publicKeys, invalidSignatures);
    // Find the authors of the bad signatures and put them on the naughty list.
    for (T badSignature : invalidSignatures) {
      BlockchainAddress baddie = signatureAuthors.get(badSignature.get());
      goodSignatures.remove(baddie);
      badSignatures.put(baddie, badSignature);
    }
    this.signature = good;
    this.publicKey = null;
    for (Map.Entry<BlockchainAddress, BlsPublicKey> signer : signers.entrySet()) {
      if (goodSignatures.containsKey(signer.getKey())) {
        this.publicKey = addPublicKey(this.publicKey, signer.getValue());
      }
    }
  }

  private static BlsPublicKey addPublicKey(BlsPublicKey left, BlsPublicKey right) {
    if (left == null) {
      return right;
    } else {
      return left.addPublicKey(right);
    }
  }

  static final class SignatureAndBitVector {
    public final byte[] bitVector;
    public final BlsSignature signature;

    SignatureAndBitVector(byte[] bitVector, BlsSignature signature) {
      this.bitVector = bitVector;
      this.signature = signature;
    }
  }

  interface HasBlsSignature {
    BlsSignature get();
  }
}
