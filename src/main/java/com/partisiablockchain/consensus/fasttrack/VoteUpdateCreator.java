package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.VoteResult;
import com.secata.stream.DataStreamSerializable;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

interface VoteUpdateCreator {

  static VoteUpdateCreator createDefault(DeterministicRandomBitGenerator randomGenerator) {
    final BooleanSupplier random =
        () -> {
          byte[] randomByte = new byte[1];
          randomGenerator.nextBytes(randomByte);
          return (randomByte[0] & 0b0000001) == 0;
        };
    return new VoteUpdateCreator() {
      @Override
      public Protocol<VoteResult> createDetecting(
          PartyConfiguration partyConfiguration,
          byte[] protocolId,
          Consumer<DataStreamSerializable> flood,
          boolean vote) {
        return VoteUpdatingProtocol.createDetecting(partyConfiguration, protocolId, flood, vote);
      }

      @Override
      public Protocol<VoteResult> createStabilizing(
          PartyConfiguration partyConfiguration,
          byte[] protocolId,
          Consumer<DataStreamSerializable> flood,
          boolean vote) {
        return VoteUpdatingProtocol.createStabilizing(
            partyConfiguration, protocolId, flood, random, vote);
      }
    };
  }

  Protocol<VoteResult> createDetecting(
      PartyConfiguration partyConfiguration,
      byte[] protocolId,
      Consumer<DataStreamSerializable> flood,
      boolean vote);

  Protocol<VoteResult> createStabilizing(
      PartyConfiguration partyConfiguration,
      byte[] protocolId,
      Consumer<DataStreamSerializable> flood,
      boolean vote);
}
