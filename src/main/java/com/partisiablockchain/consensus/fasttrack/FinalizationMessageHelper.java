package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;

/** Singleton class for creating the message that is signed as part of finalization. */
final class FinalizationMessageHelper {

  public static String FINALIZATION_DOMAIN_SEPARATOR = "FastTrack";

  private FinalizationMessageHelper() {}

  static Hash createMessage(Hash blockIdentifier, BlsSignature blsSignature) {
    return Hash.create(
        s -> {
          blockIdentifier.write(s);
          blsSignature.write(s);
          s.writeString(FINALIZATION_DOMAIN_SEPARATOR);
        });
  }
}
