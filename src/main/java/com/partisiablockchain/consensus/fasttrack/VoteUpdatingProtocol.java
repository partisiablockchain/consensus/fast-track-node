package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.VoteResult;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class VoteUpdatingProtocol implements Protocol<VoteResult> {

  private static final Logger logger = LoggerFactory.getLogger(VoteUpdatingProtocol.class);

  private final BroadcastCreator broadcastCreator;
  private final PartyConfiguration partyConfiguration;
  private final byte[] protocolId;
  private final Consumer<DataStreamSerializable> flood;
  private final Function<List<Boolean>, Boolean> softVote;
  private Stage stage = Stage.COLLECTING_VOTES;
  private final Map<BlockchainAddress, Vote> votes = new HashMap<>();
  private final Map<Hash, Protocol<byte[]>> justifiedBroadcast = new HashMap<>();
  private final List<Boolean> justifiedVotes = new ArrayList<>();
  private VoteResult result;

  private VoteUpdatingProtocol(
      PartyConfiguration partyConfiguration,
      byte[] protocolId,
      Consumer<DataStreamSerializable> flood,
      Function<List<Boolean>, Boolean> softVote,
      boolean myVote,
      BroadcastCreator broadcastCreator) {
    this.partyConfiguration = partyConfiguration;
    this.protocolId = protocolId;
    this.flood = flood;
    this.softVote = softVote;
    this.broadcastCreator = broadcastCreator;
    storeAndFloodVote(
        partyConfiguration.getMyAddress(),
        new Vote(myVote, partyConfiguration.sign(createHashForVote(protocolId, myVote))));
  }

  static VoteUpdatingProtocol createStabilizing(
      PartyConfiguration partyConfiguration,
      byte[] protocolId,
      Consumer<DataStreamSerializable> flood,
      BooleanSupplier randomBitSupplier,
      boolean vote) {
    return new VoteUpdatingProtocol(
        partyConfiguration,
        protocolId,
        flood,
        booleans -> randomBitSupplier.getAsBoolean(),
        vote,
        defaultBroadcast());
  }

  static VoteUpdatingProtocol createDetecting(
      PartyConfiguration partyConfiguration,
      byte[] protocolId,
      Consumer<DataStreamSerializable> flood,
      boolean vote) {
    return createDetectingInternal(partyConfiguration, protocolId, flood, vote, defaultBroadcast());
  }

  static VoteUpdatingProtocol createDetectingInternal(
      PartyConfiguration partyConfiguration,
      byte[] protocolId,
      Consumer<DataStreamSerializable> flood,
      boolean vote,
      BroadcastCreator broadcastCreator) {
    return new VoteUpdatingProtocol(
        partyConfiguration,
        protocolId,
        flood,
        booleans -> {
          long trueVotes = booleans.stream().filter(Boolean::booleanValue).count();
          return trueVotes > partyConfiguration.maliciousCount();
        },
        vote,
        broadcastCreator);
  }

  private static BroadcastCreator defaultBroadcast() {
    return new BroadcastCreator() {
      @Override
      public Protocol<byte[]> sendBroadcast(
          byte[] broadcastId,
          PartyConfiguration partyConfiguration,
          Consumer<DataStreamSerializable> flood,
          byte[] broadcastValue) {
        return AsyncBroadcastFromSignatures.sendBroadcast(
            broadcastId, partyConfiguration, flood, broadcastValue);
      }

      @Override
      public Protocol<byte[]> receiveBroadcast(
          byte[] broadcastId,
          BlockchainAddress broadcaster,
          PartyConfiguration partyConfiguration,
          Consumer<DataStreamSerializable> flood) {
        return AsyncBroadcastFromSignatures.receiveBroadcast(
            broadcastId, broadcaster, partyConfiguration, flood);
      }
    };
  }

  static Hash createHashForVote(byte[] protocolId, boolean vote) {
    return Hash.create(
        stream -> {
          stream.writeString("VOTE");
          stream.write(protocolId);
          stream.writeBoolean(vote);
        });
  }

  @Override
  public void incoming(SafeDataInputStream message) {
    if (isDone()) {
      return;
    }
    MessageType messageType = message.readEnum(MessageType.values());
    if (collectingSignatures() && messageType == MessageType.VOTE) {
      Vote vote = Vote.read(message);
      BlockchainAddress voter = vote.voter(protocolId);
      if (partyConfiguration.isParticipating(voter) && !votes.containsKey(voter)) {
        storeAndFloodVote(voter, vote);
        checkSignatureCount();
      }
    }
    if (collectingJustifiedVotes() && messageType == MessageType.JUSTIFIED_VOTE) {
      Hash broadcastId = Hash.read(message);
      if (justifiedBroadcast.containsKey(broadcastId)) {
        Protocol<byte[]> broadcast = justifiedBroadcast.get(broadcastId);
        broadcast.incoming(message);
        if (broadcast.isDone()) {
          justifiedBroadcast.remove(broadcastId);
          handleJustifiedVote(broadcast);
        }
      }
    }
  }

  private void handleJustifiedVote(Protocol<byte[]> broadcast) {
    SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(broadcast.getResult());
    JustifiedVote read = JustifiedVote.read(partyConfiguration, fromBytes);
    if (read.isValid(partyConfiguration, protocolId)) {
      justifiedVotes.add(read.vote);
      checkJustifiedCount();
    } else {
      logger.warn("Received invalid justified vote from {}", broadcast);
    }
  }

  private void checkJustifiedCount() {
    if (hasEnoughVotes(justifiedVotes)) {
      if (allVotesAgree(justifiedVotes)) {
        this.result = new VoteResult(true, justifiedVotes.get(0));
      } else {
        this.result = new VoteResult(false, softVote.apply(justifiedVotes));
      }
      this.stage = Stage.DONE;
    }
  }

  private boolean hasEnoughVotes(Collection<?> votes) {
    return votes.size() == partyConfiguration.honestCount();
  }

  private boolean allVotesAgree(List<Boolean> votes) {
    return votes.stream().distinct().count() == 1;
  }

  private void storeAndFloodVote(BlockchainAddress voter, Vote vote) {
    this.votes.put(voter, vote);
    flood.accept(
        stream -> {
          stream.writeEnum(MessageType.VOTE);
          vote.write(stream);
        });
  }

  private void checkSignatureCount() {
    if (hasEnoughVotes(votes.values())) {
      for (BlockchainAddress participant : partyConfiguration.getParticipants()) {
        Hash broadcastId = createBroadcastId(protocolId, participant);
        Protocol<byte[]> broadcast;
        if (partyConfiguration.getMyAddress().equals(participant)) {
          broadcast =
              broadcastCreator.sendBroadcast(
                  broadcastId.getBytes(),
                  partyConfiguration,
                  createBroadcastFlood(broadcastId),
                  createJustifiedVote());
        } else {
          broadcast =
              broadcastCreator.receiveBroadcast(
                  broadcastId.getBytes(),
                  participant,
                  partyConfiguration,
                  createBroadcastFlood(broadcastId));
        }
        justifiedBroadcast.put(broadcastId, broadcast);
      }
      stage = Stage.COLLECTING_JUSTIFIED_VOTES;
    }
  }

  static Hash createBroadcastId(byte[] protocolId, BlockchainAddress participant) {
    return Hash.create(
        stream -> {
          stream.writeString("JUSTIFIED_VOTE");
          stream.write(protocolId);
          participant.write(stream);
        });
  }

  private byte[] createJustifiedVote() {
    List<Vote> trueVotes =
        votes.values().stream().filter(Vote::getVote).collect(Collectors.toList());
    List<Vote> falseVotes =
        votes.values().stream().filter(Predicate.not(Vote::getVote)).collect(Collectors.toList());
    boolean vote = trueVotes.size() > partyConfiguration.maliciousCount();
    List<Vote> votes = vote ? trueVotes : falseVotes;
    JustifiedVote justifiedVote =
        new JustifiedVote(
            vote,
            votes.stream()
                .map(Vote::getSignature)
                .limit(partyConfiguration.honestCount())
                .collect(Collectors.toList()));
    return SafeDataOutputStream.serialize(justifiedVote::write);
  }

  private Consumer<DataStreamSerializable> createBroadcastFlood(Hash broadcastId) {
    return serializable ->
        flood.accept(
            stream -> {
              stream.writeEnum(MessageType.JUSTIFIED_VOTE);
              broadcastId.write(stream);
              serializable.write(stream);
            });
  }

  @Override
  public boolean isDone() {
    return stage == Stage.DONE;
  }

  @Override
  public VoteResult getResult() {
    return result;
  }

  boolean collectingJustifiedVotes() {
    return stage == Stage.COLLECTING_JUSTIFIED_VOTES;
  }

  boolean collectingSignatures() {
    return stage == Stage.COLLECTING_VOTES;
  }

  private enum Stage {
    COLLECTING_VOTES,
    COLLECTING_JUSTIFIED_VOTES,
    DONE
  }

  enum MessageType {
    VOTE,
    JUSTIFIED_VOTE,
  }

  static final class Vote {

    private final boolean vote;
    private final Signature signature;

    Vote(boolean vote, Signature signature) {
      this.vote = vote;
      this.signature = signature;
    }

    static Vote read(SafeDataInputStream stream) {
      boolean vote = stream.readBoolean();
      Signature signature = Signature.read(stream);
      return new Vote(vote, signature);
    }

    void write(SafeDataOutputStream stream) {
      stream.writeBoolean(vote);
      signature.write(stream);
    }

    boolean getVote() {
      return vote;
    }

    Signature getSignature() {
      return signature;
    }

    BlockchainAddress voter(byte[] protocolId) {
      return signature.recoverSender(createHashForVote(protocolId, vote));
    }
  }

  static final class JustifiedVote {

    private final boolean vote;
    private final List<Signature> signatures;

    JustifiedVote(boolean vote, List<Signature> signatures) {
      this.vote = vote;
      this.signatures = signatures;
    }

    static JustifiedVote read(PartyConfiguration configuration, SafeDataInputStream stream) {
      boolean vote = stream.readBoolean();
      List<Signature> signatures =
          FastTrackProducer.SIGNATURE_LIST.readFixed(stream, configuration.maliciousCount());
      return new JustifiedVote(vote, signatures);
    }

    void write(SafeDataOutputStream stream) {
      stream.writeBoolean(vote);
      FastTrackProducer.SIGNATURE_LIST.writeFixed(stream, signatures);
    }

    boolean isValid(PartyConfiguration configuration, byte[] protocolId) {
      Hash hashForVote = createHashForVote(protocolId, vote);
      long validSignatures =
          signatures.stream()
              .map(signature -> signature.recoverSender(hashForVote))
              .filter(Objects::nonNull)
              .distinct()
              .filter(configuration::isParticipating)
              .count();
      return validSignatures == signatures.size();
    }
  }

  static final class VoteResult {

    private final boolean isHardVote;
    private final boolean vote;

    VoteResult(boolean isHardVote, boolean vote) {
      this.isHardVote = isHardVote;
      this.vote = vote;
    }

    boolean isHardVote() {
      return isHardVote;
    }

    boolean getVote() {
      return vote;
    }
  }

  interface BroadcastCreator {

    Protocol<byte[]> sendBroadcast(
        byte[] broadcastId,
        PartyConfiguration partyConfiguration,
        Consumer<DataStreamSerializable> flood,
        byte[] broadcastValue);

    Protocol<byte[]> receiveBroadcast(
        byte[] broadcastId,
        BlockchainAddress broadcaster,
        PartyConfiguration partyConfiguration,
        Consumer<DataStreamSerializable> flood);
  }
}
