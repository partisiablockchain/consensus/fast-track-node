package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.storage.RootDirectory;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

final class StorageFiles {

  static final String FILE_PREFIX = "PendingFastTrack-";
  private final RootDirectory dataDirectory;
  private int nextId = 0;

  StorageFiles(RootDirectory dataDirectory) {
    this.dataDirectory = dataDirectory;
    while (fileForId(nextId).exists()) {
      nextId++;
    }
  }

  List<RandomAccessFile> getExisting() {
    return IntStream.range(0, nextId).mapToObj(this::create).collect(Collectors.toList());
  }

  RandomAccessFile createNext() {
    RandomAccessFile safe = create(nextId);
    nextId++;
    return safe;
  }

  private RandomAccessFile create(int nextId) {
    File file = fileForId(nextId);
    return ExceptionConverter.call(
        () -> new RandomAccessFile(file, "rw"), "Unable to open pending file ");
  }

  private File fileForId(int nextId) {
    String fileName = FILE_PREFIX + nextId;
    return dataDirectory.createFile(fileName);
  }
}
