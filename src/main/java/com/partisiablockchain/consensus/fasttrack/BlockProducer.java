package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockAndStateWithParent;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.LedgerUtil;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.storage.BlockTimeStorage;
import com.secata.tools.coverage.WithCloseableResources;
import com.secata.tools.thread.ThreadedLoop;
import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Block producer listens to the blockchain and if it is my time to produce creates a new block. */
public final class BlockProducer implements Listener, WithCloseableResources {

  private static final Logger logger = LoggerFactory.getLogger(BlockProducer.class);

  static final int MAX_TRANSACTIONS = 0xFF;
  private final BlockTimeStorage latestProductionTime;
  private final BlockchainLedger blockchain;
  private final ThreadedLoop loop;
  private final BlockchainPublicKey myAddress;
  private final ShouldProduce shouldProduce;
  private final ToLongFunction<BlockAndStateWithParent> maximumWait;
  private final Consumer<Block> producedCallback;
  private final long heartBeatTimeout;

  /**
   * Construct a new block producer.
   *
   * @param blockchain the blockchain to attach to
   * @param myAddress address of the block producer
   * @param dataFile a file to store latest produced block
   * @param shouldProduce predicate to indicate if the produce should produce for this parent
   * @param maximumWait the maximum time to wait when producing for the supplied parent
   * @param producedCallback callback to invoke when a block has been produced
   * @param heartBeatTimeout the heart beat timeout used by the producer
   */
  BlockProducer(
      BlockchainLedger blockchain,
      BlockchainPublicKey myAddress,
      File dataFile,
      ShouldProduce shouldProduce,
      ToLongFunction<BlockAndStateWithParent> maximumWait,
      Consumer<Block> producedCallback,
      long heartBeatTimeout) {
    this.myAddress = myAddress;
    this.shouldProduce = shouldProduce;
    this.maximumWait = maximumWait;
    this.producedCallback = producedCallback;
    this.latestProductionTime = new BlockTimeStorage(dataFile, blockchain.getBlockTime());
    this.blockchain = blockchain;
    logger.info(
        "Block producer initiated. Latest produced block is={}",
        latestProductionTime.getLatestBlockTime());
    String shard = getNullSafeShard(blockchain.getChainState().getExecutedState().getSubChainId());
    loop = ThreadedLoop.create(this::produceBlock, "FastTrackProducer-" + shard).start();
    blockchain.attach(this);
    this.heartBeatTimeout = heartBeatTimeout;
  }

  synchronized void produceBlock() throws InterruptedException {
    for (BlockAndStateWithParent blockAndState : blocksToConsider()) {
      Block block = blockAndState.currentBlock();
      ImmutableChainState latestState = blockAndState.currentState();
      boolean allowedToProduce = allowedToProduce(block);
      if (allowedToProduce && shouldProduce.test(block, new ConsensusState(latestState))) {
        long timeout = maximumWait.applyAsLong(blockAndState);
        produceBlock(block, latestState, timeout);
      }
    }
    this.wait(heartBeatTimeout);
  }

  private void produceBlock(Block parent, ImmutableChainState latestState, long maximumWait)
      throws InterruptedException {
    long timeout = System.currentTimeMillis() + maximumWait;
    ProductionData productionData = waitForTransactions(timeout, latestState);
    List<Hash> includedTransactions =
        productionData.transactions.stream()
            .filter(transactionsDistinctByAccount())
            .limit(MAX_TRANSACTIONS)
            .map(SignedTransaction::identifier)
            .collect(Collectors.toList());

    List<Hash> includedEvents =
        productionData.events.stream()
            .limit(MAX_TRANSACTIONS)
            .map(ExecutableEvent::identifier)
            .collect(Collectors.toList());

    produceStoreSend(
        parent, latestState, productionData.productionTime, includedEvents, includedTransactions);
  }

  private void produceStoreSend(
      Block parent,
      ImmutableChainState latestState,
      long productionTime,
      List<Hash> includedEvents,
      List<Hash> includedTransactions) {
    long nextBlockTime = parent.getBlockTime() + 1;

    ConsensusState consensusState = new ConsensusState(latestState);
    short producerIndex = getIndexOfSelfInCommittee(consensusState);
    long activeCommittee = determineCommitteeId(consensusState);
    Block producedBlock =
        new Block(
            productionTime,
            nextBlockTime,
            activeCommittee,
            parent.identifier(),
            latestState.getHash(),
            includedEvents,
            includedTransactions,
            producerIndex);

    latestProductionTime.storeLatestBlockTime(nextBlockTime);
    producedCallback.accept(producedBlock);
  }

  long determineCommitteeId(ConsensusState latestState) {
    if (latestState.isFastTrack()) {
      return latestState.getActiveCommittee();
    } else {
      return -1;
    }
  }

  private short getIndexOfSelfInCommittee(ConsensusState latestState) {
    short producerIndex = -1;
    if (latestState.isFastTrack()) {
      List<BlockchainPublicKey> committee = latestState.getCommittee();
      producerIndex = (short) committee.indexOf(myAddress);
    }
    return producerIndex;
  }

  boolean allowedToProduce(Block block) {
    return block.getBlockTime() + 1 > latestProductionTime.getLatestBlockTime();
  }

  private synchronized ProductionData waitForTransactions(
      long timeout, ImmutableChainState chainState) throws InterruptedException {
    return waitForTransactions(timeout, () -> getProductionTimeAndTransactions(chainState));
  }

  synchronized ProductionData waitForTransactions(long timeout, Supplier<ProductionData> supplier)
      throws InterruptedException {
    ProductionData productionData = supplier.get();
    long nextWait = timeout - System.currentTimeMillis();
    while (nextWait > 0 && !productionData.hasTransaction()) {
      this.wait(nextWait);
      productionData = supplier.get();
      nextWait = timeout - System.currentTimeMillis();
    }
    return productionData;
  }

  private ProductionData getProductionTimeAndTransactions(ImmutableChainState chainState) {
    long productionTime = System.currentTimeMillis();
    List<SignedTransaction> transactions =
        getValidTransactions(blockchain, productionTime, chainState);
    List<ExecutableEvent> events =
        LedgerUtil.selectValidEvents(
            chainState.isSyncing(),
            chainState.getShardNonces(),
            blockchain.getPendingEvents().values(),
            chainState.getGovernanceVersion());

    return new ProductionData(productionTime, transactions, events);
  }

  static List<SignedTransaction> getValidTransactions(
      BlockchainLedger blockchain, long productionTime, ImmutableChainState state) {
    if (state.isSyncing()) {
      return List.of();
    } else {
      return blockchain.getPendingTransactions().stream()
          .filter(isValid(state, productionTime))
          .collect(Collectors.toList());
    }
  }

  private List<BlockAndStateWithParent> blocksToConsider() {
    return blockchain.getPossibleHeads();
  }

  private static Predicate<SignedTransaction> isValid(
      ImmutableChainState state, long productionTime) {
    return t -> state.isTransactionValidInState(t, productionTime);
  }

  private Predicate<SignedTransaction> transactionsDistinctByAccount() {
    Set<BlockchainAddress> appliedForAccount = new HashSet<>();
    return t -> appliedForAccount.add(t.getSender());
  }

  @Override
  public synchronized void newFinalBlock(Block block, ImmutableChainState state) {
    this.notifyAll();
  }

  @Override
  public synchronized void newPendingTransaction(SignedTransaction transaction) {
    this.notifyAll();
  }

  @Override
  public synchronized void newPendingEvent(ExecutableEvent event) {
    this.notifyAll();
  }

  @Override
  public synchronized void newBlockProposal(Block block) {
    this.notifyAll();
  }

  BlockTimeStorage getLatestProductionTime() {
    return latestProductionTime;
  }

  boolean isRunning() {
    return loop.isRunning();
  }

  @Override
  public Stream<AutoCloseable> resources() {
    return Stream.of(latestProductionTime, loop::stop);
  }

  static String getNullSafeShard(String shard) {
    return shard == null ? "Gov" : shard;
  }

  static final class ProductionData {

    private final long productionTime;
    private final List<SignedTransaction> transactions;
    private final List<ExecutableEvent> events;

    ProductionData(
        long productionTime, List<SignedTransaction> transactions, List<ExecutableEvent> events) {
      this.productionTime = productionTime;
      this.transactions = transactions;
      this.events = events;
    }

    boolean hasTransaction() {
      return !transactions.isEmpty() || !events.isEmpty();
    }
  }
}
