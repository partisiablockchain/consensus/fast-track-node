package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

final class TimeCapMessage {

  private final boolean vote;
  private final Signature signature;

  TimeCapMessage(boolean vote, Signature signature) {
    this.vote = vote;
    this.signature = signature;
  }

  void write(SafeDataOutputStream stream) {
    stream.writeBoolean(vote);
    signature.write(stream);
  }

  static TimeCapMessage read(SafeDataInputStream stream) {
    boolean vote = stream.readBoolean();
    Signature signature = Signature.read(stream);
    return new TimeCapMessage(vote, signature);
  }

  boolean vote() {
    return vote;
  }

  Signature signature() {
    return signature;
  }
}
