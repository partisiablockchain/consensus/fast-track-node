package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

final class DeterministicRandomBitGenerator {

  private static final int BLOCK_SIZE = 16;
  private static final long RESEED_LIMIT = 1L << 48;

  private final long reseedLimit;
  private final Cipher cipher;
  private byte[] counter;
  private long numberOfUpdates = 0;

  DeterministicRandomBitGenerator(byte[] entropy) {
    this(entropy, RESEED_LIMIT);
  }

  private DeterministicRandomBitGenerator(byte[] entropy, long reseedLimit) {
    this.reseedLimit = reseedLimit;
    this.counter = new byte[BLOCK_SIZE];
    this.cipher =
        ExceptionConverter.call(() -> Cipher.getInstance("AES/ECB/NoPadding"), "Missing AES");
    updateKey(new byte[BLOCK_SIZE]);
    update(entropy);
  }

  private void updateKey(byte[] key) {
    ExceptionConverter.run(
        () -> cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES")),
        "Unable to initialize AES");
  }

  static DeterministicRandomBitGenerator createForTest(byte[] entropy) {
    return new DeterministicRandomBitGenerator(entropy, 2L);
  }

  void nextBytes(byte[] result) {
    nextBytes(result, 0, result.length);
  }

  void nextBytes(byte[] result, int offset, int length) {
    int fullBlocks = length / BLOCK_SIZE;
    for (int i = 0; i < fullBlocks; i++) {
      nextBlock(result, offset + i * BLOCK_SIZE);
    }
    int remaining = length - fullBlocks * BLOCK_SIZE;
    if (remaining >= 1) {
      byte[] bytes = new byte[BLOCK_SIZE];
      nextBlock(bytes, 0);
      System.arraycopy(bytes, 0, result, offset + fullBlocks * BLOCK_SIZE, remaining);
    }
    update(null);
  }

  private void nextBlock(byte[] result, int offset) {
    applyCipher(result, offset);
    incrementCounter();
  }

  private void update(byte[] entropy) {
    checkReseedLimit(numberOfUpdates);

    byte[] key = new byte[BLOCK_SIZE];
    applyCipher(key, 0);
    incrementCounter();
    applyCipher(this.counter, 0);

    if (entropy != null) {
      for (int i = 0; i < key.length; i++) {
        key[i] ^= entropy[i];
      }
      for (int i = 0; i < counter.length; i++) {
        counter[i] ^= entropy[i + key.length];
      }
    }
    updateKey(key);
    numberOfUpdates++;
  }

  private void applyCipher(byte[] result, int offset) {
    ExceptionConverter.run(
        () -> cipher.doFinal(counter, 0, counter.length, result, offset), "Unable to apply AES");
  }

  private void checkReseedLimit(long numberOfUpdates) {
    if (numberOfUpdates >= reseedLimit) {
      throw new RuntimeException("Ran out of AES values - should not happen");
    }
  }

  private void incrementCounter() {
    for (int i = 0; i < counter.length; i++) {
      if (counter[i] != -1) {
        counter[i]++;
        return;
      } else {
        counter[i] = 0;
      }
    }
  }
}
