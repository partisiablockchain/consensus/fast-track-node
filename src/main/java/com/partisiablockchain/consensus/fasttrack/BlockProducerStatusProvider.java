package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.providers.AdditionalStatusProvider;
import java.util.Map;

/** Additional information about the status of the block producer. */
final class BlockProducerStatusProvider implements AdditionalStatusProvider {

  private final String subChainId;
  private final AdditionalBlockProducerStatus blockProducerStatus;

  public BlockProducerStatusProvider(
      String subChainId, AdditionalBlockProducerStatus blockProducerStatus) {
    this.subChainId = subChainId;
    this.blockProducerStatus = blockProducerStatus;
  }

  @Override
  public String getName() {
    String shard = subChainId == null ? "Gov" : subChainId;
    return shard + "-" + getClass().getSimpleName();
  }

  @Override
  public Map<String, String> getStatus() {
    return Map.of(
        "latestProducedBlockTime", Long.toString(blockProducerStatus.getLatestProducedBlockTime()));
  }
}
