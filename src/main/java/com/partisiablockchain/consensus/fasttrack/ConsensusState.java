package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.List;

final class ConsensusState implements CommitteeInfoGetter {

  private static final String GLOBAL_TYPE =
      "com.partisiablockchain.consensus.fasttrack.FastTrackGlobal";

  final StateSerializable global;
  final StateSerializable local;

  ConsensusState(StateSerializable global, StateSerializable local) {
    this.global = global;
    this.local = local;
  }

  ConsensusState(ImmutableChainState state) {
    this(
        state.getGlobalPluginState(ChainPluginType.CONSENSUS),
        state.getLocalConsensusPluginState());
  }

  boolean isFastTrack() {
    return global != null && global.getClass().getName().equals(GLOBAL_TYPE);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BlockchainPublicKey> getCommittee() {
    return safeInvokeWithLongParameter(
        global, "getCommitteeKeys", List.class, getActiveCommittee());
  }

  @Override
  public int getActiveCommittee() {
    return safeInvoke(local, "getActiveCommittee", Integer.TYPE);
  }

  Hash getCommitteeId() {
    return safeInvokeWithLongParameter(global, "getCommitteeId", Hash.class, getActiveCommittee());
  }

  int getNewestCommitteeId() {
    return safeInvoke(global, "getNewestCommitteeId", int.class);
  }

  @SuppressWarnings("unchecked")
  List<BlsPublicKey> getCommitteeBlsKeys() {
    return safeInvokeWithLongParameter(
        global, "getCommitteeBlsKeys", List.class, getActiveCommittee());
  }

  int getResetBlocksSeen() {
    return safeInvoke(local, "getResetBlocksSeen", int.class);
  }

  boolean hasNewCommittee() {
    int newestCommittee = getNewestCommitteeId();
    int activeCommittee = getActiveCommittee();
    return newestCommittee > activeCommittee;
  }

  @SuppressWarnings("unchecked")
  static <T> T safeInvokeWithLongParameter(
      StateSerializable state, String name, Class<T> returnType, long parameter) {
    try {
      MethodHandle handle =
          MethodHandles.lookup()
              .findVirtual(state.getClass(), name, MethodType.methodType(returnType, Long.TYPE));

      return (T) handle.invokeWithArguments(state, parameter);
    } catch (Throwable throwable) {
      throw new RuntimeException(throwable);
    }
  }

  @SuppressWarnings("unchecked")
  static <T> T safeInvoke(StateSerializable state, String name, Class<T> returnType) {
    try {
      MethodHandle handle =
          MethodHandles.publicLookup()
              .findVirtual(state.getClass(), name, MethodType.methodType(returnType));
      return (T) handle.invoke(state);
    } catch (Throwable throwable) {
      throw new RuntimeException(throwable);
    }
  }
}
