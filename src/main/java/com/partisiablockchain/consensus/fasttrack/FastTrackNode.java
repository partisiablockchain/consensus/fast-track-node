package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.server.ServerModule;
import com.partisiablockchain.storage.RootDirectory;
import java.math.BigInteger;

/** Node responsible for running the fast track block producer. */
public final class FastTrackNode implements ServerModule<FastTrackProducerConfigDto> {

  @Override
  public void register(
      RootDirectory rootDirectory,
      BlockchainLedger blockchainLedger,
      ServerConfig serverConfig,
      FastTrackProducerConfigDto configDto) {
    register(rootDirectory, blockchainLedger, serverConfig, configDto, null);
  }

  /**
   * Register a FastTrackNode onto the given blockchain.
   *
   * @param rootDirectory the root directory for storing files
   * @param blockchainLedger the ledger
   * @param serverConfig the server configuration
   * @param configDto the node configuration
   * @param sender the transaction sender to use for rewards callback
   */
  public static void register(
      RootDirectory rootDirectory,
      BlockchainLedger blockchainLedger,
      ServerModule.ServerConfig serverConfig,
      FastTrackProducerConfigDto configDto,
      TransactionSender sender) {
    RootDirectory fastTrackDirectory = rootDirectory.createSub("fastTrack");
    KeyPair bakerKeyPair = new KeyPair(new BigInteger(configDto.producerKey, 16));
    BlsKeyPair bakerBlsKeyPair = new BlsKeyPair(new BigInteger(configDto.producerBlsKey, 16));
    FastTrackProducer producer =
        new FastTrackProducer(
            fastTrackDirectory,
            blockchainLedger,
            bakerKeyPair,
            bakerBlsKeyPair,
            configDto.timeout,
            configDto.networkDelay,
            sender,
            configDto.registerProvider);

    serverConfig.registerCloseable(producer);
  }

  @Override
  public Class<FastTrackProducerConfigDto> configType() {
    return FastTrackProducerConfigDto.class;
  }
}
