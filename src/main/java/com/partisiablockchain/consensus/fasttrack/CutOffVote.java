package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Consumer;

/** Implements the cut off vote protocol. */
public final class CutOffVote implements Protocol<Long> {

  private final PartyConfiguration partyConfiguration;
  private final Consumer<DataStreamSerializable> flood;
  private final byte[] protocolId;
  private final LinkedHashMap<Hash, Protocol<Boolean>> agreements;
  private final long maximalBlockTime;

  private Long result;

  private CutOffVote(
      PartyConfiguration partyConfiguration,
      Consumer<DataStreamSerializable> flood,
      AgreementCreator agreementCreator,
      byte[] protocolId,
      long maximalBlockTime,
      DeterministicRandomBitGenerator generator) {
    this.partyConfiguration = partyConfiguration;
    this.flood = flood;
    this.protocolId = protocolId;
    this.maximalBlockTime = maximalBlockTime;
    this.agreements = initialize(agreementCreator, maximalBlockTime, generator);
  }

  static CutOffVote create(
      PartyConfiguration partyConfiguration,
      Consumer<DataStreamSerializable> flood,
      byte[] protocolId,
      long maximalBlockTime,
      DeterministicRandomBitGenerator generator) {
    return createInternal(
        partyConfiguration,
        flood,
        AsyncByzantineAgreement::create,
        protocolId,
        maximalBlockTime,
        generator);
  }

  static CutOffVote createInternal(
      PartyConfiguration partyConfiguration,
      Consumer<DataStreamSerializable> flood,
      AgreementCreator agreementCreator,
      byte[] protocolId,
      long maximalBlockTime,
      DeterministicRandomBitGenerator generator) {
    return new CutOffVote(
        partyConfiguration, flood, agreementCreator, protocolId, maximalBlockTime, generator);
  }

  @SuppressWarnings("NonApiType")
  private LinkedHashMap<Hash, Protocol<Boolean>> initialize(
      AgreementCreator agreementCreator,
      long maximalBlockTime,
      DeterministicRandomBitGenerator generator) {
    long s = maximalBlockTime % 4;
    long p = (maximalBlockTime - 1) % 4;
    LinkedHashMap<Hash, Protocol<Boolean>> agreements = new LinkedHashMap<>();
    for (int i = 0; i < 4; i++) {
      byte[] nextSeed = new byte[32];
      generator.nextBytes(nextSeed);
      Hash protocolId = createProtocolId(i);
      agreements.put(
          protocolId,
          agreementCreator.create(
              new DeterministicRandomBitGenerator(nextSeed),
              partyConfiguration,
              createSubFlood(protocolId),
              protocolId.getBytes(),
              i == s || i == p));
    }
    return agreements;
  }

  private Consumer<DataStreamSerializable> createSubFlood(Hash protocolId) {
    return serializable ->
        flood.accept(
            stream -> {
              protocolId.write(stream);
              serializable.write(stream);
            });
  }

  Hash createProtocolId(int agreementIndex) {
    return Hash.create(
        stream -> {
          stream.writeString("ABA");
          stream.write(protocolId);
          stream.writeInt(agreementIndex);
        });
  }

  @Override
  public Long getResult() {
    return result;
  }

  @Override
  public boolean isDone() {
    return result != null;
  }

  @Override
  public void incoming(SafeDataInputStream message) {
    if (isDone()) {
      return;
    }
    Hash protocolId = Hash.read(message);
    Protocol<Boolean> agreement = agreements.get(protocolId);
    if (agreement != null) {
      agreement.incoming(message);
      if (allAreTerminated()) {
        result = determineResult();
      }
    }
  }

  private long determineResult() {
    List<Protocol<Boolean>> agreements = new ArrayList<>(this.agreements.values());
    long nextBlockTime;
    for (nextBlockTime = maximalBlockTime + 1;
        nextBlockTime >= maximalBlockTime - 1;
        nextBlockTime--) {
      if (agreements.get((int) (nextBlockTime % 4)).getResult()) {
        return nextBlockTime;
      }
    }
    if (agreements.get((int) (nextBlockTime % 4)).getResult()) {
      throw new RuntimeException(
          "Malicious behaviour!"
              + " This should not happen as long as out security model is adhered to."
              + " We are only able to do a single rollback.");
    } else {
      throw new RuntimeException("Malicious behaviour! No agreement about block times.");
    }
  }

  boolean allAreTerminated() {
    return agreements.values().stream().allMatch(Protocol::isTerminated);
  }

  interface AgreementCreator {

    Protocol<Boolean> create(
        DeterministicRandomBitGenerator randomGenerator,
        PartyConfiguration partyConfiguration,
        Consumer<DataStreamSerializable> flood,
        byte[] protocolId,
        boolean currentVote);
  }
}
