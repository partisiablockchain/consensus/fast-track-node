package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Provider of additional information about the status of blocks being produced. */
final class FastTrackStateStatusProvider implements AdditionalStatusProvider {

  /** Shard ID. */
  private final String subChainId;

  private final AdditionalFastTrackStateStatus fastTrackStateStatus;

  FastTrackStateStatusProvider(
      String subChainId, AdditionalFastTrackStateStatus fastTrackStateStatus) {
    this.subChainId = subChainId;
    this.fastTrackStateStatus = fastTrackStateStatus;
  }

  @Override
  public String getName() {
    String shard = subChainId == null ? "Gov" : subChainId;
    return shard + "-" + getClass().getSimpleName();
  }

  @Override
  public Map<String, String> getStatus() {
    Map<String, String> statuses = new HashMap<>();
    statuses.put(
        "latestSignedBlockTime", Long.toString(fastTrackStateStatus.latestSignedBlockTime()));
    List<BlockStateStatus> blockStates = fastTrackStateStatus.blockStates();
    for (int i = 0; i < blockStates.size(); i++) {
      String prefix = "blockState" + i;
      BlockStateStatus blockState = blockStates.get(i);
      statuses.put(prefix + "BlockIdentifier", blockState.blockIdentifier().toString());
      statuses.put(prefix + "BlockTime", Long.toString(blockState.blockTime()));
      statuses.put(prefix + "BlockProducerIndex", Long.toString(blockState.blockProducerIndex()));
      statuses.put(
          prefix + "GoodSigners",
          blockState.goodSigners().stream()
              .map(BlockchainAddress::writeAsString)
              .toList()
              .toString());
      statuses.put(
          prefix + "BadSigners",
          blockState.badSigners().stream()
              .map(BlockchainAddress::writeAsString)
              .toList()
              .toString());
    }
    return statuses;
  }
}
