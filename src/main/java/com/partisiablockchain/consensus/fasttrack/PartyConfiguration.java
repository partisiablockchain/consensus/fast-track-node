package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class PartyConfiguration {

  private final List<BlockchainAddress> parties;
  private final List<BlsPublicKey> publicKeys;
  private final KeyPair myKey;
  private final BlsKeyPair myBlsKey;
  private final BlockchainAddress myAddress;
  private final int maliciousCount;

  PartyConfiguration(
      List<BlockchainAddress> parties,
      List<BlsPublicKey> publicKeys,
      KeyPair myKey,
      BlsKeyPair myBlsKey) {
    int partiesMinusOne = parties.size() - 1;
    if (partiesMinusOne < 3) {
      throw new IllegalArgumentException("Party count must be n >= 3t+1");
    }
    this.maliciousCount = partiesMinusOne / 3;
    this.parties = List.copyOf(parties);
    this.publicKeys = List.copyOf(publicKeys);
    this.myKey = myKey;
    this.myBlsKey = myBlsKey;
    this.myAddress = myKey.getPublic().createAddress();
    if (!isParticipating(getMyAddress())) {
      throw new IllegalArgumentException("I am not participating");
    }
  }

  int honestCount() {
    return partyCount() - maliciousCount();
  }

  int maliciousCount() {
    return maliciousCount;
  }

  int partyCount() {
    return parties.size();
  }

  boolean isParticipating(BlockchainAddress signer) {
    return parties.contains(signer);
  }

  BlockchainAddress getMyAddress() {
    return myAddress;
  }

  Signature sign(Hash message) {
    return myKey.sign(message);
  }

  BlsSignature blsSign(Hash message) {
    return myBlsKey.sign(message);
  }

  List<BlockchainAddress> getParticipants() {
    return List.copyOf(parties);
  }

  Map<BlockchainAddress, BlsPublicKey> getParties() {
    Map<BlockchainAddress, BlsPublicKey> map = new HashMap<>();
    for (int i = 0; i < parties.size(); i++) {
      map.put(parties.get(i), publicKeys.get(i));
    }
    return map;
  }
}
