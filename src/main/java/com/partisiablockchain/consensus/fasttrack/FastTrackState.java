package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.consensus.fasttrack.FastTrackProducer.FastTrackBlock;
import com.partisiablockchain.consensus.fasttrack.FastTrackProducer.FinalizationMessage;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.storage.BlockTimeStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.WithCloseableResources;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class FastTrackState implements WithCloseableResources {

  private static final Logger logger = LoggerFactory.getLogger(FastTrackState.class);

  private final KeyPair myKeyPair;
  private final BlsKeyPair myBlsKeyPair;
  private final BlockchainPublicKey myAccount;
  private final BlockTimeStorage latestSignedBlock;

  private final BooleanSupplier shutdownInProgress;
  private final BiConsumer<FinalizationMessage, Consumer<SafeDataOutputStream>> flood;
  private final Function<Hash, SignedTransaction> pendingTransactionLookup;
  private final Function<Hash, ExecutableEvent> pendingEventLookup;

  private final StorageFiles files;
  private final List<BlockStateStorage> blockStates = new ArrayList<>();
  private final Function<Hash, byte[]> rawStateAccess;

  FastTrackState(
      KeyPair myKeyPair,
      BlsKeyPair myBlsKeyPair,
      List<BlockAndState> possibleParents,
      RootDirectory dataDirectory,
      File latestSignedBlock,
      BiConsumer<FinalizationMessage, Consumer<SafeDataOutputStream>> flood,
      Function<Hash, SignedTransaction> pendingTransactionLookup,
      Function<Hash, ExecutableEvent> pendingEventLookup,
      BooleanSupplier shutdownInProgress,
      Function<Hash, byte[]> rawStateAccess) {
    this.myKeyPair = myKeyPair;
    this.myAccount = myKeyPair.getPublic();
    this.myBlsKeyPair = myBlsKeyPair;
    this.latestSignedBlock = new BlockTimeStorage(latestSignedBlock, 0);
    this.flood = flood;
    this.pendingTransactionLookup = pendingTransactionLookup;
    this.files = new StorageFiles(dataDirectory);
    this.pendingEventLookup = pendingEventLookup;
    this.shutdownInProgress = shutdownInProgress;
    this.rawStateAccess = rawStateAccess;

    files
        .getExisting()
        .forEach(
            storageFile ->
                blockStates.add(
                    BlockStateStorage.createFromFile(
                        storageFile, possibleParents, rawStateAccess)));
  }

  synchronized void clean(List<BlockAndState> possibleParents) {
    List<Hash> possibleParentIds =
        possibleParents.stream()
            .map(BlockAndState::getBlock)
            .map(Block::identifier)
            .collect(Collectors.toList());
    blockStates.forEach(block -> block.clean(possibleParentIds));
  }

  private BlockState getForBlockTime(long blockTime) {
    return blockStates.stream()
        .filter(BlockStateStorage::hasValue)
        .map(BlockStateStorage::getCurrentState)
        .filter(state -> state.getBlock().getBlockTime() == blockTime)
        .findFirst()
        .orElse(null);
  }

  private BlockStateStorage nextStorage() {
    Optional<BlockStateStorage> freeStorage =
        blockStates.stream().filter(Predicate.not(BlockStateStorage::hasValue)).findAny();
    if (freeStorage.isEmpty()) {
      BlockStateStorage storage = BlockStateStorage.createEmpty(files.createNext());
      blockStates.add(storage);
      return storage;
    } else {
      return freeStorage.get();
    }
  }

  synchronized List<FinalBlock> getFinalBlocks() {
    return blockStates.stream()
        .filter(BlockStateStorage::hasValue)
        .map(BlockStateStorage::getCurrentState)
        .filter(BlockState::isDone)
        .map(BlockState::asFinalBlock)
        .collect(Collectors.toList());
  }

  List<BlockState> getBlockStates() {
    return blockStates.stream()
        .filter(BlockStateStorage::hasValue)
        .map(BlockStateStorage::getCurrentState)
        .collect(Collectors.toList());
  }

  long getLatestSignedBlockTime() {
    return latestSignedBlock.getLatestBlockTime();
  }

  synchronized void addEcho(SignatureAndVerification signatureAndVerification) {
    boolean added =
        blockStates.stream()
            .filter(BlockStateStorage::hasValue)
            .map(BlockStateStorage::getCurrentState)
            .anyMatch(blockState -> blockState.addEcho(signatureAndVerification));
    if (added) {
      floodEcho(signatureAndVerification);
    }
  }

  private void floodEcho(SignatureAndVerification signatureAndVerification) {
    flood.accept(FinalizationMessage.ECHO, signatureAndVerification::write);
  }

  private void floodBlock(FastTrackBlock block) {
    flood.accept(
        FinalizationMessage.BLOCK,
        stream -> {
          block.block.write(stream);
          block.producerSignature.write(stream);
        });
  }

  synchronized void writeForSync(SafeDataOutputStream stream) {
    Collection<BlockState> values =
        blockStates.stream()
            .filter(BlockStateStorage::hasValue)
            .map(BlockStateStorage::getCurrentState)
            .collect(Collectors.toList());
    stream.writeInt(values.size());
    for (BlockState value : values) {
      value.writeTo(stream);
    }
  }

  synchronized void addBlock(FastTrackBlock fastTrackBlock) {
    if (fastTrackBlock.isValid()) {
      if (fastTrackBlock.validateTransactions(pendingTransactionLookup, pendingEventLookup)) {
        addValidBlock(fastTrackBlock);
      } else {
        logger.warn("Received invalid block with invalid transactions {}", fastTrackBlock.block);
        throw new RuntimeException("Block with invalid transactions");
      }
    } else {
      logger.warn("Received invalid block {}", fastTrackBlock.block);
      throw new RuntimeException("Invalid block");
    }
  }

  private void addValidBlock(FastTrackBlock parent) {
    Block block = parent.block;
    BlockState currentState = getForBlockTime(block.getBlockTime());
    if (currentState != null) {
      if (!currentState.getBlock().equals(block)) {
        throw new RuntimeException(
            "Unable to add block since the block time has already been seen");
      } else {
        logger.debug("Skipping already handled block {}", block);
      }
    } else {
      logger.info("Handling proposed block {}", block);
      BlockStateStorage storage = nextStorage();
      storage.setCurrentState(
          block,
          parent.producerSignature,
          parent.getVerifiers(),
          parent.getVerifierBlsKeys(),
          rawStateAccess);
      floodBlock(parent);
      BlockState appended = storage.getCurrentState();
      if (latestSignedBlock.getLatestBlockTime() < block.getBlockTime()
          && appended.isSigner(myAccount)) {
        if (!shutdownInProgress.getAsBoolean()) {
          logger.info("Signing {}", appended);
          latestSignedBlock.storeLatestBlockTime(block.getBlockTime());
          // The proof that we will send around is the tuple (s, b, h) where
          //  - s = Sign(key, b)
          //  - b = Sign(BlsKey, blockID)
          //  - h = proof-of-verification
          // the reason for (normal) signing the BLS signature is to prevent a bad producer from
          // making a denial of service attack, by sending lots of bogus BLS signatures. If s is
          // invalid, then we wont consider b. And if s is valid (but b is not) then we'll only
          // consider b once (and not accept more signatures from this producer).
          Hash myProof = createProofOfVerification(myAccount, block.getState());
          BlsSignature myBlsSignature = myBlsKeyPair.sign(appended.getEchoHash());
          Hash message =
              FinalizationMessageHelper.createMessage(appended.getEchoHash(), myBlsSignature);
          Signature mySignature = myKeyPair.sign(message);
          SignatureAndVerification signatureAndVerification =
              new SignatureAndVerification(mySignature, myBlsSignature, myProof);
          appended.addEcho(signatureAndVerification);
          floodEcho(signatureAndVerification);
        } else {
          logger.info("Not signing as shutdown is active");
        }
      } else {
        logger.info(
            "Not signing {}. latestSignedBlock={}. ",
            appended,
            latestSignedBlock.getLatestBlockTime());
      }
    }
  }

  private Hash createProofOfVerification(BlockchainPublicKey identity, Hash state) {
    return Hash.create(
        stream -> {
          identity.write(stream);
          stream.write(rawStateAccess.apply(state));
        });
  }

  @Override
  public Stream<AutoCloseable> resources() {
    return Stream.concat(Stream.of(this.latestSignedBlock), blockStates.stream());
  }
}
