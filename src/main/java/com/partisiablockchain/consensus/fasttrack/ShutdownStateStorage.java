package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.coverage.ThrowingConsumer;
import com.secata.tools.coverage.WithCloseableResources;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class ShutdownStateStorage implements WithCloseableResources {

  private static final Logger logger = LoggerFactory.getLogger(ShutdownStateStorage.class);

  private static final int INDEX_OF_PROTOCOL_ID = 0;
  private static final int LENGTH_OF_PROTOCOL_ID = Hash.BYTES;
  private static final int INDEX_OF_RANDOM_SEED = INDEX_OF_PROTOCOL_ID + LENGTH_OF_PROTOCOL_ID;
  private static final int LENGTH_OF_RANDOM_SEED = 32;
  private static final int INDEX_OF_FIRST_MESSAGE = INDEX_OF_RANDOM_SEED + LENGTH_OF_RANDOM_SEED;

  private final RandomAccessFile storageFile;

  ShutdownStateStorage(File dataFile) {
    this.storageFile =
        ExceptionConverter.call(
            () -> new RandomAccessFile(dataFile, "rw"), "Unable to open data file");
  }

  void clear() {
    ExceptionConverter.run(() -> storageFile.setLength(0), "Unable to clear storage");
  }

  Hash readProtocolId() {
    byte[] bytes = new byte[LENGTH_OF_PROTOCOL_ID];
    getPersistenceAtLocation(INDEX_OF_PROTOCOL_ID, file -> file.readFully(bytes));
    return Hash.read(SafeDataInputStream.createFromBytes(bytes));
  }

  void writeProtocolIdentifier(Hash protocolIdentifier) {
    getPersistenceAtLocation(
        INDEX_OF_PROTOCOL_ID, file -> file.write(protocolIdentifier.getBytes()));
  }

  byte[] readRandomSeed() {
    byte[] bytes = new byte[LENGTH_OF_RANDOM_SEED];
    getPersistenceAtLocation(INDEX_OF_RANDOM_SEED, file -> file.readFully(bytes));
    return bytes;
  }

  void writeRandomSeed(byte[] randomSeed) {
    if (randomSeed.length != LENGTH_OF_RANDOM_SEED) {
      throw new IllegalArgumentException("Random seed must be of length " + LENGTH_OF_RANDOM_SEED);
    }
    getPersistenceAtLocation(INDEX_OF_RANDOM_SEED, file -> file.write(randomSeed));
  }

  void forEachPersistedMessage(Consumer<byte[]> callback) {
    ExceptionLogger.handle(
        logger::info,
        () ->
            getPersistenceAtLocation(
                INDEX_OF_FIRST_MESSAGE,
                file -> {
                  long size = file.length();
                  while (file.getFilePointer() != size) {
                    int messageSize = file.readInt();
                    byte[] message = new byte[messageSize];
                    file.readFully(message);
                    callback.accept(message);
                  }
                }),
        "Error while reading persistent messages");
  }

  void writeFloodedMessage(byte[] serializedMessage) {
    getPersistenceAtLocation(
        storageLength(),
        writer -> {
          writer.writeInt(serializedMessage.length);
          writer.write(serializedMessage);
        });
  }

  private void getPersistenceAtLocation(long location, ThrowingConsumer<RandomAccessFile> writer) {
    ExceptionConverter.run(
        () -> {
          storageFile.seek(location);
          writer.accept(storageFile);
        },
        "Unable to update persistence");
  }

  @Override
  public Stream<AutoCloseable> resources() {
    return Stream.of(storageFile);
  }

  boolean isEmpty() {
    return storageLength() == 0;
  }

  long storageLength() {
    return ExceptionConverter.call(storageFile::length, "Unable to read length");
  }
}
