package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

final class AsyncBroadcastFromSignatures implements Protocol<byte[]> {

  private final byte[] broadcastId;
  private final BlockchainAddress broadcaster;
  private final Consumer<DataStreamSerializable> flood;
  private final PartyConfiguration partyConfiguration;

  private Map<BlockchainAddress, Signature> signatures = new HashMap<>();
  private SignatureRequest request;
  private boolean done = false;

  private AsyncBroadcastFromSignatures(
      byte[] broadcastId,
      PartyConfiguration partyConfiguration,
      BlockchainAddress broadcaster,
      Consumer<DataStreamSerializable> flood) {
    this.broadcastId = broadcastId;
    this.partyConfiguration = partyConfiguration;
    this.broadcaster = broadcaster;
    this.flood = flood;
  }

  static AsyncBroadcastFromSignatures receiveBroadcast(
      byte[] broadcastId,
      BlockchainAddress broadcaster,
      PartyConfiguration partyConfiguration,
      Consumer<DataStreamSerializable> flood) {
    return new AsyncBroadcastFromSignatures(broadcastId, partyConfiguration, broadcaster, flood);
  }

  static AsyncBroadcastFromSignatures sendBroadcast(
      byte[] broadcastId,
      PartyConfiguration partyConfiguration,
      Consumer<DataStreamSerializable> flood,
      byte[] broadcastValue) {
    AsyncBroadcastFromSignatures broadcast =
        new AsyncBroadcastFromSignatures(
            broadcastId, partyConfiguration, partyConfiguration.getMyAddress(), flood);
    broadcast.sendBroadcast(broadcastValue);
    return broadcast;
  }

  private void sendBroadcast(byte[] broadcastValue) {
    SignatureRequest request =
        new SignatureRequest(
            broadcastValue, partyConfiguration.sign(createHashForRequest(broadcastValue)));
    setAndFloodRequest(request);
  }

  @Override
  public byte[] getResult() {
    if (!isDone()) {
      throw new IllegalStateException("Broadcast is not finished");
    }
    return request.value;
  }

  @Override
  public boolean isDone() {
    return done;
  }

  @Override
  public void incoming(SafeDataInputStream message) {
    if (isDone()) {
      return;
    }
    MessageType messageType = message.readEnum(MessageType.values());
    if (request == null) {
      if (messageType == MessageType.SIGNATURE_REQUEST) {
        SignatureRequest read = SignatureRequest.read(message);
        if (isRequestValid(read)) {
          setAndFloodRequest(read);
          signRequest();
        }
      }
    } else if (messageType == MessageType.SIGNATURE) {
      collectSignature(Signature.read(message));
    }
    if (messageType == MessageType.SIGNATURE_SET) {
      handleSignatureSet(SignatureSet.read(message));
    }
  }

  private void signRequest() {
    Signature sign = partyConfiguration.sign(createHashForRequest(request));
    collectSignature(sign);
  }

  private void handleSignatureSet(SignatureSet signatureSet) {
    SignatureRequest request = signatureSet.request;
    if (isRequestValid(request)) {
      Hash messageHash = createHashForRequest(request);
      Map<BlockchainAddress, Signature> signatures =
          signatureSet.collectValidSignatures(messageHash, this::isSigner);
      checkFinished(request, signatures);
    }
  }

  private void checkFinished(SignatureRequest request, Map<BlockchainAddress, Signature> signers) {
    int requiredAdditionalSignatures = partyConfiguration.honestCount() - 1;
    if (signers.size() >= requiredAdditionalSignatures) {
      this.request = request;
      this.signatures = signers;
      flood.accept(
          stream -> {
            stream.writeEnum(MessageType.SIGNATURE_SET);
            new SignatureSet(request, List.copyOf(signers.values())).write(stream);
          });
      this.done = true;
    }
  }

  private void collectSignature(Signature signature) {
    BlockchainAddress signer = signature.recoverSender(createHashForRequest(request));
    if (isSigner(signer) && !signatures.containsKey(signer)) {
      signatures.put(signer, signature);
      floodSignature(signature);
      checkFinished(request, signatures);
    }
  }

  private boolean isSigner(BlockchainAddress signer) {
    return !broadcaster.equals(signer) && partyConfiguration.isParticipating(signer);
  }

  private void floodSignature(Signature signature) {
    flood.accept(
        stream -> {
          stream.writeEnum(MessageType.SIGNATURE);
          signature.write(stream);
        });
  }

  private void setAndFloodRequest(SignatureRequest request) {
    this.request = request;
    flood.accept(
        stream -> {
          stream.writeEnum(MessageType.SIGNATURE_REQUEST);
          request.write(stream);
        });
  }

  private boolean isRequestValid(SignatureRequest request) {
    Hash messageHash = createHashForRequest(request);
    return broadcaster.equals(request.signature.recoverSender(messageHash));
  }

  private Hash createHashForRequest(SignatureRequest request) {
    return createHashForRequest(request.value);
  }

  Hash createHashForRequest(byte[] broadcastValue) {
    return Hash.create(
        stream -> {
          stream.write(broadcastId);
          broadcaster.write(stream);
          stream.writeDynamicBytes(broadcastValue);
        });
  }

  BlockchainAddress getBroadcaster() {
    return broadcaster;
  }

  static final class SignatureRequest {

    private final byte[] value;
    private final Signature signature;

    SignatureRequest(byte[] value, Signature signature) {
      this.value = value;
      this.signature = signature;
    }

    void write(SafeDataOutputStream stream) {
      stream.writeDynamicBytes(value);
      signature.write(stream);
    }

    static SignatureRequest read(SafeDataInputStream stream) {
      byte[] broadcastValue = stream.readDynamicBytes();
      Signature signature = Signature.read(stream);
      return new SignatureRequest(broadcastValue, signature);
    }
  }

  static final class SignatureSet {

    private final SignatureRequest request;
    private final List<Signature> signatures;

    SignatureSet(SignatureRequest request, List<Signature> signatures) {
      this.request = request;
      this.signatures = signatures;
    }

    void write(SafeDataOutputStream stream) {
      request.write(stream);
      FastTrackProducer.SIGNATURE_LIST.writeDynamic(stream, signatures);
    }

    static SignatureSet read(SafeDataInputStream stream) {
      SignatureRequest request = SignatureRequest.read(stream);
      List<Signature> signatures = FastTrackProducer.SIGNATURE_LIST.readDynamic(stream);
      return new SignatureSet(request, signatures);
    }

    Map<BlockchainAddress, Signature> collectValidSignatures(
        Hash messageHash, Predicate<BlockchainAddress> isSigner) {
      Map<BlockchainAddress, Signature> signers = new HashMap<>();
      for (Signature signature : signatures) {
        BlockchainAddress blockchainAddress = signature.recoverSender(messageHash);
        if (isSigner.test(blockchainAddress)) {
          signers.put(blockchainAddress, signature);
        }
      }
      return signers;
    }

    SignatureRequest getRequest() {
      return request;
    }

    List<Signature> getSignatures() {
      return Collections.unmodifiableList(signatures);
    }
  }

  enum MessageType {
    SIGNATURE_REQUEST,
    SIGNATURE,
    SIGNATURE_SET
  }
}
