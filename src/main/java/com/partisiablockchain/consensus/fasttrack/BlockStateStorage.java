package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class BlockStateStorage implements AutoCloseable {

  private static final Logger logger = LoggerFactory.getLogger(BlockStateStorage.class);

  private final RandomAccessFile storage;

  private BlockState currentState;

  private BlockStateStorage(RandomAccessFile file) {
    this.storage = file;
  }

  static BlockStateStorage createEmpty(RandomAccessFile file) {
    return new BlockStateStorage(file);
  }

  static BlockStateStorage createFromFile(
      RandomAccessFile storageFile,
      List<BlockAndState> possibleParents,
      Function<Hash, byte[]> rawStateAccess) {
    BlockStateStorage storage = new BlockStateStorage(storageFile);
    storage.initializeFromFile(possibleParents, rawStateAccess);
    return storage;
  }

  void setCurrentState(
      Block block,
      Signature producerSignature,
      List<BlockchainPublicKey> verifiers,
      List<BlsPublicKey> verifierBlsKeys,
      Function<Hash, byte[]> rawStateAccess) {
    ExceptionConverter.run(
        () -> {
          storage.setLength(0);
          byte[] serialize =
              SafeDataOutputStream.serialize(
                  stream -> {
                    block.write(stream);
                    producerSignature.write(stream);
                  });
          storage.writeInt(serialize.length);
          storage.write(serialize);
        },
        "Unable to write initial part");
    this.currentState =
        new BlockState(
            this::addSignature,
            block,
            producerSignature,
            verifiers,
            verifierBlsKeys,
            List.of(),
            rawStateAccess);
  }

  private void addSignature(SignatureAndVerification signatureAndVerification) {
    ExceptionConverter.run(
        () -> storage.write(SafeDataOutputStream.serialize(signatureAndVerification::write)),
        "Unable to write signature");
  }

  @Override
  public void close() {
    ExceptionLogger.handle(logger::info, storage::close, "Unable to close file");
  }

  private void initializeFromFile(
      List<BlockAndState> possibleParents, Function<Hash, byte[]> rawStateAccess) {
    try {
      int length = storage.readInt();
      byte[] bytes = new byte[length];
      storage.readFully(bytes);
      SafeDataInputStream stream = SafeDataInputStream.createFromBytes(bytes);
      Block block = Block.read(stream);
      Signature producerSignature = Signature.read(stream);
      Optional<BlockAndState> foundParent =
          possibleParents.stream()
              .filter(parent -> parent.getBlock().identifier().equals(block.getParentBlock()))
              .findFirst();
      if (foundParent.isPresent()) {
        ImmutableChainState state = foundParent.get().getState();
        ConsensusState consensusState = new ConsensusState(state);
        List<BlockchainPublicKey> verifiers = consensusState.getCommittee();
        List<BlsPublicKey> verifierBlsKeys = consensusState.getCommitteeBlsKeys();
        // the rest of the storage are the signatures, BLS signatures and proofs of verification.
        byte[] signatureData = new byte[(int) (storage.length() - 4 - length)];
        // 65 + 48 + 32 = |signature| + |BLS signature| + |PoV|
        int signatureCount = signatureData.length / (65 + 48 + 32);
        storage.readFully(signatureData);
        List<SignatureAndVerification> signatures =
            FastTrackProducer.SIGNATURE_AND_VERIFICATION_LIST.readFixed(
                SafeDataInputStream.createFromBytes(signatureData), signatureCount);
        this.currentState =
            new BlockState(
                this::addSignature,
                block,
                producerSignature,
                List.copyOf(verifiers),
                List.copyOf(verifierBlsKeys),
                signatures,
                rawStateAccess);
      } else {
        logger.info("Ignoring pending file as parent is outdated for block {}", block);
      }
    } catch (Exception e) {
      logger.info("Unable to read from pending file", e);
    }
  }

  void clean(List<Hash> possibleParentIds) {
    if (hasValue()) {
      if (!possibleParentIds.contains(getCurrentState().getBlock().getParentBlock())) {
        currentState = null;
      } else if (possibleParentIds.contains(getCurrentState().getBlock().identifier())) {
        currentState = null;
      }
    }
  }

  boolean hasValue() {
    return getCurrentState() != null;
  }

  BlockState getCurrentState() {
    return currentState;
  }
}
