package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import java.util.List;

/** An interface for getting info about the currently active committee. */
public interface CommitteeInfoGetter {

  /**
   * Gets the id of the active committee.
   *
   * @return id of the active committee.
   */
  int getActiveCommittee();

  /**
   * Gets a list of the blockchain public keys of the members of the active committee.
   *
   * @return The members of the active committee.
   */
  List<BlockchainPublicKey> getCommittee();
}
