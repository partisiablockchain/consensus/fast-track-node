package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockAndStateWithParent;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.util.LivenessMonitor;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.coverage.WithCloseableResources;
import java.io.File;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for handling the shutdown procedure as described in the book "Secure
 * Distributed Systems", as well as starting the procedure.
 *
 * <p>To handle that the procedure should run on a async flooding network we persist all messages
 * that we flood to ensure that we can later replay and regain the same state even if some servers
 * crash and come up later.
 *
 * <p>The procedure has been extended with the flooding of signatures on a reset block when
 * consensus has been reached for which block time we should terminate and restart the fast track
 * procedure. This allow updating the committee on chain after the shutdown procedure has
 * terminated.
 */
final class ShutdownState implements Listener, WithCloseableResources {

  private static final Logger logger = LoggerFactory.getLogger(ShutdownState.class);

  private final SecureRandom random = new SecureRandom();
  private final VoteCreator voteCreator;
  private final BlockchainLedger blockchain;
  private final String subChainId;
  private final KeyPair keyPair;
  private final BlsKeyPair blsKeyPair;
  private final Runnable syncTrigger;
  private final Consumer<DataStreamSerializable> flood;
  private final LivenessMonitor liveness;
  private final LivenessMonitor shutdownLiveness;
  private final ShutdownStateStorage persistentShutdownState;

  private boolean initialized = false;
  private long maxPrefix;

  private final byte[] randomSeed = new byte[32];
  private Hash protocolIdentifier;
  private PartyConfiguration configuration;

  private ShutdownStage stage = ShutdownStage.NOT_PARTICIPATING;
  private Map<Long, Map<BlockchainAddress, Signature>> shutdownSignatures = new HashMap<>();
  private Protocol<Long> cutOffVote;
  private ResetBlock resetBlock;
  private long latestEvent = System.currentTimeMillis();

  /** Block time when shutdown began. */
  private long blockTimeForShutdown = Long.MAX_VALUE;

  ShutdownState(
      VoteCreator voteCreator,
      File dataFile,
      BlockchainLedger blockchain,
      KeyPair keyPair,
      BlsKeyPair blsKeyPair,
      Runnable syncTrigger,
      Consumer<DataStreamSerializable> flood,
      long heartBeatInterval,
      long networkDelay) {
    this.subChainId = blockchain.getChainState().getExecutedState().getSubChainId();
    this.voteCreator = voteCreator;
    this.blockchain = blockchain;
    this.keyPair = keyPair;
    this.blsKeyPair = blsKeyPair;
    this.persistentShutdownState = new ShutdownStateStorage(dataFile);
    this.syncTrigger = syncTrigger;
    this.flood = flood;
    initialize(blockchain);
    this.liveness =
        new LivenessMonitor(
            this::triggerShutdown,
            this::getLatestEvent,
            shutdownTimeout(heartBeatInterval, networkDelay));
    this.shutdownLiveness =
        new LivenessMonitor(
            this::triggerLiveness, this::getLatestEvent, shutdownLiveness(networkDelay));
  }

  ShutdownState(
      BlockchainLedger blockchain,
      File dataFile,
      KeyPair keyPair,
      BlsKeyPair blsKeyPair,
      Runnable syncTrigger,
      Consumer<DataStreamSerializable> flood,
      long heartBeatInterval,
      long networkDelay) {
    this(
        CutOffVote::create,
        dataFile,
        blockchain,
        keyPair,
        blsKeyPair,
        syncTrigger,
        flood,
        heartBeatInterval,
        networkDelay);
  }

  long getLatestEvent() {
    return Math.max(latestEvent, blockchain.getLatestAppend());
  }

  static long shutdownTimeout(long productionTimeout, long networkDelay) {
    return 2 * productionTimeout + 5 * networkDelay;
  }

  static long shutdownLiveness(long networkDelay) {
    return 10 * networkDelay;
  }

  private synchronized void initialize(BlockchainLedger blockchain) {
    blockchain.attach(this);
    BlockAndState latest = blockchain.latest();
    List<BlockAndState> proposals = blockchain.getProposals();
    Block latestProposal = proposals.size() > 0 ? proposals.get(0).getBlock() : latest.getBlock();
    this.maxPrefix = latestProposal.getBlockTime();
    updateCommittee(latest.getState(), false);
    ExceptionLogger.handle(
        logger::info,
        this::initializeFromStorage,
        "Error while initializing from previous persistent storage");
    initialized = true;
  }

  private synchronized void initializeFromStorage() {
    boolean isShuttingDown = !persistentShutdownState.isEmpty();
    if (isShuttingDown) {
      Hash protocolId = persistentShutdownState.readProtocolId();
      if (Objects.equals(protocolId, this.protocolIdentifier)) {
        stage = ShutdownStage.COLLECT_SIGNATURES;
        System.arraycopy(persistentShutdownState.readRandomSeed(), 0, randomSeed, 0, 32);
        persistentShutdownState.forEachPersistedMessage(
            bytes -> incomingMessage(SafeDataInputStream.createFromBytes(bytes)));
      } else {
        logger.info("Persistent shutdown state outdated - ignoring");
        persistentShutdownState.clear();
      }
    }
  }

  private void reset(
      Hash committeeId,
      Hash protocolIdentifier,
      PartyConfiguration configuration,
      boolean resetStorage) {
    logger.info(
        "Reset called with committee: {}, old pid: {}, new pid: {}",
        committeeId,
        this.protocolIdentifier,
        protocolIdentifier);
    this.protocolIdentifier = protocolIdentifier;
    this.configuration = configuration;
    this.stage = configuration == null ? ShutdownStage.NOT_PARTICIPATING : ShutdownStage.WAITING;
    this.shutdownSignatures = new HashMap<>();
    this.cutOffVote = null;
    this.resetBlock = null;
    this.blockTimeForShutdown = Long.MAX_VALUE;
    this.random.nextBytes(randomSeed);
    if (resetStorage) {
      persistentShutdownState.clear();
    }
  }

  private void bumpLatestEvent() {
    setLatestEvent(System.currentTimeMillis());
  }

  void setLatestEvent(long latestEvent) {
    this.latestEvent = latestEvent;
  }

  Hash getProtocolIdentifier() {
    return protocolIdentifier;
  }

  @Override
  public synchronized void newFinalBlock(Block block, ImmutableChainState state) {
    updateCommittee(state, true);
    bumpLatestEvent();
  }

  private void updateCommittee(ImmutableChainState state, boolean resetStorage) {
    ConsensusState consensusState = new ConsensusState(state);
    updatePlugin(consensusState, resetStorage);
  }

  private void updatePlugin(ConsensusState consensusState, boolean resetStorage) {
    Hash protocolIdentifier = null;
    PartyConfiguration configuration = null;
    Hash committeeId = null;
    if (consensusState.isFastTrack()) {
      committeeId = consensusState.getCommitteeId();

      protocolIdentifier =
          deriveProtocolIdentifier(committeeId, consensusState.getResetBlocksSeen());

      List<BlockchainPublicKey> committee = consensusState.getCommittee();
      if (committee.contains(keyPair.getPublic())) {
        List<BlockchainAddress> committeeAddresses =
            committee.stream().map(BlockchainPublicKey::createAddress).collect(Collectors.toList());
        List<BlsPublicKey> committeeKeys = consensusState.getCommitteeBlsKeys();
        configuration =
            new PartyConfiguration(committeeAddresses, committeeKeys, keyPair, blsKeyPair);
      }
    }

    if (!Objects.equals(this.protocolIdentifier, protocolIdentifier)) {
      reset(committeeId, protocolIdentifier, configuration, resetStorage);
    }
  }

  Hash deriveProtocolIdentifier(Hash committeeId, int resetBlocksSeen) {
    return deriveProtocolIdentifier(committeeId, subChainId, resetBlocksSeen);
  }

  static Hash deriveProtocolIdentifier(Hash committeeId, String subChainId, int resetBlocksSeen) {
    return Hash.create(
        hash -> {
          committeeId.write(hash);
          if (subChainId != null) {
            hash.writeString(subChainId);
          }

          hash.writeInt(resetBlocksSeen);
        });
  }

  @Override
  public synchronized void newBlockProposal(Block block) {
    bumpLatestEvent();
    updateMaxPrefix(block);
    boolean newCommitteeAvailable =
        blockchain.getPossibleHeads().stream()
            .filter(b -> b.currentBlock().equals(block))
            .map(BlockAndStateWithParent::currentState)
            .map(ConsensusState::new)
            .anyMatch(ConsensusState::hasNewCommittee);
    if (newCommitteeAvailable) {
      triggerShutdown();
    }
  }

  private void updateMaxPrefix(Block block) {
    this.maxPrefix = Math.max(this.maxPrefix, block.getBlockTime());
    shutdownSignatures.keySet().removeIf(Predicate.not(this::isAcceptableBlockTime));
    if (isShuttingDown()) {
      if (stage == ShutdownStage.COLLECT_SIGNATURES) {
        if (maxPrefix > blockTimeForShutdown + 2) {
          this.stage = ShutdownStage.ABORTING;
        } else {
          createOwnShutdownSignature();
        }
      } else if (stage == ShutdownStage.ABORTING && maxPrefix > blockTimeForShutdown + 4) {
        abortShutdown();
      }
      createResetBlockIfAppropriate();
    }
  }

  private void createOwnShutdownSignature() {
    Hash hash = shutdownHash(maxPrefix);
    Signature signature = configuration.sign(hash);
    addShutdownSignature(maxPrefix, signature, configuration.getMyAddress());
  }

  private boolean isAcceptableBlockTime(long blockTime) {
    return Math.abs(blockTime - this.maxPrefix) <= 1;
  }

  synchronized void incoming(SafeDataInputStream message) {
    Hash protocolId = Hash.read(message);
    if (protocolId.equals(this.protocolIdentifier)) {
      incomingMessage(message);
    } else {
      logger.debug(
          "Got a message with wrong protocol identifier. Expected: {}, got: {}",
          this.protocolIdentifier,
          protocolId);
    }
  }

  private void incomingMessage(SafeDataInputStream message) {
    ShutdownMessage messageType = message.readEnum(ShutdownMessage.values());
    boolean collectingSignatures =
        stage == ShutdownStage.COLLECT_SIGNATURES
            || stage == ShutdownStage.WAITING
            || stage == ShutdownStage.ABORTING;
    if (collectingSignatures && messageType == ShutdownMessage.SIGNATURE) {
      long blockTime = message.readLong();
      if (isAcceptableBlockTime(blockTime)) {
        Hash shutdownHash = shutdownHash(blockTime);
        verifyParticipant(
            shutdownHash,
            message,
            (signature, signer) -> addShutdownSignature(blockTime, signature, signer));
      }
    }
    if (stage == ShutdownStage.DETERMINE_CUT_OFF && messageType == ShutdownMessage.CUT_OFF) {
      cutOffVote.incoming(message);
      if (cutOffVote.isDone()) {
        logger.info(
            "Finished DETERMINE_CUT_OFF. Resulting final block: {}", cutOffVote.getResult());
        stage = ShutdownStage.CREATE_BLOCK;
        createResetBlockIfAppropriate();
      }
    }
    if (stage == ShutdownStage.CREATE_BLOCK
        && messageType == ShutdownMessage.RESET_BLOCK
        && this.resetBlock != null) {
      addResetBlockSignature(PairOfSignatures.create(message));
    }
  }

  private void verifyParticipant(
      Hash signedHash,
      SafeDataInputStream message,
      BiConsumer<Signature, BlockchainAddress> successCallback) {
    Signature signature = Signature.read(message);
    BlockchainAddress signer = signature.recoverSender(signedHash);
    if (configuration.isParticipating(signer)) {
      successCallback.accept(signature, signer);
    }
  }

  private void addResetBlockSignature(PairOfSignatures signatures) {
    if (resetBlock.addSignatures(signatures)) {
      floodMessage(ShutdownMessage.RESET_BLOCK, signatures::write);
    }
    if (resetBlock.isDone()) {
      FinalBlock block = this.resetBlock.finished();
      blockchain.appendBlock(block);
    }
  }

  synchronized void writeShutDownStage(SafeDataOutputStream stream) {
    stream.writeEnum(this.stage);
  }

  Hash shutdownHash(long blockTime) {
    Hash protocolIdentifier = this.protocolIdentifier;
    return Hash.create(
        stream -> {
          stream.writeString("SHUTDOWN");
          protocolIdentifier.write(stream);
          stream.writeLong(blockTime);
        });
  }

  private void createResetBlockIfAppropriate() {
    if (stage == ShutdownStage.CREATE_BLOCK && this.resetBlock == null) {
      Optional<BlockAndState> foundShutdownBlock = findBlock(cutOffVote.getResult());
      if (foundShutdownBlock.isPresent()) {
        BlockAndState blockAndState = foundShutdownBlock.get();
        this.resetBlock = createResetBlock(blockAndState);
        logger.info("Created reset block: {}", resetBlock.block);
        addResetBlockSignature(resetBlock.createSignatures());
      }
    }
  }

  private ResetBlock createResetBlock(BlockAndState blockAndState) {
    Block block = blockAndState.getBlock();
    return new ResetBlock(
        new Block(
            block.getProductionTime() + 1,
            block.getBlockTime() + 1,
            new ConsensusState(blockAndState.getState()).getActiveCommittee(),
            block.identifier(),
            blockAndState.getState().getHash(),
            List.of(),
            List.of()),
        configuration);
  }

  private Optional<BlockAndState> findBlock(long blockTime) {
    List<BlockAndState> proposals = blockchain.getProposals();
    return Stream.concat(proposals.stream(), Stream.of(blockchain.latest()))
        .filter(blockAndState -> blockAndState.getBlock().getBlockTime() == blockTime)
        .findFirst();
  }

  synchronized void triggerLiveness() {
    if (isShuttingDown()) {
      syncTrigger.run();
    }
  }

  void abortShutdown() {
    logger.info("Aborting shutdown procedure");
    this.stage = ShutdownStage.WAITING;
    this.random.nextBytes(randomSeed);
    blockTimeForShutdown = Long.MAX_VALUE;
    persistentShutdownState.clear();
  }

  synchronized void triggerShutdown() {
    if (stage == ShutdownStage.WAITING) {
      logger.info("Initializing shutdown procedure");
      Hash protocolIdentifier = this.protocolIdentifier;
      byte[] randomSeed = this.randomSeed;
      persistentShutdownState.writeProtocolIdentifier(protocolIdentifier);
      persistentShutdownState.writeRandomSeed(randomSeed);

      this.stage = ShutdownStage.COLLECT_SIGNATURES;
      this.blockTimeForShutdown = maxPrefix;

      this.shutdownSignatures.forEach(
          (blockTime, signatures) ->
              signatures.values().forEach(signature -> floodSignature(blockTime, signature)));
      createOwnShutdownSignature();
      checkSignatureCount();
    } else if (stage == ShutdownStage.ABORTING) {
      this.stage = ShutdownStage.COLLECT_SIGNATURES;
      blockTimeForShutdown = maxPrefix;
      this.shutdownSignatures.forEach(
          (blockTime, signatures) ->
              signatures.values().forEach(signature -> floodSignature(blockTime, signature)));
      createOwnShutdownSignature();
    }
  }

  void addShutdownSignature(long blockTime, Signature signature, BlockchainAddress signer) {
    Signature existing =
        shutdownSignatures
            .computeIfAbsent(blockTime, ignored -> new HashMap<>())
            .putIfAbsent(signer, signature);
    if (stage != ShutdownStage.ABORTING && stage != ShutdownStage.WAITING && existing == null) {
      floodSignature(blockTime, signature);
      checkSignatureCount();
    }
  }

  void checkSignatureCount() {
    if (stage == ShutdownStage.COLLECT_SIGNATURES) {
      Set<Map.Entry<Long, Map<BlockchainAddress, Signature>>> entrySet =
          this.shutdownSignatures.entrySet();
      for (Map.Entry<Long, Map<BlockchainAddress, Signature>> blockSignatures : entrySet) {
        if (blockSignatures.getValue().size() >= configuration.honestCount()) {
          long maximalBlockTime = blockSignatures.getKey();
          logger.info(
              "Initializing DETERMINE_CUT_OFF protocol for value={}, pid: {}",
              maximalBlockTime,
              protocolIdentifier);

          stage = ShutdownStage.DETERMINE_CUT_OFF;
          cutOffVote =
              voteCreator.create(
                  configuration,
                  dataStreamSerializable ->
                      floodMessage(ShutdownMessage.CUT_OFF, dataStreamSerializable),
                  this.protocolIdentifier.getBytes(),
                  maximalBlockTime,
                  new DeterministicRandomBitGenerator(randomSeed));
        }
      }
    }
  }

  private void floodSignature(long blockTime, Signature signature) {
    floodMessage(
        ShutdownMessage.SIGNATURE,
        stream -> {
          stream.writeLong(blockTime);
          signature.write(stream);
        });
  }

  void floodMessage(ShutdownMessage messageType, Consumer<SafeDataOutputStream> message) {
    if (!initialized) {
      return;
    }
    bumpLatestEvent();
    byte[] serializedMessage =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeEnum(messageType);
              message.accept(stream);
            });
    persistentShutdownState.writeFloodedMessage(serializedMessage);

    flood.accept(writeWithIdentifier(serializedMessage));
  }

  boolean isStorageEmpty() {
    return persistentShutdownState.isEmpty();
  }

  long getStorageLength() {
    return persistentShutdownState.storageLength();
  }

  Map<Long, Set<BlockchainAddress>> getShutdownSigners() {
    return shutdownSignatures.keySet().stream()
        .collect(
            Collectors.toMap(
                Function.identity(), blockTime -> shutdownSignatures.get(blockTime).keySet()));
  }

  @SuppressWarnings("EnumOrdinal")
  boolean isShuttingDown() {
    return stage.ordinal() > ShutdownStage.WAITING.ordinal();
  }

  byte[] getRandomSeed() {
    return randomSeed;
  }

  ShutdownStage getStage() {
    return stage;
  }

  @Override
  public Stream<AutoCloseable> resources() {
    return Stream.of(liveness, shutdownLiveness, persistentShutdownState);
  }

  @SuppressWarnings("EnumOrdinal")
  synchronized void sendSync(Consumer<DataStreamSerializable> sync, ShutdownStage invocation) {
    if (isShuttingDown()) {
      persistentShutdownState.forEachPersistedMessage(
          message -> {
            int invocationForStoredMessage = message[0];
            // Note that invocation for stored message are two lower than invocation, therefore the
            // offset.
            if (invocationForStoredMessage + 2 >= invocation.ordinal()) {
              sync.accept(writeWithIdentifier(message));
            }
          });
    }
  }

  private DataStreamSerializable writeWithIdentifier(byte[] message) {
    Hash protocolIdentifier = this.protocolIdentifier;
    return stream -> {
      protocolIdentifier.write(stream);
      stream.write(message);
    };
  }

  public Block getResetBlocks() {
    return resetBlock.block;
  }

  enum ShutdownMessage {
    SIGNATURE,
    CUT_OFF,
    RESET_BLOCK
  }

  /** The ordering of these enums matters, and rearranging them will break the node. */
  enum ShutdownStage {
    NOT_PARTICIPATING,
    WAITING,
    COLLECT_SIGNATURES,
    DETERMINE_CUT_OFF,
    CREATE_BLOCK,
    ABORTING
  }

  interface VoteCreator {

    Protocol<Long> create(
        PartyConfiguration partyConfiguration,
        Consumer<DataStreamSerializable> flood,
        byte[] protocolId,
        long maximalBlockTime,
        DeterministicRandomBitGenerator generator);
  }

  static final class ResetBlock {

    private final Block block;
    private final PartyConfiguration configuration;
    private final BlsSignatureHelper<PairOfSignatures> helper;

    ResetBlock(Block block, PartyConfiguration configuration) {
      this.block = block;
      this.configuration = configuration;
      this.helper = new BlsSignatureHelper<>(configuration.getParties(), block.identifier());
    }

    public FinalBlock finished() {
      // this signature is gonna be null (and so there's gonna be a NPE) if this method is
      // called before the value of isDone() have been checked.
      BlsSignatureHelper.SignatureAndBitVector signature =
          helper.getSignature(configuration.getParticipants());
      return new FinalBlock(
          block,
          SafeDataOutputStream.serialize(
              stream -> {
                stream.writeDynamicBytes(signature.bitVector);
                signature.signature.write(stream);
              }));
    }

    public boolean addSignatures(PairOfSignatures signatures) {
      Hash message = getMessage(signatures.blsSignature);
      return helper.addSignature(signatures.signature, signatures, message);
    }

    private Hash getMessage(BlsSignature blsSignature) {
      return FinalizationMessageHelper.createMessage(block.identifier(), blsSignature);
    }

    public PairOfSignatures createSignatures() {
      // include a signature of the bls signature, so we can aggregate without suffering a denial
      // of service.
      Hash hash = block.identifier();
      BlsSignature blsSignature = configuration.blsSign(hash);
      Hash message = getMessage(blsSignature);
      Signature signature = configuration.sign(message);
      return new PairOfSignatures(signature, blsSignature);
    }

    public boolean isDone() {
      return helper.check();
    }
  }

  static final class PairOfSignatures
      implements DataStreamSerializable, BlsSignatureHelper.HasBlsSignature {

    public final Signature signature;
    public final BlsSignature blsSignature;

    public static PairOfSignatures create(SafeDataInputStream stream) {
      return new PairOfSignatures(Signature.read(stream), BlsSignature.read(stream));
    }

    public PairOfSignatures(Signature signature, BlsSignature blsSignature) {
      this.signature = signature;
      this.blsSignature = blsSignature;
    }

    @Override
    public void write(SafeDataOutputStream stream) {
      signature.write(stream);
      blsSignature.write(stream);
    }

    @Override
    public BlsSignature get() {
      return this.blsSignature;
    }
  }
}
