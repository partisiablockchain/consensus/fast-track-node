package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockAndStateWithParent;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.FinalizationData;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.consensus.fasttrack.rewards.EpochChangeListener;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.flooding.Connection;
import com.partisiablockchain.flooding.Packet;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.util.LivenessMonitor;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.coverage.WithCloseableResources;
import java.io.Serial;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.LongFunction;
import java.util.function.LongSupplier;
import java.util.function.Supplier;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Wraps the generic block producer with the fast track context. */
public final class FastTrackProducer implements Listener, WithCloseableResources {

  static final SafeListStream<Signature> SIGNATURE_LIST =
      SafeListStream.create(Signature::read, Signature::write);
  static final SafeListStream<SignatureAndVerification> SIGNATURE_AND_VERIFICATION_LIST =
      SafeListStream.create(SignatureAndVerification::read, SignatureAndVerification::write);
  private static final Logger logger = LoggerFactory.getLogger(FastTrackProducer.class);

  private final BlockchainPublicKey account;
  private final KeyPair keyPair;
  private final BlockchainLedger blockchain;

  private final FastTrackState state;
  private final ShutdownState shutdownState;
  private final BlockProducer blockProducer;
  private final LivenessMonitor livenessMonitor;

  private final Map<Hash, Integer> cachedExpectedProducer =
      Collections.synchronizedMap(new SizeConstrainedCache());

  /**
   * Create a new block producer.
   *
   * @param dataDirectory directory to store production and finalization data
   * @param blockchain the blockchain
   * @param keyPair producer key for signing new blocks
   * @param heartBeatInterval the time between producing blocks in milliseconds
   * @param networkDelay the expected network delay
   * @param additionalStatusProviders additional status providers
   */
  FastTrackProducer(
      RootDirectory dataDirectory,
      BlockchainLedger blockchain,
      KeyPair keyPair,
      BlsKeyPair blsKeyPair,
      long heartBeatInterval,
      long networkDelay,
      TransactionSender transactionSender,
      AdditionalStatusProviders additionalStatusProviders) {
    this.blockchain = blockchain;
    this.account = keyPair.getPublic();
    this.keyPair = keyPair;
    this.shutdownState =
        new ShutdownState(
            blockchain,
            dataDirectory.createFile("fastTrackShutdown"),
            keyPair,
            blsKeyPair,
            this::sendShutdownSync,
            this::floodShutdown,
            heartBeatInterval,
            networkDelay);
    this.state =
        new FastTrackState(
            keyPair,
            blsKeyPair,
            possibleParents(blockchain),
            dataDirectory.createSub("fastTrackProposals"),
            dataDirectory.createFile("latestSignedBlock"),
            this::floodFinalizationMessage,
            hashLookup(blockchain::getPending),
            hashLookup(blockchain::getPendingEvents),
            this::isShuttingDown,
            (Hash hash) -> blockchain.getStateStorage().read(hash));
    blockchain.attachFinalizationListener(this::onFinalizationMessage);
    this.blockProducer =
        new BlockProducer(
            blockchain,
            keyPair.getPublic(),
            dataDirectory.createFile("latestProducedBlock"),
            this::shouldProduceBlock,
            createMaximumWait(System::currentTimeMillis, heartBeatInterval),
            this::blockProduced,
            heartBeatInterval);

    this.livenessMonitor =
        new LivenessMonitor(
            this::sendSync,
            this::getLatestProductionTime,
            livenessTimeout(heartBeatInterval, networkDelay));
    String subChainId = blockchain.getChainState().getExecutedState().getSubChainId();
    if (transactionSender != null) {
      blockchain.attach(
          new EpochChangeListener(
              blockTime -> blockchain.getFinalizedBlock(blockTime).getFinalizationData(),
              state -> new ConsensusState(state),
              account,
              subChainId,
              transactionSender));
    }

    if (additionalStatusProviders != null) {
      registerAdditionalStatusProviders(additionalStatusProviders, subChainId);
    }

    logger.info("Initialized producer for account={}", account);
  }

  private void registerAdditionalStatusProviders(
      AdditionalStatusProviders additionalStatusProviders, String subChainId) {
    additionalStatusProviders.register(
        new FastTrackStateStatusProvider(subChainId, new FastTrackStateStatus(state)));

    additionalStatusProviders.register(
        new ShutdownStateStatusProvider(subChainId, new ShutdownStateStatus(shutdownState)));

    additionalStatusProviders.register(
        new BlockProducerStatusProvider(subChainId, new BlockProducerStatus(blockProducer)));
  }

  int determineNextProducer(Block block, ConsensusState global) {
    return determineNextProducer(
        block,
        global,
        blockchain::getBlock,
        y -> new ConsensusState(blockchain.getState(y.getBlockTime() - 1)),
        cachedExpectedProducer);
  }

  static int determineNextProducer(
      Block latestBlock,
      ConsensusState initialState,
      LongFunction<Block> blockRetriever,
      Function<Block, ConsensusState> stateRetriever,
      Map<Hash, Integer> cachedExpectedProducer) {

    ConsensusState stateForCurrentBlock = initialState;

    if (latestBlock == null || latestBlock.getBlockTime() == 0) {
      return 0;
    } else {
      int size = stateForCurrentBlock.getCommittee().size();
      Hash committeeId = stateForCurrentBlock.getCommitteeId();

      Block parent = latestBlock;
      int count = 0;
      while (true) {
        if (!isResetBlock(parent)) {
          int expectedProducer = (nextProducerNonReset(parent) + count) % size;
          cachedExpectedProducer.put(latestBlock.identifier(), expectedProducer);
          return expectedProducer;
        } else {
          if (parent.getBlockTime() == 0
              || !stateForCurrentBlock.isFastTrack()
              || !stateForCurrentBlock.getCommitteeId().equals(committeeId)) {
            int expectedProducer = count % size;
            cachedExpectedProducer.put(latestBlock.identifier(), expectedProducer);
            return expectedProducer;
          }
          Integer cachedProducer = cachedExpectedProducer.get(parent.identifier());
          if (cachedProducer != null) {
            int expectedProducer = (cachedProducer + count) % size;
            cachedExpectedProducer.put(latestBlock.identifier(), expectedProducer);
            return expectedProducer;
          }

          parent = blockRetriever.apply(parent.getBlockTime() - 1);
          stateForCurrentBlock = stateRetriever.apply(parent);
          count++;
        }
      }
    }
  }

  private static int nextProducerNonReset(Block latestBlock) {
    if (latestBlock.getBlockTime() % 100 == 0) {
      return (int) latestBlock.getProducerIndex() + 1;
    } else {
      return latestBlock.getProducerIndex();
    }
  }

  static boolean isResetBlock(Block latestBlock) {
    return latestBlock.getProducerIndex() == -1;
  }

  private static <T> Function<Hash, T> hashLookup(Supplier<Map<Hash, T>> getPending) {
    return hash -> getPending.get().get(hash);
  }

  void sendShutdownSync() {
    blockchain.sendFinalizationRequest(createShutDownSyncData());
  }

  static long livenessTimeout(long productionTimeout, long networkDelay) {
    return productionTimeout + 2 * networkDelay;
  }

  void floodShutdown(DataStreamSerializable dataStreamSerializable) {
    floodFinalizationMessage(FinalizationMessage.SHUTDOWN, dataStreamSerializable);
  }

  private void floodFinalizationMessage(
      FinalizationMessage messageType, Consumer<SafeDataOutputStream> content) {
    blockchain.floodFinalizationData(createFinalizationData(messageType, content));
  }

  static ToLongFunction<BlockAndStateWithParent> createMaximumWait(
      LongSupplier currentTimeMillis, long timeout) {
    return parent ->
        hasTransactions(parent)
                || finalizesTransactions(parent)
                || isParentOld(currentTimeMillis, parent, timeout)
            ? 0
            : timeout;
  }

  private static boolean isParentOld(
      LongSupplier now, BlockAndStateWithParent parent, long timeout) {
    return now.getAsLong() - parent.currentBlock().getProductionTime() > 10 * timeout + 30_000;
  }

  private static boolean finalizesTransactions(BlockAndStateWithParent parent) {
    return parent.finalState().getExecutedState().getExecutionStatus().size() != 0;
  }

  private static boolean hasTransactions(BlockAndStateWithParent parent) {
    return !parent.currentBlock().getTransactions().isEmpty()
        || !parent.currentBlock().getEventTransactions().isEmpty();
  }

  long getLatestProductionTime() {
    Stream<BlockAndState> blocks =
        Stream.concat(blockchain.getProposals().stream(), Stream.of(blockchain.latest()));
    return blocks.map(BlockAndState::getBlock).mapToLong(Block::getProductionTime).max().orElse(0L);
  }

  void sendSync() {
    blockchain.sendFinalizationRequest(createSyncData());
  }

  void onFinalizationMessage(Connection connection, FinalizationData data) {
    byte[] actualFinalizationData = data.getData();
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(actualFinalizationData);
    FinalizationMessage message = input.readEnum(FinalizationMessage.values());
    if (message == FinalizationMessage.BLOCK) {
      Block block = Block.read(input);
      Signature producerSignature = Signature.read(input);
      Optional<FastTrackBlock> parent = filterAndFindParent(block, producerSignature);
      if (parent.isPresent()) {
        if (blockchain.getPending().keySet().containsAll(block.getTransactions())) {
          FastTrackBlock fastTrackBlock = parent.get();
          state.addBlock(fastTrackBlock);
        } else {
          logger.info(
              "Sending sync since we are missing transactions from incoming block {}", block);
          sendFinalizationMessage(
              connection, FinalizationMessage.SYNC_REQUEST, FunctionUtility.noOpConsumer());
        }
      }
    }

    if (message == FinalizationMessage.ECHO) {
      SignatureAndVerification signatureAndVerification = SignatureAndVerification.read(input);
      state.addEcho(signatureAndVerification);
    }

    if (message == FinalizationMessage.SYNC_REQUEST) {
      sendFinalizationMessage(connection, FinalizationMessage.SYNC_RESPONSE, state::writeForSync);
    }

    // Backward compatible invocation
    if (message == FinalizationMessage.SHUTDOWN_SYNC) {
      shutdownState.sendSync(
          shutdownMessage ->
              sendFinalizationMessage(connection, FinalizationMessage.SHUTDOWN, shutdownMessage),
          ShutdownState.ShutdownStage.NOT_PARTICIPATING);
    }

    if (message == FinalizationMessage.SHUTDOWN_SYNC_LIMIT_SEND) {
      ShutdownState.ShutdownStage invocation = input.readEnum(ShutdownState.ShutdownStage.values());
      shutdownState.sendSync(
          shutdownMessage ->
              sendFinalizationMessage(connection, FinalizationMessage.SHUTDOWN, shutdownMessage),
          invocation);
    }

    if (message == FinalizationMessage.SYNC_RESPONSE) {
      int numberOfBlocks = input.readInt();
      for (int i = 0; i < numberOfBlocks; i++) {
        Block block = Block.read(input);
        Signature signature = Signature.read(input);
        Optional<FastTrackBlock> parent = filterAndFindParent(block, signature);
        parent.ifPresent(state::addBlock);
        SIGNATURE_AND_VERIFICATION_LIST.readDynamic(input).forEach(state::addEcho);
      }
    }
    if (message == FinalizationMessage.SHUTDOWN) {
      shutdownState.incoming(input);
    }

    state.getFinalBlocks().forEach(this::appendBlock);

    state.clean(possibleParents(blockchain));
  }

  private void sendFinalizationMessage(
      Connection connection,
      FinalizationMessage messageType,
      Consumer<SafeDataOutputStream> message) {
    connection.send(
        new Packet<>(Packet.Type.FINALIZATION, createFinalizationData(messageType, message)));
  }

  void appendBlock(FinalBlock finalBlock) {
    blockchain.appendBlock(finalBlock);
  }

  private FinalizationData createSyncData() {
    return createFinalizationData(FinalizationMessage.SYNC_REQUEST, FunctionUtility.noOpConsumer());
  }

  private FinalizationData createShutDownSyncData() {
    return createFinalizationData(
        FinalizationMessage.SHUTDOWN_SYNC_LIMIT_SEND, shutdownState::writeShutDownStage);
  }

  private static FinalizationData createFinalizationData(
      FinalizationMessage messageType, Consumer<SafeDataOutputStream> content) {
    return FinalizationData.create(
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeEnum(messageType);
              content.accept(stream);
            }));
  }

  private List<BlockAndState> possibleParents(BlockchainLedger blockchain) {
    ArrayList<BlockAndState> blockAndStates = new ArrayList<>(blockchain.getProposals());
    blockAndStates.add(blockchain.latest());
    return blockAndStates;
  }

  private void blockProduced(Block block) {
    logger.info("Created block {}", block);
    Hash identifier = ProducerHash.create(block);
    Signature signature = keyPair.sign(identifier);
    Optional<FastTrackBlock> parent = filterAndFindParent(block, signature);
    parent.ifPresent(state::addBlock);
  }

  boolean shouldProduceBlock(Block block, ConsensusState consensus) {
    if (shutdownState.isShuttingDown()) {
      return false;
    }

    if (consensus.isFastTrack()) {
      int newestCommittee = consensus.getNewestCommitteeId();
      int activeCommittee = consensus.getActiveCommittee();
      if (newestCommittee > activeCommittee) {
        return false;
      }

      int nextProducer = determineNextProducer(block, consensus);
      int index = consensus.getCommittee().indexOf(account);
      return index == nextProducer;
    } else {
      return false;
    }
  }

  @Override
  public Stream<AutoCloseable> resources() {
    return Stream.of(blockProducer, state, livenessMonitor, shutdownState);
  }

  /**
   * Ignores the block if it exists in block proposals. Otherwise, tries to find the parent of the
   * given block and returns the corresponding fast track block if possible.
   *
   * @param block to find parent of it does not exist in proposals already
   * @param producerSignature if parent exists for fast track block
   * @return fast track block if not proposed already and parent exists
   */
  private Optional<FastTrackBlock> filterAndFindParent(Block block, Signature producerSignature) {
    List<BlockAndState> potentialParents = possibleParents(blockchain);
    boolean blockAlreadyProposed =
        potentialParents.stream().anyMatch(b -> b.getBlock().equals(block));
    if (blockAlreadyProposed) {
      return Optional.empty();
    }
    for (BlockAndState parent : potentialParents) {
      if (parent.getBlock().identifier().equals(block.getParentBlock())) {
        ImmutableChainState parentState = parent.getState();
        ConsensusState consensusState = new ConsensusState(parentState);
        if (consensusState.isFastTrack()) {
          int currentProducer = determineNextProducer(parent.getBlock(), consensusState);

          FastTrackBlock fastTrackBlock =
              new FastTrackBlock(block, producerSignature, parent, currentProducer);
          return Optional.of(fastTrackBlock);
        }
      }
    }
    return Optional.empty();
  }

  void triggerShutdown() {
    shutdownState.triggerShutdown();
  }

  boolean isShuttingDown() {
    return shutdownState.isShuttingDown();
  }

  List<BlockState> getBlockStates() {
    return state.getBlockStates();
  }

  enum FinalizationMessage {
    BLOCK,
    ECHO,
    SYNC_REQUEST,
    SYNC_RESPONSE,
    SHUTDOWN,
    SHUTDOWN_SYNC,
    SHUTDOWN_SYNC_LIMIT_SEND
  }

  static final class FastTrackBlock {

    final Block block;
    final Signature producerSignature;
    final BlockAndState parent;
    final int expectedProducer;

    FastTrackBlock(
        Block block, Signature producerSignature, BlockAndState parent, int expectedProducer) {
      this.block = block;
      this.producerSignature = producerSignature;
      this.parent = parent;
      this.expectedProducer = expectedProducer;
    }

    boolean isValid() {
      Hash messageHash = ProducerHash.create(block);
      BlockchainPublicKey producer = producerSignature.recoverPublicKey(messageHash);
      boolean structurallyValid = BlockchainLedger.isBlockStructurallyValid(parent, block);
      short producerIndex = block.getProducerIndex();
      return structurallyValid
          && block.getCommitteeId() == getConsensus().getActiveCommittee()
          && producerIndex == expectedProducer
          && getConsensus().getCommittee().get(producerIndex).equals(producer);
    }

    List<BlockchainPublicKey> getVerifiers() {
      return new ArrayList<>(getConsensus().getCommittee());
    }

    List<BlsPublicKey> getVerifierBlsKeys() {
      return new ArrayList<>(getConsensus().getCommitteeBlsKeys());
    }

    private ConsensusState getConsensus() {
      return new ConsensusState(parent.getState());
    }

    boolean validateTransactions(
        Function<Hash, SignedTransaction> pendingTransactionLookup,
        Function<Hash, ExecutableEvent> pendingEventLookup) {
      List<SignedTransaction> transactions =
          block.getTransactions().stream()
              .map(pendingTransactionLookup)
              .collect(Collectors.toList());
      List<ExecutableEvent> events =
          block.getEventTransactions().stream()
              .map(pendingEventLookup)
              .collect(Collectors.toList());
      if (transactions.contains(null) || events.contains(null)) {
        return false;
      }
      return BlockchainLedger.areTransactionsAndEventsValid(
          block, parent.getState(), transactions, events);
    }
  }

  static final class SizeConstrainedCache extends LinkedHashMap<Hash, Integer> {

    @Serial private static final long serialVersionUID = 1L;
    private static final int MAX_SIZE = 100;

    @Override
    protected boolean removeEldestEntry(Map.Entry<Hash, Integer> eldest) {
      return size() > MAX_SIZE;
    }
  }
}
