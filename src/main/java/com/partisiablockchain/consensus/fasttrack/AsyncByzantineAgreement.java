package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.consensus.fasttrack.VoteUpdatingProtocol.VoteResult;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Implementation of async byzantine agreement protocol. */
public final class AsyncByzantineAgreement implements Protocol<Boolean> {

  private static final Logger logger = LoggerFactory.getLogger(AsyncByzantineAgreement.class);

  private final PartyConfiguration partyConfiguration;
  private final Consumer<DataStreamSerializable> flood;
  private final byte[] protocolId;
  private final TimeCapsules timeCapsules;
  private boolean currentVote;

  private Hash activeUpdaterId;
  private int round = 0;
  private Protocol<VoteResult> activeVoteUpdater;
  private Stage stage;
  private Boolean output;
  private VoteUpdateCreator voteUpdateCreator;

  private AsyncByzantineAgreement(
      PartyConfiguration partyConfiguration,
      Consumer<DataStreamSerializable> flood,
      byte[] protocolId,
      boolean currentVote,
      VoteUpdateCreator voteUpdateCreator) {
    this.partyConfiguration = partyConfiguration;
    this.flood = flood;
    this.protocolId = protocolId;
    this.currentVote = currentVote;
    this.timeCapsules = new TimeCapsules(this.partyConfiguration);
    this.voteUpdateCreator = voteUpdateCreator;

    startNextStabilizing();
  }

  static AsyncByzantineAgreement create(
      DeterministicRandomBitGenerator randomGenerator,
      PartyConfiguration partyConfiguration,
      Consumer<DataStreamSerializable> flood,
      byte[] protocolId,
      boolean currentVote) {
    return createInternal(
        partyConfiguration,
        flood,
        VoteUpdateCreator.createDefault(randomGenerator),
        protocolId,
        currentVote);
  }

  static AsyncByzantineAgreement createInternal(
      PartyConfiguration partyConfiguration,
      Consumer<DataStreamSerializable> flood,
      VoteUpdateCreator voteUpdateCreator,
      byte[] protocolId,
      boolean currentVote) {
    return new AsyncByzantineAgreement(
        partyConfiguration, flood, protocolId, currentVote, voteUpdateCreator);
  }

  @Override
  public Boolean getResult() {
    return output;
  }

  @Override
  public boolean isDone() {
    return output != null;
  }

  @Override
  public boolean isTerminated() {
    return timeCapsules.shouldTerminate();
  }

  @Override
  public void incoming(SafeDataInputStream message) {
    if (isTerminated()) {
      return;
    }
    MessageType messageType = message.readEnum(MessageType.values());
    if (messageType == MessageType.VUP) {
      handleVupMessage(message);
    }
    if (messageType == MessageType.TIMECAP) {
      TimeCapMessage read = TimeCapMessage.read(message);
      Hash timeCapsuleHash = createTimeCapsuleHash(read.vote());
      storeAndFloodTimeCapsule(read.signature().recoverSender(timeCapsuleHash), read);
      if (timeCapsules.isDetermined()) {
        setResult(timeCapsules.getDeterminedResult());
      }
    }
  }

  private void handleVupMessage(SafeDataInputStream message) {
    Hash vupId = Hash.read(message);
    if (vupId.equals(activeUpdaterId)) {
      activeVoteUpdater.incoming(message);
      if (activeVoteUpdater.isDone()) {
        handleFinishedVup();
      }
    }
  }

  private void handleFinishedVup() {
    VoteResult updatedVote = activeVoteUpdater.getResult();
    currentVote = updatedVote.getVote();
    if (stage == Stage.STABILIZE) {
      startNextDetecting();
    } else /*if (stage == Stage.DETECT)*/ {
      round++;
      logger.info("Starting vote updating. round={}", round);
      if (updatedVote.isHardVote()) {
        setResult(updatedVote.getVote());
      }
      startNextStabilizing();
    }
  }

  private void setResult(boolean resultVote) {
    if (!isDone()) {
      this.output = resultVote;
      storeAndFloodTimeCapsule(
          partyConfiguration.getMyAddress(),
          new TimeCapMessage(
              resultVote, partyConfiguration.sign(createTimeCapsuleHash(resultVote))));
    }
  }

  private void storeAndFloodTimeCapsule(BlockchainAddress sender, TimeCapMessage message) {
    if (timeCapsules.addCapsule(message.vote(), sender)) {
      flood.accept(
          stream -> {
            stream.writeEnum(MessageType.TIMECAP);
            message.write(stream);
          });
    }
  }

  Hash createTimeCapsuleHash(boolean vote) {
    return Hash.create(
        stream -> {
          stream.writeString("TIME_CAPSULE");
          stream.write(protocolId);
          stream.writeBoolean(vote);
        });
  }

  private void startNextDetecting() {
    Hash detectingId = createDetectingHash();
    stage = Stage.DETECT;
    activeUpdaterId = detectingId;
    activeVoteUpdater =
        voteUpdateCreator.createDetecting(
            partyConfiguration, detectingId.getBytes(), createSubFlood(detectingId), currentVote);
  }

  Hash createDetectingHash() {
    return createVoteUpdaterHash("DETECTING");
  }

  Hash createStabilizingHash() {
    return createVoteUpdaterHash("STABILIZING");
  }

  private Hash createVoteUpdaterHash(String stabilizing) {
    return Hash.create(
        stream -> {
          stream.writeString(stabilizing);
          stream.write(protocolId);
          stream.writeInt(round);
        });
  }

  private void startNextStabilizing() {
    Hash stabilizingId = createStabilizingHash();
    stage = Stage.STABILIZE;
    activeUpdaterId = stabilizingId;
    activeVoteUpdater =
        voteUpdateCreator.createStabilizing(
            partyConfiguration,
            stabilizingId.getBytes(),
            createSubFlood(stabilizingId),
            currentVote);
  }

  private Consumer<DataStreamSerializable> createSubFlood(Hash protocol) {
    return serializable ->
        flood.accept(
            stream -> {
              stream.writeEnum(MessageType.VUP);
              protocol.write(stream);
              serializable.write(stream);
            });
  }

  int getRound() {
    return round;
  }

  private enum Stage {
    STABILIZE,
    DETECT
  }

  enum MessageType {
    VUP,
    TIMECAP
  }
}
