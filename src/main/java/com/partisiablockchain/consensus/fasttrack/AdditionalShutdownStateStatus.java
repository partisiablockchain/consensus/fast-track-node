package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.Map;
import java.util.Set;

/** Additional information about the status of the shutdown state. */
interface AdditionalShutdownStateStatus {

  /** Length of shutdown storage. */
  long shutdownStorageLength();

  /** Shutdown stage. */
  ShutdownState.ShutdownStage shutdownStage();

  /** Block times of shutdown blocks and addresses of the nodes that have provided signatures. */
  Map<Long, Set<BlockchainAddress>> shutdownSigners();
}
