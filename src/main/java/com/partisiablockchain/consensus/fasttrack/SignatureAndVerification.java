package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

final class SignatureAndVerification
    implements DataStreamSerializable, BlsSignatureHelper.HasBlsSignature {

  private final Signature signature;
  private final BlsSignature blsSignature;
  private final Hash proof;

  public static SignatureAndVerification read(SafeDataInputStream stream) {
    return new SignatureAndVerification(
        Signature.read(stream), BlsSignature.read(stream), Hash.read(stream));
  }

  SignatureAndVerification(Signature signature, BlsSignature blsSignature, Hash proof) {
    this.signature = signature;
    this.blsSignature = blsSignature;
    this.proof = proof;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SignatureAndVerification that = (SignatureAndVerification) o;
    return signature.equals(that.signature)
        && blsSignature.equals(that.blsSignature)
        && proof.equals(that.proof);
  }

  @Override
  public int hashCode() {
    return Objects.hash(signature, blsSignature, proof);
  }

  public Signature getSignature() {
    return signature;
  }

  public BlsSignature getBlsSignature() {
    return blsSignature;
  }

  public Hash getProof() {
    return proof;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    signature.write(stream);
    blsSignature.write(stream);
    proof.write(stream);
  }

  @Override
  public BlsSignature get() {
    return getBlsSignature();
  }
}
