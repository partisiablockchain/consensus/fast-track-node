package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

final class BlockState {

  private final Consumer<SignatureAndVerification> persistenceCallback;
  private final Hash expectedHash;
  private final Block block;
  private final Signature producerSignature;
  private final List<BlockchainPublicKey> expectedSigners;
  private final Function<Hash, byte[]> rawStateAccess;
  private final BlsSignatureHelper<SignatureAndVerification> blsHelper;

  BlockState(
      Consumer<SignatureAndVerification> persistenceCallback,
      Block block,
      Signature producerSignature,
      List<BlockchainPublicKey> verifiers,
      List<BlsPublicKey> verifierBlsKeys,
      List<SignatureAndVerification> signatures,
      Function<Hash, byte[]> rawStateAccess) {
    this.block = block;
    this.expectedHash = block.identifier();
    this.producerSignature = producerSignature;
    this.expectedSigners = List.copyOf(verifiers);
    this.persistenceCallback = persistenceCallback;
    this.rawStateAccess = rawStateAccess;
    // associate signers with their BLS keys here.
    Map<BlockchainAddress, BlsPublicKey> signerMap = new HashMap<>();
    for (int i = 0; i < verifiers.size(); i++) {
      signerMap.put(expectedSigners.get(i).createAddress(), verifierBlsKeys.get(i));
    }
    this.blsHelper = new BlsSignatureHelper<>(signerMap, block.identifier());
    signatures.forEach(this::putSignature);
  }

  private boolean putSignature(SignatureAndVerification signatureAndVerification) {
    Signature signature = signatureAndVerification.getSignature();
    BlsSignature blsSignature = signatureAndVerification.getBlsSignature();
    Hash message = FinalizationMessageHelper.createMessage(block.identifier(), blsSignature);
    BlockchainPublicKey signer = signature.recoverPublicKey(message);
    // it suffices to only check the proof here since all things signature's are handled by the
    // blsHelper.
    if (!proofValid(signatureAndVerification.getProof(), signer)) {
      return false;
    }

    boolean added = blsHelper.addSignature(signature, signatureAndVerification, message);
    // proactive check
    blsHelper.check();
    return added;
  }

  boolean proofValid(Hash received, BlockchainPublicKey prover) {
    Hash expected =
        Hash.create(
            stream -> {
              prover.write(stream);
              stream.write(rawStateAccess.apply(block.getState()));
            });
    return expected.equals(received);
  }

  boolean addEcho(SignatureAndVerification signatureAndVerification) {
    if (putSignature(signatureAndVerification)) {
      persistenceCallback.accept(signatureAndVerification);
      return true;
    } else {
      return false;
    }
  }

  boolean isDone() {
    return blsHelper.check();
  }

  FinalBlock asFinalBlock() {
    List<BlockchainAddress> signerAddresses =
        expectedSigners.stream()
            .map(BlockchainPublicKey::createAddress)
            .collect(Collectors.toList());
    BlsSignatureHelper.SignatureAndBitVector signature = blsHelper.getSignature(signerAddresses);
    return new FinalBlock(
        block,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeDynamicBytes(signature.bitVector);
              signature.signature.write(s);
            }));
  }

  Block getBlock() {
    return block;
  }

  boolean isSigner(BlockchainPublicKey account) {
    return expectedSigners.contains(account);
  }

  Hash getEchoHash() {
    return expectedHash;
  }

  void writeTo(SafeDataOutputStream stream) {
    block.write(stream);
    producerSignature.write(stream);
    FastTrackProducer.SIGNATURE_AND_VERIFICATION_LIST.writeDynamic(
        stream, blsHelper.getGoodSignatures());
  }

  List<BlockchainAddress> getGoodSigners() {
    return blsHelper.getGoodSigners();
  }

  List<BlockchainAddress> getBadSigners() {
    return blsHelper.getBadSigners();
  }

  @Override
  public String toString() {
    return "BlockState{" + "block=" + block + '}';
  }
}
