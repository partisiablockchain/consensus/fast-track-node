package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.HashSet;
import java.util.Set;

final class TimeCapsules {

  private final Set<BlockchainAddress> receivedTrueCapsules = new HashSet<>();
  private final Set<BlockchainAddress> receivedFalseCapsules = new HashSet<>();
  private final PartyConfiguration partyConfiguration;

  TimeCapsules(PartyConfiguration partyConfiguration) {
    this.partyConfiguration = partyConfiguration;
  }

  boolean addCapsule(boolean value, BlockchainAddress sender) {
    if (partyConfiguration.isParticipating(sender)) {
      if (value) {
        return receivedTrueCapsules.add(sender);
      } else {
        return receivedFalseCapsules.add(sender);
      }
    }
    return false;
  }

  boolean isDetermined() {
    return receivedCapsules() > partyConfiguration.maliciousCount();
  }

  boolean getDeterminedResult() {
    return receivedTrueCapsules.size() > partyConfiguration.maliciousCount();
  }

  boolean shouldTerminate() {
    return receivedCapsules() >= partyConfiguration.honestCount();
  }

  private int receivedCapsules() {
    return Math.max(receivedTrueCapsules.size(), receivedFalseCapsules.size());
  }
}
