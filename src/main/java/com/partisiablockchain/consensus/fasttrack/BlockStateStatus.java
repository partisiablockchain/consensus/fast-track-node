package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.crypto.Hash;
import java.util.List;

/**
 * Additional information about the state of a proposed block.
 *
 * @param blockIdentifier identifier of the proposed block
 * @param blockTime block time of the proposed block
 * @param blockProducerIndex index of the proposed block's producer.
 * @param goodSigners addresses of the accounts that have provided a valid signature for the block.
 * @param badSigners addresses of the accounts that have provided an invalid signature for the
 *     block.
 */
record BlockStateStatus(
    Hash blockIdentifier,
    long blockTime,
    long blockProducerIndex,
    List<BlockchainAddress> goodSigners,
    List<BlockchainAddress> badSigners) {

  static BlockStateStatus fromBlockState(BlockState blockState) {
    Block block = blockState.getBlock();
    return new BlockStateStatus(
        block.identifier(),
        block.getBlockTime(),
        block.getProducerIndex(),
        blockState.getGoodSigners(),
        blockState.getBadSigners());
  }
}
