package com.partisiablockchain.consensus.fasttrack.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;

/** EpochEnd. */
@SuppressWarnings("ArrayRecordComponent")
public record EpochEndPayload(String shard, long epoch, int[] metrics)
    implements DataStreamSerializable {

  /** Invocation ID of NOTIFY_WITH_METRICS in the fee distribution contract . */
  private static final int INVOKE_FEE_DISTRIBUTION_NOTIFY_WITH_METRICS = 4;

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeByte(INVOKE_FEE_DISTRIBUTION_NOTIFY_WITH_METRICS);
    stream.writeOptional(SafeListStream.primitive(SafeDataOutputStream::writeString), shard);
    stream.writeLong(epoch);
    stream.writeInt(metrics.length);
    for (int metric : metrics) {
      stream.writeShort(metric);
    }
  }
}
