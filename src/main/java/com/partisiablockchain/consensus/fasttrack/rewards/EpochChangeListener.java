package com.partisiablockchain.consensus.fasttrack.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.consensus.fasttrack.CommitteeInfoGetter;
import com.partisiablockchain.consensus.fasttrack.TransactionSender;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.secata.stream.SafeDataInputStream;
import java.util.List;
import java.util.function.Function;

/** Listens for epoch changes and sends a transaction when one occurs. */
public final class EpochChangeListener implements BlockchainLedger.Listener {

  private final Function<Long, byte[]> finalizationDataGetter;
  private final Function<ImmutableChainState, CommitteeInfoGetter> committeeInfoGetter;
  private final BlockchainPublicKey myAccount;
  private final String shard;

  private final TransactionSender transactionSender;
  private final SystemTimeProvider systemTimeProvider;

  private CommitteeSignatureFrequencies metrics;
  private CommitteeInfo committeeInfo;

  /**
   * Create a new epoch change listener.
   *
   * @param finalizationDataGetter function that returns finalization data for a given block
   * @param committeeInfoGetter committee getter
   * @param myAccount the producer's account
   * @param shard the shard the node is running on
   * @param transactionSender the transaction sender to use
   * @param systemTimeProvider a provider for the system time of a service
   */
  EpochChangeListener(
      Function<Long, byte[]> finalizationDataGetter,
      Function<ImmutableChainState, CommitteeInfoGetter> committeeInfoGetter,
      BlockchainPublicKey myAccount,
      String shard,
      TransactionSender transactionSender,
      SystemTimeProvider systemTimeProvider) {
    this.finalizationDataGetter = finalizationDataGetter;
    this.committeeInfoGetter = committeeInfoGetter;
    this.myAccount = myAccount;
    this.shard = shard;
    this.transactionSender = transactionSender;
    this.metrics = null;
    this.systemTimeProvider = systemTimeProvider;
  }

  /**
   * Create a new epoch change listener.
   *
   * @param finalizationDataGetter function that returns finalization data for a given block
   * @param committeeInfoGetter committee getter
   * @param myAccount the producer's account
   * @param shard the shard the node is running on
   * @param transactionSender the transaction sender to use
   */
  public EpochChangeListener(
      Function<Long, byte[]> finalizationDataGetter,
      Function<ImmutableChainState, CommitteeInfoGetter> committeeInfoGetter,
      BlockchainPublicKey myAccount,
      String shard,
      TransactionSender transactionSender) {
    this(
        finalizationDataGetter,
        committeeInfoGetter,
        myAccount,
        shard,
        transactionSender,
        System::currentTimeMillis);
  }

  @Override
  public void newFinalBlock(Block block, ImmutableChainState state) {
    CommitteeInfo newCommitteeInfo = new CommitteeInfo(committeeInfoGetter, state, myAccount);
    if (committeeInfo == null || committeeInfo.committeeChanged(newCommitteeInfo)) {
      metrics =
          new CommitteeSignatureFrequencies(
              new int[newCommitteeInfo.committeeSize], block, systemTimeProvider);

      committeeInfo = newCommitteeInfo;
      // Block was signed by previous committee - ignore
      return;
    }

    if (!committeeInfo.inCommittee) {
      // Nothing to do since we are not participating
      return;
    }

    if (epochChanged(block)) {
      if (metrics.isRecentEpoch) {
        sendEpochEnd();
      }
      metrics =
          new CommitteeSignatureFrequencies(
              new int[newCommitteeInfo.committeeSize], block, systemTimeProvider);
    }

    if (metrics.isRecentEpoch) {
      updateMetrics(finalizationDataGetter.apply(block.getBlockTime()));
    }
  }

  private boolean epochChanged(Block block) {
    long parentBlockTime = block.getBlockTime() - 1;
    long currentEpoch = Epoch.epochFromTimestamp(block.getProductionTime());
    return parentBlockTime != 0 && currentEpoch != metrics.epochOfLatestBlock;
  }

  CommitteeSignatureFrequencies getMetrics() {
    return metrics;
  }

  private void updateMetrics(byte[] finalizationData) {
    SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(finalizationData);
    byte[] bitmap = fromBytes.readDynamicBytes();
    for (int i = 0; i < metrics.frequencies.length; i++) {
      if (testBit(bitmap, i)) {
        metrics.increment(i);
      }
    }
  }

  static boolean testBit(byte[] bitmap, int i) {
    byte block = bitmap[i >>> 3];
    int blockOffset = i % 8;
    return ((block >> blockOffset) & 1) == 1;
  }

  private void sendEpochEnd() {
    EpochEndPayload epochEnd =
        new EpochEndPayload(shard, metrics.epochOfLatestBlock, metrics.getFrequencies());
    transactionSender.sendTransaction(epochEnd);
  }

  static final class CommitteeSignatureFrequencies {

    private final int[] frequencies;
    private final long epochOfLatestBlock;
    private final boolean isRecentEpoch;

    public CommitteeSignatureFrequencies(
        int[] frequencies, Block block, SystemTimeProvider systemTimeProvider) {
      this.frequencies = frequencies;
      this.epochOfLatestBlock = Epoch.epochFromTimestamp(block.getProductionTime());
      this.isRecentEpoch = isRecentEpoch(systemTimeProvider);
    }

    void increment(int index) {
      // only increment if we stay within the bounds of an unsigned short
      if (frequencies[index] <= 2 * Short.MAX_VALUE) {
        frequencies[index]++;
      }
    }

    int[] getFrequencies() {
      return frequencies;
    }

    private boolean isRecentEpoch(SystemTimeProvider systemTimeProvider) {
      long currentEpoch = Epoch.epochFromTimestamp(systemTimeProvider.getSystemTime());
      return currentEpoch <= epochOfLatestBlock + 1;
    }
  }

  @FunctionalInterface
  interface SystemTimeProvider {

    /**
     * Get the current system time in milliseconds.
     *
     * @return current system time
     */
    long getSystemTime();
  }

  private static final class CommitteeInfo {
    private final int committeeSize;
    private final long committeeId;
    private final boolean inCommittee;

    public CommitteeInfo(
        Function<ImmutableChainState, CommitteeInfoGetter> committeeInfoGetterFunction,
        ImmutableChainState state,
        BlockchainPublicKey myAccount) {
      CommitteeInfoGetter committeeInfoGetter = committeeInfoGetterFunction.apply(state);
      committeeId = committeeInfoGetter.getActiveCommittee();

      List<BlockchainPublicKey> committee = committeeInfoGetter.getCommittee();
      committeeSize = committee.size();
      inCommittee = committee.contains(myAccount);
    }

    private boolean committeeChanged(CommitteeInfo committeeInfo) {
      return committeeInfo.committeeId != committeeId;
    }
  }
}
