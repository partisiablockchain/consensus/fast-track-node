package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** Additional information about an ongoing shutdown. */
final class ShutdownStateStatusProvider implements AdditionalStatusProvider {

  /** Shard ID. */
  private final String subChainId;

  private final AdditionalShutdownStateStatus shutdownStateStatus;

  ShutdownStateStatusProvider(
      String subChainId, AdditionalShutdownStateStatus shutdownStateStatus) {
    this.subChainId = subChainId;
    this.shutdownStateStatus = shutdownStateStatus;
  }

  @Override
  public String getName() {
    String shard = subChainId == null ? "Gov" : subChainId;
    return shard + "-" + getClass().getSimpleName();
  }

  @Override
  public Map<String, String> getStatus() {
    Map<String, String> statuses = new HashMap<>();
    statuses.put("storageLength", Long.toString(shutdownStateStatus.shutdownStorageLength()));
    statuses.put("stage", shutdownStateStatus.shutdownStage().toString());
    Map<Long, Set<BlockchainAddress>> blockTimeSigners = shutdownStateStatus.shutdownSigners();
    for (Long blockTime : blockTimeSigners.keySet()) {
      statuses.put(
          "signers" + blockTime,
          blockTimeSigners.get(blockTime).stream()
              .map(BlockchainAddress::writeAsString)
              .toList()
              .toString());
    }
    return statuses;
  }
}
